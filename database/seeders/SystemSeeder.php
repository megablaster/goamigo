<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('systems')->insert([
            'name' => 'GoAmigo',
            'logo' => 'default.png',
            'emails' => 'admin@brauliomiramontes.com',
            'facebook' => 'https://www.facebook.com/Go-Amigo-106699731464261',
            'youtube' => 'https://www.youtube.com/channel/UCPdcHk-Q_wmj3MdO69E5rsA',
            'instagram' => 'https://www.instagram.com/goamigogo',
            'copyright' => 'Copyright © 2021, Go Amigo. All Right Reserved.'
        ]);
    }
}
