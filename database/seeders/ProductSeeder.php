<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //First product
        // $product = new Product;
        // $product
        // ->setTranslation('name','en','Sample class')
        // ->setTranslation('description','en','Take 1 sample class.')
        // ->setTranslation('name','es','Clase muestra')
        // ->setTranslation('description','es','Toma 1 clase muestra.')
        // ->save();

        // $product->price = 0;
        // $product->days = 10;
        // $product->credits = 1;
        // $product->save();

        //Second product
        $product = new Product;
        $product
        ->setTranslation('name','en','1 Class')
        ->setTranslation('description','en','Take 1 class.')
        ->setTranslation('name','es','1 Clase')
        ->setTranslation('description','es','Toma 1 clase.')
        ->save();

        $product->price = 18;
        $product->days = 15;
        $product->credits = 1;
        $product->save();

        //Thrid product
        $product = new Product;
        $product
        ->setTranslation('name','en','5 Classes')
        ->setTranslation('description','en','Take 5 classes.')
        ->setTranslation('name','es','5 Clases')
        ->setTranslation('description','es','Toma 5 clases.')
        ->save();

        $product->price = 85;
        $product->days = 30;
        $product->credits = 5;
        $product->save();

        //Four product
        $product = new Product;
        $product
        ->setTranslation('name','en','10 Classes')
        ->setTranslation('description','en','Take 10 classes.')
        ->setTranslation('name','es','10 Clases')
        ->setTranslation('description','es','Toma 10 clases.')
        ->save();

        $product->price = 160;
        $product->days = 60;
        $product->credits = 10;
        $product->save();
    }
}
