<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Level;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Level Begginner
        $level = new Level;
        $level
        ->setTranslation('name','en','Beginner')
        ->setTranslation('description','en','-')
        ->setTranslation('name','es','Principiante')
        ->setTranslation('description','es','-')
        ->save();

        $level->lessons = 10;
        $level->save();

        //Level Medium
        $level = new Level;
        $level
        ->setTranslation('name','en','Medium')
        ->setTranslation('description','en','-')
        ->setTranslation('name','es','Intermedio')
        ->setTranslation('description','es','-')
        ->save();

        $level->lessons = 10;
        $level->save();

        //Level Advanced
        $level = new Level;
        $level
        ->setTranslation('name','en','Advanced')
        ->setTranslation('description','en','-')
        ->setTranslation('name','es','Avanzado')
        ->setTranslation('description','es','-')
        ->save();

        $level->lessons = 10;
        $level->save();

    }
}
