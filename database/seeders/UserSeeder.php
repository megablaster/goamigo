<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\InfoStudent;
use App\Models\InfoTeacher;
use App\Models\Credit;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creamos usuario de administración
        DB::table('users')->insert([
            'name' => 'Braulio',
            'last_name' => 'Miramontes',
            'email' => 'admin@goamigogo.com',
            'email_verified_at' => now(),
            'uid' => random_int(100000, 999999),
            'id_unique' => Str::random(26),
            'password' => Hash::make('password'),
        ]);

         //Se crea el registro de estudiante
        InfoStudent::create([
            'user_id' => 1
        ]);

        InfoTeacher::create([
            'user_id' => 1
        ]);

        //Creamos usuario de Estudiante
        DB::table('users')->insert([
            'name' => 'Juan',
            'last_name' => 'Valdivia',
            'email' => 'student@goamigogo.com',
            'email_verified_at' => now(),
            'uid' => random_int(100000, 999999),
            'id_unique' => Str::random(26),
            'password' => Hash::make('password'),
        ]);

        //Se crea el registro de estudiante
        InfoStudent::create([
            'user_id' => 2
        ]);

        InfoTeacher::create([
            'user_id' => 2
        ]);

        Credit::create([
            'user_id' => 2,
            'expiration' => Carbon::now()->subDays(11),
            'credit' => 1,
            'trial' => true,
        ]);

        //Creamos roles
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'manager']);
        Role::create(['name' => 'teacher']);
        Role::create(['name' => 'student']);

        //Asignamos rol a administrador
        $user = User::find(1);
        $user->assignRole('admin');

        $user = User::find(2);
        $user->assignRole('student');
    }
}
