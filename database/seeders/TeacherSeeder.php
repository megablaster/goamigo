<?php

namespace Database\Seeders;

use App\Models\InfoStudent;
use App\Models\InfoTeacher;
use App\Models\ScheduleTeacher;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creamos usuario de Maestro
        DB::table('users')->insert([
            'name' => 'Ramón',
            'last_name' => 'Aguilar',
            'email' => 'teacher@goamigogo.com',
            'email_verified_at' => now(),
            'uid' => random_int(100000, 999999),
            'id_unique' => Str::random(26),
            'password' => Hash::make('password'),
        ]);

        //Se crea el registro de teacher
        InfoStudent::create([
            'user_id' => 3
        ]);

        InfoTeacher::create([
            'user_id' => 3,
            'step' => 'third',
            'contract_date' => Carbon::now()
        ]);

        $user = User::find(3);

        $user->assignRole('teacher');

        $hour = Carbon::now();

        //Days
        for ($i=1;$i<5;$i++){

            $multi = 30 * $i;
            $halfAdditional = $hour->addMinute($multi);

            DB::table('schedule_teachers')->insert([
                'user_id' => $user->id,
                'assigned' => 0,
                'title' => $user->name,
                'start' => $hour,
                'end' => $halfAdditional,
                'hour' => $hour,
                'days' => 2
            ]);

        }
    }
}
