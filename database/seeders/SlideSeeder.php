<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Slide;

class SlideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Creamos usuario de administración
        $slide = new Slide;
        $slide
        ->setTranslation('title','en','Online Spanish lessons')
        ->setTranslation('description','en','Open a door to another world for your children, where they will learn, have fun and improve their social skills.')
        ->setTranslation('button','en','Book a free trial')
        ->setTranslation('title','es','Clases de español online')
        ->setTranslation('description','es','Abre una puerta a otro mundo para tus hijos, donde aprenderán, se divertirán y mejorarán sus habilidades sociales.')
        ->setTranslation('button','es','Prueba gratuita')
        ->save();
        $slide->url = 'https://google.com';
        $slide->save();
    }
}
