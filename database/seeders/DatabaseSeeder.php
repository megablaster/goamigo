<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\LevelSeeder;
use Database\Seeders\ProductSeeder;
use Database\Seeders\SlideSeeder;
use Database\Seeders\SystemSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TimezoneSeeder::class);
        $this->call(LevelSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(SlideSeeder::class);
        $this->call(SystemSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TeacherSeeder::class);
        // User::factory(10)->create();
    }
}
