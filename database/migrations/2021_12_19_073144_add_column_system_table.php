<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('systems', function (Blueprint $table) {
            $table->text('privacy')->nullable();
            $table->text('terms')->nullable();
            $table->text('faqs')->nullable();
            $table->string('stripe_base')->nullable()->default('https://api.stripe.com');
            $table->string('stripe_key')->nullable()->default('pk_test_NBLy5vyqPwQ3tsu9UqKujojK');
            $table->string('stripe_secret')->nullable()->default('sk_test_TgNEdZWUZCXpAKPsRf7JZixQ');
            $table->string('bluejeans_base')->nullable()->default('https://api.bluejeans.com/oauth2/token#User');
            $table->string('bluejeans_key')->nullable()->default('37429843263245');
            $table->string('bluejeans_secret')->nullable()->default('d1c64b37879242b6a3eae423c74f2589');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('systems', function (Blueprint $table) {
            $table->dropColumn('privacy');
            $table->dropColumn('terms');
            $table->dropColumn('faqs');
            $table->dropColumn('stripe_base');
            $table->dropColumn('stripe_key');
            $table->dropColumn('stripe_secret');
            $table->dropColumn('bluejeans_base');
            $table->dropColumn('bluejeans_key');
            $table->dropColumn('bluejeans_secret');
        });
    }
}
