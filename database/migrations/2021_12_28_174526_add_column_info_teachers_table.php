<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInfoTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('info_teachers', function (Blueprint $table) {
            $table->boolean('featured')->default(0);
            $table->boolean('approve_info')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('info_teachers', function (Blueprint $table) {
            $table->dropColumn('featured');
            $table->dropColumn('approve_info');
        });
    }
}
