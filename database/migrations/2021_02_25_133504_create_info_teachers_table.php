<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfoTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_teachers', function (Blueprint $table) {
            $table->id();
            $table->set('step',['first','second','third'])->default('first');
            $table->set('level_education',['tecnico_superior','licenciatura','posgrado'])->default('tecnico_superior');

            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            //Fecha de contrato
            $table->date('contract_date')->nullable();
            
            $table->text('experience')->nullable();
            $table->string('letter_presentation')->nullable();
            $table->string('cv')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
