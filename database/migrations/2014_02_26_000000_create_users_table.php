<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->id();
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();

            $table->string('email')->unique()->comment();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            //Info general
            $table->set('gender',['male','female','undefined'])->nullable();
            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();

            //Zoom ID
            $table->string('zoom_id')->default('000000');

            //Enable account
            $table->boolean('suspend')->default(0);

            //Encryp ID
            $table->string('uid')->unique();
            $table->string('id_unique')->unique();

            $table->string('img')->default('user/default.png');

            //Timezone
            $table->unsignedBigInteger('timezone_id')->default(145);
            $table->foreign('timezone_id')->references('id')->on('timezones');

            $table->date('last_login')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
