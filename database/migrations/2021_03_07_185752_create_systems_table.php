<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('logo')->nullable();
            $table->string('facebook')->nullable();
            $table->string('youtube')->nullable();
            $table->string('instagram')->nullable();
            $table->text('emails')->nullable();
            $table->text('copyright')->nullable();
            $table->set('type_app',['campaing','live'])->default('live');
            $table->set('type_coupon',['percentage','amount'])->default('percentage');
            $table->integer('percentage_coupon')->default(10);
            $table->integer('amount_coupon')->default(50);
            $table->date('date_coupon')->nullable();
            $table->decimal('paymentfull', 8,2)->nullable();
            $table->decimal('paymenthalf', 8,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}
