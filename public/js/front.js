$(document).ready(function(){

	$('.change_lang').on('change', function(){
	    var form = $('#change_lang');
	    form.submit();
	});

	//Scroll navbar
	$(function() {
	    var header = $("#navbar");
	    $(window).scroll(function() {
	        var scroll = $(window).scrollTop();

	        if (scroll >= 100) {
	            header.addClass("fixed");
	        } else {
	            header.removeClass("fixed");
	        }
	    });
	});

	$('.menu-burger').on('click', function(e){
		e.preventDefault();
		$('.hidden-mobile').toggleClass('active');
	});

	$('.close-burger').on('click', function(e){
		e.preventDefault();
		$('.hidden-mobile').toggleClass('active');
	});
	
});