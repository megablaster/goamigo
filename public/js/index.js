jQuery(document).ready(function($){

	$('.owl-slide').owlCarousel({
	    loop:true,
	    margin:30,
	    nav:false,
	    autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('.owl-info2').owlCarousel({
	    loop:true,
	    margin:30,
	    nav:false,
	    autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        992:{
	            items:3
	        },
	        1366: {
	        	items:4	
	        }
	    }
	});

	$('.owl-teachers').owlCarousel({
	    loop:true,
	    margin:30,
	    nav:false,
	    autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1200:{
	            items:3
	        }
	    }
	});

	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();

	    if (scroll >= 100) {
	        $(".wsmainfull").addClass("fixed");
	    } else {
	        $(".wsmainfull ").removeClass("fixed");
	    }
	});

});