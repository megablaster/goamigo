<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require('teacher.php');
require('admin.php');
require('student.php');

//Rutas de Auth
Auth::routes(['register'=>false,'verify' => true]);

Route::get('/', function () {
    return view('welcome');
});


//Rutas para FrontEnd
Route::get('/teacher-register', [HomeController::class, 'teacher_register'])->name('register.teacher');
Route::get('/register', [HomeController::class, 'student_register'])->name('register.student');
Route::post('/save-teacher', [HomeController::class, 'teacher_save'])->name('save.teacher');
Route::post('/save-student', [HomeController::class, 'student_save'])->name('save.student');
Route::post('change_lang', [HomeController::class, 'change_lang'])->name('change.lang');
Route::post('search', [HomeController::class, 'search'])->name('search');

//page
Route::get('/', [HomeController::class, 'index'])->name('front.index');
Route::get('about', [HomeController::class, 'about'])->name('front.about');
Route::get('blog', [HomeController::class, 'post'])->name('front.post');
Route::get('teachers', [HomeController::class, 'teacher'])->name('front.teacher');
Route::get('philosophy', [HomeController::class, 'philosophy'])->name('front.philosophy');
Route::get('hiring', [HomeController::class, 'hiring'])->name('front.hiring');
Route::get('pricing', [HomeController::class, 'pricing'])->name('front.pricing');
Route::get('notice-of-privacy', [HomeController::class, 'privacy'])->name('front.privacy');
Route::get('terms-and-conditions', [HomeController::class, 'terms'])->name('front.terms');
Route::get('faqs', [HomeController::class, 'faqs'])->name('front.faqs');
Route::get('blog/search', [HomeController::class, 'search'])->name('front.search');
Route::get('blog/{category}/{url}', [HomeController::class, 'single'])->name('front.single');
Route::get('blog/{category}', [HomeController::class, 'category'])->name('front.category');

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});

//Invoices
// Route::get('/invoices', [StripeController::class,'invoices'])->name('invoices.stripe');
// Route::get('/invoice/{user_id}/{invoice_id}', [StripeController::class,'invoice'])->name('invoice.stripe');

//Get image
Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path($filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
})->name('get.image');
