<?php
use App\Http\Controllers\Manager\AdminController;
use App\Http\Controllers\Manager\ManagerController;

//Teachers
Route::prefix('go-admin/managers')->middleware(['auth','verified','disabled','role:manager'])->group(function () {

    //Managers
    Route::resource('manager',ManagerController::class);

    //Change lang
    Route::post('change/lang', [AdminController::class,'lang'])->name('lang.change.manager');
});
