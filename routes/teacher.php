<?php
use App\Http\Controllers\Teacher\AdminController;
use App\Http\Controllers\Teacher\TeacherController;
use App\Http\Controllers\Teacher\ScheduleTeacherController;
use App\Http\Controllers\Teacher\UserController;
use App\Http\Controllers\Teacher\ClassroomTeacherController;
use App\Http\Controllers\Teacher\InfoTeacherController;
use App\Http\Controllers\Teacher\InvoiceTeacherController;


//Teachers
Route::prefix('go-admin/teachers')->middleware(['auth','verified','disabled','role:teacher'])->group(function () {

	//Teachers
	Route::get('/',[AdminController::class,'index'])->name('admin.teacher.index');
	Route::resource('teachers', TeacherController::class);
	Route::get('calendar', [TeacherController::class,'calendar'])->name('teacher.calendar');
	Route::get('availability', [TeacherController::class,'availability'])->name('teacher.availability');

	//Users
	Route::get('users/suspend/{id}',[UserController::class,'suspend'])->name('users.teacher.suspend');
	Route::get('users/{type}',[UserController::class,'type'])->name('users.teacher.type');
	Route::resource('users_teacher',UserController::class);

	//Schedule Teacher
	Route::post('schedule/ajax',[ScheduleTeacherController::class,'ajax'])->name('teacher.schedule.ajax');
	Route::post('schedule/cancelteacher',[ScheduleTeacherController::class,'cancelTeacher'])->name('teacher.schedule.cancelteacher');
	Route::get('schedule/getDates/{id}',[ScheduleTeacherController::class,'getDates'])->name('teacher.schedule.get');
	Route::resource('schedule',ScheduleTeacherController::class);

	//Update Availability
	Route::resource('infoteachers',InfoTeacherController::class);

	//Invoice
	Route::resource('invoicesteacher',InvoiceTeacherController::class);

	//Classroom
	Route::get('classroomteacher',[ClassroomTeacherController::class,'index'])->name('classroomteacher.index');
	Route::get('classroomteacher/rate/{id}',[ClassroomTeacherController::class,'rate'])->name('classroomteacher.rate');
    Route::post('classroomteacher/rate/save',[ClassroomTeacherController::class,'save_rate'])->name('classroomteacher.rate.save');
    Route::get('classroomteacher/create_room/{id}',[ClassroomTeacherController::class,'create_room'])->name('classroomteacher.create_room');

	//Change lang
	Route::post('change/lang', [AdminController::class,'lang'])->name('lang.change.teacher');
});
