<?php
use App\Http\Controllers\Student\AdminController;
use App\Http\Controllers\Student\StudentController;
use App\Http\Controllers\Student\ScheduleStudentController;
use App\Http\Controllers\Student\UserController;
use App\Http\Controllers\Student\InfoStudentController;
use App\Http\Controllers\Student\FavoriteController;
use App\Http\Controllers\Student\InvoiceController;
use App\Http\Controllers\Student\LessonController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\Student\StripeController;
use App\Http\Controllers\Student\ClassroomController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\VedamoController;

//Teachers
Route::prefix('go-admin/students')->middleware(['auth','verified','disabled','role:student'])->group(function () {

	//Students
	Route::get('/',[AdminController::class,'index'])->name('admin.student.index');
	//Route::resource('student',StudentController::class);

	//Buy package
	Route::get('package',[AdminController::class,'package'])->name('admin.student.package');

	//Products
	Route::get('products/{product}/pay',[ProductController::class,'pay'])->name('product.pay');
	// Route::resource('products', ProductController::class);

	//Users
	Route::resource('student',UserController::class);

	//Stripe
	Route::post('trial', [StripeController::class, 'trial'])->name('trial.stripe');
	Route::post('payment', [StripeController::class, 'payment'])->name('payment.stripe');
	Route::get('invoices/lessons',[InvoiceController::class,'lessons'])->name('invoice.lesson');
	Route::get('invoices/product',[InvoiceController::class,'product'])->name('invoice.product');

	//Schedule
	// Route::post('schedule/ajax',[ScheduleTeacherController::class,'ajax'])->name('teacher.schedule.ajax');
	Route::get('schedule/{id}',[LessonController::class,'getSchedule'])->name('student.schedule.get');
	Route::post('addschedule',[LessonController::class,'schedule'])->name('teacher.schedule');

	//Lessons
	Route::get('lessons/calendar_teacher/{id}',[LessonController::class,'calendar_teacher'])->name('lessons.calendar_teacher');
	Route::get('lessons/search',[LessonController::class,'search'])->name('lesson.search');
	Route::get('lessons/create_room/{id}',[LessonController::class,'create_room'])->name('create.room');

	// Route::get('lessons/searching',[LessonController::class,'searching'])->name('lesson.searching');
	Route::resource('lessons', LessonController::class);
    Route::resource('reviews', ReviewController::class);

	Route::post('schedulestudent/cancel',[ScheduleStudentController::class,'cancel'])->name('student.schedule.cancel');
	Route::resource('schedulestudent',ScheduleStudentController::class);

	//Vedamo
	Route::get('create_room/{schedule}/{teacher}/{student}/{classroom_id}',[VedamoController::class,'create_room'])->name('create.room.vedamo');

	//Update Availability
	Route::resource('infostudents',InfoStudentController::class);

	//Add to favorites
	Route::resource('favorites',FavoriteController::class);

	//Change lang
	Route::post('change/lang', [AdminController::class,'lang'])->name('lang.change.student');
});
