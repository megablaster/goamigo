<?php
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\LevelController;
// use App\Http\Controllers\Admin\PlanController;
use App\Http\Controllers\Admin\TestimonialController;
use App\Http\Controllers\Admin\AccountingController;
use App\Http\Controllers\Admin\InfoStudentController;
use App\Http\Controllers\Admin\SlideController;
use App\Http\Controllers\Admin\CreditController;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\LessonController;
use App\Http\Controllers\Admin\SystemController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\Admin\InfoTeacherController;
use App\Http\Controllers\Admin\ScheduleTeacherController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\VedamoController;

//Rutas de administración sin disabled
Route::prefix('go-admin')->middleware(['auth','verified'])->group(function(){
	Route::get('suspend',[AdminController::class,'suspend'])->name('admin.suspend');
});

Route::prefix('go-admin')->middleware(['auth','verified','disabled','role:admin|teacher|student'])->group(function () {

	//Admin
	Route::get('/',[AdminController::class,'index'])->name('admin.index');
	Route::get('users/suspend/{id}',[UserController::class,'suspend'])->name('users.suspend');
	Route::get('users/roles/{type}',[UserController::class,'type'])->name('users.type');
	Route::post('users/featured',[UserController::class,'featured'])->name('users.featured');
	Route::resource('users',UserController::class);
	Route::resource('levels',LevelController::class);
	// Route::resource('plans',PlanController::class);
	Route::resource('testimonials',TestimonialController::class);
	Route::resource('slides',SlideController::class);

	//Info Student
	Route::resource('info_student',InfoStudentController::class);

	//Info Student
	Route::resource('systems',SystemController::class);

	//Credit
	Route::resource('credits',CreditController::class);

	//Products
	Route::resource('products',ProductController::class);

    //Posts
    Route::post('gallery/destroy',[PostController::class,'destroyGallery'])->name('destroy.gallery');
    Route::post('posts/gallery',[PostController::class,'gallery'])->name('upload.gallery');
    Route::resource('posts',PostController::class);

    //Categories
    Route::resource('categories',CategoryController::class);

	//Ingresos
	Route::get('accounting/income',[AccountingController::class,'income'])->name('accounting.income');
	Route::get('accounting/lesson',[AccountingController::class,'lesson'])->name('accounting.lesson');

	//Change lang
	Route::post('change/lang', [AdminController::class,'lang'])->name('lang.change');
});


Route::prefix('go-admin/manager')->middleware(['auth','verified','disabled'])->group(function () {
});

//Logout
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout.click');
