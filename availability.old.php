var calendar = $('#calendar').fullCalendar({
	    	    // defaultView: 'agendaWeek',
                editable: true,
                slotDuration: '00:30:00',
                events: "{{route('schedule.index')}}",
                displayEventTime: true,
                firstDay: 0,
                editable: true,
                allDaySlot: false,
                eventRender: function (event, element, view) {
                    if (event.allDay === 'true') {
                            event.allDay = true;
                    } else {
                            event.allDay = false;
                    }
                },
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay) {

                    var title = "{{auth()->user()->name}} {{auth()->user()->last_name}}";

                    if (title) {
                        var start = $.fullCalendar.formatDate(start, "Y-MM-DD");
                        var end = $.fullCalendar.formatDate(end, "Y-MM-DD");
                        $.ajax({
                            url: "{{route('teacher.schedule.ajax')}}",
                            data: {
                                title: title,
                                start: start,
                                end: end,
                                user_id: "{{auth()->user()->id}}",
                                type: 'add'
                            },
                            type: "POST",
                            success: function (data) {
                                displayMessage("Se ha creado el evento correctamente.");

                                calendar.fullCalendar('renderEvent',
                                    {
                                        id: data.id,
                                        title: title,
                                        start: start,
                                        end: end,
                                        allDay: allDay
                                    },true);

                                calendar.fullCalendar('unselect');
                            }
                        });
                    }
                },
                eventDrop: function (event, delta) {
                    var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
                    var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD");

                    $.ajax({
                        url: "{{route('teacher.schedule.ajax')}}",
                        data: {
                            title: event.title,
                            start: start,
                            end: end,
                            id: event.id,
                            type: 'update'
                        },
                        type: "POST",
                        success: function (response) {
                            displayMessage("Se ha actualizado el evento correctamente.");
                        }
                    });
                },
                eventClick: function (event) {
                    var deleteMsg = confirm("¿Realmente quieres eliminar?");
                    if (deleteMsg) {
                        $.ajax({
                            type: "POST",
                            url: "{{route('teacher.schedule.ajax')}}",
                            data: {
                                    id: event.id,
                                    type: 'delete'
                            },
                            success: function (response) {
                                calendar.fullCalendar('removeEvents', event.id);
                                displayMessage("Se ha borrado el evento correctamente.");
                            }
                        });
                    }
                }
            });

			function displayMessage(message) {
			    toastr.success(message, 'Event');
			} 