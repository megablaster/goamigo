<div>
    <div class="card">
        <div class="card-body">

            <form id="card-form">

                <div class="row">
                    <div class="col-xl-12">
                        <h2>Método de pago</h2>
                    </div>
                </div>

                <div class="form group">
                    <label>Nombre del titular</label>
                    <input id="card-holder-name" type="text" class="form-control" required>
                </div>

                <!-- Stripe Elements Placeholder -->
                <div class="form-group">
                    <label>No. de tarjeta</label>
                    <div id="card-element" class="form-control"></div>
                    <span id="card-error"></span>
                </div>

                <button id="card-button" class="btn btn-primary btn-block">Realizar pago</button>

            </form>

        </div>
    </div>

</div>

