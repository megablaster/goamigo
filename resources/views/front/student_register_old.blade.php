@extends('layouts.app')

@section('content')

	<section id="register-teacher" class="p-40" style="background-image: url('{{asset('assets/img/home/bg-teacher.jpg')}}');">
		<div class="container-fluid">
			<div class="row">

				<div class="col-xl-12">

					<h2>Llena la solicitud para formar parte de <strong>Go Amigo</strong>.</h2>
					<form action="#">
						<h3>Datos del tutor</h3>
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group">
									<label>Nombre</label>
									<input type="text"  class="form-control" name="name" required placeholder="Nombre">
								</div>
							</div>
							<div class="col-xl-6">
								<div class="form-group">
									<label>Apellido</label>
									<input type="text"  class="form-control" name="last_name" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Teléfono</label>
							<input type="tel" class="form-control" name="phone" required>
						</div>
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group">
									<label>Correo electrónico</label>
									<input type="email"  class="form-control" name="email" required>
								</div>
							</div>
							<div class="col-xl-6">
								<div class="form-group">
									<label>Repetir correo electrónico</label>
									<input type="email"  class="form-control" name="email" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group">
									<label>Ciudad</label>
									<input type="text" class="form-control" name="city" required>
								</div>
							</div>
							<div class="col-xl-6">
								<div class="form-group">
									<label>País</label>
									<input type="text" class="form-control" name="country" required>
								</div>
							</div>
							<div class="col-xl-6">
								<div class="form-group">
									<label>Selecciona un genero</label>
									<select name="gender" class="form-control">
										<option value="male">Masculino</option>
										<option value="female">Femenino</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group">
									<label>Contraseña</label>
									<input type="password"  class="form-control" name="password" required>
								</div>
							</div>
							<div class="col-xl-6">
								<div class="form-group">
									<label>Repetir contraseña</label>
									<input type="password"  class="form-control" name="password_repeat" required>
								</div>
							</div>
						</div>
						<h3>Datos del alumno</h3>
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group">
									<label>Nombre de usuario</label>
									<input type="text" class="form-control" name="nickname" required>
								</div>	
							</div>
							<div class="col-xl-6">
								<div class="form-group">
									<label>Fecha de nacimiento</label>
									<input type="date" class="form-control" name="birthday" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>¿Por que necesita que aprenda español?</label>
							<textarea name="comments_learn" class="form-control" rows="3" required></textarea>
						</div>
						<div class="form-group">
							<label>¿Comentarios sobre el niñ@?</label>
							<textarea name="comments_children" class="form-control" rows="3" required></textarea>
						</div>
						<button type="submit" class="btn btn-yellow btn-block">Registrar cuentra</button>
					</form>
				</div>

			</div>
		</div>
	</section>

@endsection

@push('css')
@endpush

@push('js')
@endpush