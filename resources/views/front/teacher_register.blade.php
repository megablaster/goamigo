@extends('layouts.appwhite')

@section('title','¡Registra una cuenta, ahora!')

@push('css')
	<style>
		body {
			background-image: url('{{asset('/assets/img/home/bg-header.jpg')}}');
			background-size: cover;
			background-repeat: no-repeat;
		}
	</style>
@endpush

@section('content')

	<section id="register-teacher" class="p-40">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 text-center">
					<a href="{{route('front.index')}}">
						<img src="{{asset('assets/img/logo.svg')}}" alt="{{config('app.name')}}" class="img-fluid logo">
					</a>
					<h2>{!!__('register_teacher.title')!!}</h2>
				</div>
				<div class="col-xl-10 offset-xl-1">

					<form action="{{ route('save.teacher') }}" method="post">
						<h3>{{__('register_teacher.subtitle')}}</h3><hr>
						@csrf
						<div class="row">
							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.name')}}</label>
									<input type="text"  class="form-control" name="name" required>
								</div>
							</div>

							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.last_name')}}</label>
									<input type="text"  class="form-control" name="last_name" required>
								</div>
							</div>

							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.phone')}}</label>
									<input type="tel" class="form-control" name="phone" required>
								</div>
							</div>

							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.gender')}}</label>
									<select name="gender" class="form-control">
										<option value="">{{__('general.gender.select')}}</option>
										<option value="male">{{__('general.gender.male')}}</option>
										<option value="female">{{__('general.gender.female')}}</option>
										<option value="undefined">{{__('general.gender.undefined')}}</option>
									</select>
								</div>
							</div>
							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.email')}}</label>
									<input type="email"  class="form-control" name="email" required>
								</div>
							</div>
							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.city')}}</label>
									<input type="text" class="form-control" name="city" required>
								</div>
							</div>

							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.country')}}</label>
									<select name="country" class="form-control">
			            				@foreach ($data['countries'] as $country)
			            					<option value="{{$country}}">{{$country}}</option>
			            				@endforeach
			            			</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-12">
								<h3>{{__('register_teacher.title_password')}}</h3><hr>
							</div>
							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.password')}}</label>
									<input type="password"  class="form-control" name="password" required>
								</div>
							</div>

							<div class="col-xl-4">
								<div class="form-group">
									<label>{{__('register_teacher.repeat_password')}}</label>
									<input type="password"  class="form-control" name="password_repeat" required>
								</div>
							</div>
						</div>
						<button type="submit" class="btn btn-blue btn-block">{{__('register_teacher.button')}}</button>
					</form>

				</div>

			</div>
		</div>
	</section>
@endsection

@push('js')

	@if (Session::has('success'))
		<script>
			alert('{!! Session::get('success') !!}');
		</script>
	@endif

@endpush
