@extends('layouts.app')

@section('title','Terms and Conditions')

@push('css')
	<style>
		h1,h2,h3,h4,h5,h6,small,li {
			color: white;
			font-family: "ProgramOT";
		}

		li {
			font-size: 16px;
		}

		p {
			text-align: justify;
			color: white;
		}
	</style>
@endpush

@push('js')
@endpush

@section('content')

	<div id="about">

		<div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">

			@include('front.parts.navbar')

		</section>

		<section style="padding:60px 0;background-image: url('{{asset('/assets/img/home/bg-info2.jpg')}}');">
			<div class="container-fluid">
				<div class="row">

					<div class="col-xl-12">

						<h1>{{__('navbar.menu.terms')}}</h1>
						{!!$global_system->terms!!}

					</div>
					
				</div>
			</div>
		</section>

	</div>

@endsection