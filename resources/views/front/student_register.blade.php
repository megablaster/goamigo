@extends('layouts.app')

@section('title')
	¡Registra una cuenta, ahora!
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('script/password/jquery.passwordRequirements.css')}}">
	<link rel="stylesheet" href="{{asset('script/select/bootstrap-select.min.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/4.0.6/sweetalert2.min.css">
	<style>
		body {
			background-image: url('{{asset('/assets/img/home/bg-header.jpg')}}');
			background-size: cover;
			background-repeat: no-repeat;
		}
	</style>

@endpush

@section('content')
	@include('front.parts.navbar')

	<section id="register-teacher" class="p-40">

		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3">

					<h1>{!!__('register_student.title')!!}</h1>
					
					<form action="{{route('save.student')}}" method="POST">
                        @csrf
                        <!--campaing or live-->
                        <input type="hidden" name="type" value="campaing">
                        <div class="row">
                            <div class="col-xl-12">
                            	<h1 style="display: none;">{!!__('register_student.title')!!}</h1>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        @foreach ($errors->all() as $error)
                                            {{ $error }}
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                	<label>{{__('register_student.email')}}</label>
                                    <input type="email"  class="form-control" name="email" value="{{@old('email')}}" required>
                                </div>
                            </div>
                            <div class="col-xl-12">
								<div class="form-group">
									<label>{{__('register_student.password')}}</label>
									<input type="password"  class="form-control pr-password" name="password" required>
								</div>
							</div>
                             <div class="col-xl-12" style="margin-bottom: 10px;">
                             	 <div class="form-check">
								  <input class="form-check-input" name="privacy" type="checkbox" id="flexCheckDefault">
								  <label class="form-check-label" for="flexCheckDefault">
								    {{__('register_teacher.privacy')}} <a style="text-decoration: underline;color: #af5db0;font-size: 17px;font-family: 'ProgramOT';" href="{{route('front.privacy')}}">{{__('register_teacher.text_privacy')}}</a>
								  </label>
								</div>
                             </div>
                             <div class="col-xl-12" style="margin-bottom: 10px;">
                             	{!! htmlFormSnippet() !!}
                             </div>
                        </div>
                        <button type="submit" class="btn btn-submit">{{__('register_student.button')}}</button>
                    </form>
				</div>

			</div>
		</div>
	</section>

@endsection

@push('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/4.0.6/sweetalert2.min.js"></script>
    <script src="{{asset('script/password/jquery.passwordRequirements.min.js')}}"></script>
	<script>
		$(".pr-password").passwordRequirements({
			numCharacters: 6,
  			useLowercase:true,
  			useUppercase:true,
  			useNumbers:true,
  			useSpecial:true
        });
	</script>
	@if(Auth::check())
		<script>
			swal({
				title: 'Please wait',
				text: 'You will be redirected to the administrator.',
				type: 'success',
				timer: 3000,
				showConfirmButton: false,
			});
			
			setTimeout(function() {
			window.location.href = "/go-admin";
			}, 4000);
		</script>
	@endif
@endpush
