<!--Cookies-->
 <a href="javascript:void(0)" class="js-lcc-settings-toggle"></a>
 
<footer style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">
	<div class="container-fluid padding">
		<div class="row">
			<div class="col-xl-3 col-lg-4">
				<img src="{{route('get.image',$global_system->logo)}}" alt="Go Amigo" class="img-fluid logo">
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4">
				<h3>Quick links</h3>
				<ul>
					@php
			            $route = Route::current()->getName();
			          @endphp
			          <li class="{{($route == 'front.index')?'active':''}}">
			            <a href="{{route('front.index')}}">> {{__('navbar.menu.home')}}</a>
			          </li>
			          <li class="{{($route == 'front.about')?'active':''}}">
			            <a href="{{route('front.about')}}">> {{__('navbar.menu.about')}}</a>
			          </li>
			          <li class="{{($route == 'front.philosophy')?'active':''}}">
			            <a href="{{route('front.philosophy')}}">> {{__('navbar.menu.philosophy')}}</a>
			          </li>
			          <li class="{{($route == 'front.teacher')?'active':''}}">
			            <a href="{{route('front.teacher')}}">> {{__('navbar.menu.teachers')}}</a>
			          </li>
			          <li class="{{($route == 'front.pricing')?'active':''}}">
			            <a href="{{route('front.pricing')}}">> {{__('navbar.menu.pricing')}}</a>
			          </li>
			          <li class="{{($route == 'front.hiring')?'active':''}}">
			            <a href="{{route('front.hiring')}}">> {{__('navbar.menu.hiring')}}</a>
			          </li>
			          <li class="{{($route == 'front.blog')?'active':''}}" style="display: none;">
			            <a href="#">> {{__('navbar.menu.blog')}}</a>
			          </li>
				</ul>
			</div>
			<div class="col-xl-3 col-lg-4">
				<h3>Privacy Policies</h3>
				<ul>
					<li class="{{($route == 'front.privacy')?'active':''}}">
			            <a href="{{route('front.privacy')}}">> {{__('navbar.menu.privacy')}}</a>
			        </li>
				    <li class="{{($route == 'front.terms')?'active':''}}">
		                <a href="{{route('front.terms')}}">> {{__('navbar.menu.terms')}}</a>
		            </li>
		            <li class="{{($route == 'front.faqs')?'active':''}}">
		                <a href="{{route('front.faqs')}}">> Faqs</a>
		            </li>
				</ul>
			</div>
			<div class="col-xl-3 col-lg-4">
				<h3>Contact With US</h3>
				<ul class="social">
					@if ($global_system->facebook)
						<li>
							<a href="{{$global_system->facebook}}" target="_blank">
								<img src="{{asset('assets/img/facebook.png')}}" alt="Facebook">
							</a>
						</li>	
					@endif
					@if ($global_system->youtube)
						<li>
							<a href="{{$global_system->youtube}}" target="_blank">
								<img src="{{asset('assets/img/youtube.png')}}" alt="Youtube">
							</a>
						</li>
					@endif
					@if ($global_system->instagram)
						<li>
							<a href="{{$global_system->instagram}}" target="_blank">
								<img src="{{asset('assets/img/instagram.png')}}" alt="Instagram">
							</a>
						</li>
					@endif
				</ul>
				<div class="row m-b">
					<div class="col-lg-3">
						<img src="{{asset('assets/img/email.png')}}" alt="Email" class="img-fluid icon">
					</div>
					<div class="col-lg-9 p-l">
						@php
							$emails = explode(',',$global_system->emails);
						@endphp

						@foreach ($emails as $key => $email)
							@if ($key == 0 || $key == 1)
								<a href="mailto:{{$email}}">{{$email}}</a>	
							@endif
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-4">
				<h3>Become a <span>Go Amigo teacher!</span></h3>
				<p>Manage your schedules, combine classes with your regular job, or even work remotely. Earn money and be part of a change in future generations.</p>
				<a href="{{route('front.hiring')}}" class="btn btn-green">Learn more</a>
			</div>
		</div>
	</div>
	<p class="copy">{{$global_system->copyright}}</p>
</footer>