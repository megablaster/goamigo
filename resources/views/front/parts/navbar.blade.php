<div id="navbar">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-2 col-md-2 col-12">
        <a href="{{route('front.index')}}">
            <img src="{{route('get.image',$global_system->logo)}}" class="logo top">
        </a>
      </div>
      <div class="col-xl-6 col-md-2 col-1 sp">

        <ul class="top hidden-mobile">
          @php
            $route = Route::current()->getName();
          @endphp
          <li class="{{($route == 'front.index')?'active':''}}">
            <a href="{{route('front.index')}}">{{__('navbar.menu.home')}}</a>
          </li>
          <li class="{{($route == 'front.about')?'active':''}}">
            <a href="{{route('front.about')}}">{{__('navbar.menu.about')}}</a>
          </li>
          <li class="{{($route == 'front.philosophy')?'active':''}}">
            <a href="{{route('front.philosophy')}}">{{__('navbar.menu.philosophy')}}</a>
          </li>
          <li class="{{($route == 'front.teacher')?'active':''}}">
            <a href="{{route('front.teacher')}}">{{__('navbar.menu.teachers')}}</a>
          </li>
          <li class="{{($route == 'front.pricing')?'active':''}}">
            <a href="{{route('front.pricing')}}">{{__('navbar.menu.pricing')}}</a>
          </li>
          <li class="{{($route == 'front.hiring')?'active':''}}">
            <a href="{{route('front.hiring')}}">{{__('navbar.menu.hiring')}}</a>
          </li>
          <li class="{{($route == 'front.post')?'active':''}}">
            <a href="{{route('front.post')}}">{{__('navbar.menu.blog')}}</a>
          </li>
          <li class="close-burger">
            <a href="#">
              <i class="far fa-times-circle fa-2x"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="col-xl-3 col-md-7 col-7 text-right">

          @if (Auth::check())
            @if (auth()->user()->hasRole('admin'))
              <a href="{{route('admin.index')}}" class="btn btn-purple">{{__('navbar.menu.account')}}</a>
              <a href="{{route('logout.click')}}" class="btn btn-green">Cerrar sesión</a>
            @elseif(auth()->user()->hasRole('teacher'))
              <a href="{{route('admin.teacher.index')}}" class="btn btn-purple">{{__('navbar.menu.account')}}</a>
              <a href="{{route('logout.click')}}" class="btn btn-green">Cerrar sesión</a>
            @elseif(auth()->user()->hasRole('student'))
              <a href="{{route('admin.student.index')}}" class="btn btn-purple">{{__('navbar.menu.account')}}</a>
              <a href="{{route('logout.click')}}" class="btn btn-green">Cerrar sesión</a>
            @endif
          @else
            <a href="{{route('admin.index')}}" class="btn btn-green top">{{__('navbar.menu.login')}}</a>
          @endif
      </div>
      <div class="col-xl-1 col-md-1 col-1">
        <i class="fas fa-bars fa-3x menu-burger"></i>
      </div>
    </div>
  </div>
</div>
