@extends('layouts.app')

@section('title',$data['post']->name)

@push('css')
    <link rel="stylesheet" href="{{asset('script/owl/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('script/owl/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v12.0&appId=539808576195774&autoLogAppEvents=1" nonce="rX2Kvsro"></script>
@endpush

@push('js')
    <script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{asset('script/owl/owl.carousel.min.js')}}"></script>
    <script>
        $('.owl-gallery').owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3
                },
                1000:{
                    items:3
                }
            }
        });
    </script>
@endpush

@section('content')

    <div id="posts">
        <div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">
            @include('front.parts.navbar')
        </div>
        <section id="single-blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">

                       
                        <div class="item">
                            <div class="img" style="background-image: url('{{route('get.image',$data['post']->img)}}')"></div>
                            <h1>{{$data['post']->name}}</h1>
                            <ul class="list">
                                <li><i class="fas fa-user"></i> {{$data['post']->author->name}}</li>
                                <li><i class="fas fa-bookmark"></i> {{@$data['post']->category->name}}</li>
                                <li><i class="fas fa-clock"></i> {{$data['post']->getDate()}}</li>
                            </ul>
                            {!!$data['post']->description!!}
                        </div>
                    
                        <div class="owl-carousel owl-theme owl-gallery">

                            @foreach($data['post']->gallery as $gallery)
                                <div class="item">
                                    <a href="{{route('get.image',$gallery->img)}}" data-fancybox="gallery">
                                        <div class="img" style="background-image: url('{{route('get.image',$gallery->img)}}');"></div>
                                    </a>
                                </div>
                            @endforeach

                        </div>

                        <div id="comments">
                            <div class="fb-comments" data-href="{{url()->current()}}" data-width="100%" data-numposts="5"></div>
                        </div>

                    </div>
                    <div class="col-lg-3">

                        <div class="widget">
                            <form action="{{route('front.search')}}" method="get">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="search" placeholder="Search...">
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Search</button>
                            </form>
                        </div>

                        <div class="widget">
                            <h3>Last entries</h3>
                            <ul>
                                @foreach($data['posts'] as $random)
                                    <li><a href="{{route('front.single',[Str::slug($random->category->name),$random->url])}}">{{$random->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="widget">
                            <h3>Categories</h3>
                            <ul>
                                @foreach($data['categories'] as $category)
                                    <li><a href="{{route('front.category',$category->url)}}">{{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
