@extends('layouts.app')

@section('title','Teachers')

@push('css')
@endpush

@push('js')
@endpush

@section('content')

	<div id="pricing">

		<div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">

			@include('front.parts.navbar')

		</section>

		<section id="title">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 offset-lg-4">
						<h2>The ideal plan <span>for your children</span></h2>
					</div>
				</div>
			</div>
		</section>

		<section id="table">
			<div class="container">
				<div class="row">
					<div class="col-lg-4" style="position: relative;">
						<img src="{{asset('assets/img/pricing/ajolote-top.png')}}" alt="Ajolote" class="img-fluid ajolote">
						<div class="cube">
							<h3>Just one class</h3>
							<img src="{{asset('assets/img/pricing/1.png')}}" alt="One" class="img-fluid">
							<p>Give Spanish a chance. A 25 minute cultural connection for your child</p>
							<div class="price">
								<h4>$18 <span>usd</span></h4>
							</div>
							<p class="subtitle">Each class, one user</p>
							<a href="{{route('register.student')}}" class="btn btn-green btn-block">Buy package</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="cube">
							<h3>Save Five</h3>
							<img src="{{asset('assets/img/pricing/5.png')}}" alt="Five" class="img-fluid">
							<p>"Give your child a real connection with another language and a different culture"</p>
							<div class="price">
								<h4>$17 <span>usd</span></h4>
							</div>
							<p class="subtitle">Each class, one user</p>
							<a href="{{route('register.student')}}" class="btn btn-green btn-block">Buy package</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="cube">
							<h3>Best Deal</h3>
							<img src="{{asset('assets/img/pricing/10.png')}}" alt="Ten" class="img-fluid">
							<p>"Make serious progress with a new language and notice how your child grows."</p>
							<div class="price">
								<h4>$16 <span>usd</span></h4>
							</div>
							<p class="subtitle">Each class, one user</p>
							<a href="{{route('register.student')}}" class="btn btn-green btn-block">Buy package</a>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

@endsection