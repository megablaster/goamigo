@extends('layouts.app')

@section('title','Teachers')

@push('css')
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.theme.default.min.css')}}">
@endpush

@push('js')
	<script src="{{asset('script/owl/owl.carousel.min.js')}}"></script>
	<script src="{{asset('js/index.js')}}"></script>
@endpush

@section('content')

	<div id="teacher-section">

		<div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">

			@include('front.parts.navbar')

			<div class="container-fluid" style="margin-bottom: 40px;">
				<div class="col-12">
					<img src="{{asset('assets/img/teacher/header-teacher.png')}}" class="img-fluid">
				</div>
			</div>

		</section>

		<section id="info">
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<h2 class="title">Learning Spanish reinforces the integration of cultures and fosters respect among people.</h2>
					</div>
					<div class="col-xl-10 offset-xl-1">
						<p><span>We know that your children are the most important.</span> That is why for us the selection process of our team takes into account the following, in order to guarantee safety and the best education.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-6">
						<div class="item">
							<img src="{{asset('assets/img/teacher/icon-1.png')}}" alt="Bachelor's degree" class="img-fluid">
							<h2 class="purple">Bachelor's degree.</h2>
							<p class="purple">All of our teachers have a bachelor's degree or even a graduate degree in addition to previous experience working with children.</p>
						</div>
					</div>
					<div class="col-xl-6">
						<div class="item">
							<img src="{{asset('assets/img/teacher/icon-2.png')}}" alt="Personality" class="img-fluid">
							<h2>Personality.</h2>
							<p>Our team of teachers have great personalities that will make learning a time of entertainment and personal growth.</p>
						</div>
					</div>
					<div class="col-xl-6">
						<div class="item">
							<img src="{{asset('assets/img/teacher/icon-3.png')}}" alt="Values" class="img-fluid">
							<h2 class="green">Values.</h2>
							<p class="green">The entire Go Amigo team shares our values. Our teachers aim to make changes with small actions, sharing communication tools to make the world a better place.</p>
						</div>
					</div>
					<div class="col-xl-6">
						<div class="item">
							<img src="{{asset('assets/img/teacher/icon-4.png')}}" alt="Review of classes" class="img-fluid">
							<h2 class="purple">Review of classes.</h2>
							<p class="purple">Go Amigo classes are monitored to help make improvements where we need to. We are always seeking better results by creating the ideal environment.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

	<section id="teacher" class="p-80" style="background-image: url('{{asset('assets/img/home/bg-teacher.jpg')}}');">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-10 offset-xl-1 text-center">
					<h3 class="mb-30">Meet our teachers</h3>
					<div class="row">

						<div class="owl-carousel owl-theme owl-teachers">

							@foreach ($data['teachers'] as $teacher)
									
								<div class="item">
									<div class="teacher">
										<div class="img" style="background-image: url('{{route('get.image',$teacher->img)}}');">
										</div>
										<h4>{{$teacher->name}} {{$teacher->last_name}}</h4>
										<p style="color:black;margin:0;">{{$teacher->teacher->letter_presentation}}</p>
									</div>
								</div>

							@endforeach

						</div>

					</div>
					
				</div>
			</div>
		</div>
	</section>

@endsection