@extends('layouts.app')

@section('title','Posts')

@push('css')
@endpush

@push('js')
@endpush

@section('content')

    <div id="posts">
        <div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">
            @include('front.parts.navbar')
        </div>
        <section id="latest-blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">

                        @if(@$data['search'])
                            <h4 class="result-search">Showing <span class="count">{{$data['posts']->count()}}</span> from the search: <span>{{$data['search']}}</span></h4>
                        @endif

                        @if($data['posts']->count() > 0)

                            <div class="row">
                                @foreach($data['posts'] as $post)

                                    <div class="col-xl-6">
                                        <div class="item">
                                            <div class="img" style="background-image: url('{{route('get.image',$post->img)}}')"></div>
                                            <div class="text">
                                                <h2>{{$post->name}}</h2>
                                                <ul>
                                                    <li><i class="fas fa-user"></i> {{$post->author->name}}</li>
                                                    <li><i class="fas fa-bookmark"></i> {{@$post->category->name}}</li>
                                                    <li><i class="fas fa-clock"></i> {{$post->getDate()}}</li>
                                                </ul>
                                                <p>{{$post->extract}}</p>
                                            </div>
                                            <a
                                            href="{{route('front.single',[Str::slug($post->category->name), $post->url])}}" class="btn">Read more</a>
                                        </div>
                                    </div>
                                    
                                @endforeach
                            </div>

                        @else
                            <h3 class="text-center">There are no recent entries.</h3>
                        @endif

                    </div>
                    <div class="col-lg-3">

                        <div class="widget">
                            <form action="{{route('front.search')}}" method="get">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="search" placeholder="Search...">
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm btn-block">Search</button>
                            </form>
                        </div>

                        <div class="widget">
                            <h3>Last entries</h3>
                            <ul>
                                @foreach($data['randoms'] as $random)
                                    <li><a href="{{route('front.single',[Str::slug($random->category->name),$random->url])}}">{{$random->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>

                        <div class="widget">
                            <h3>Categories</h3>
                            <ul>
                                @foreach($data['categories'] as $category)
                                    <li><a href="{{route('front.category',$category->url)}}">{{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
