@extends('layouts.app')

@section('title','About')

@push('css')
@endpush

@push('js')
@endpush

@section('content')

	<div id="about">

		<div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">

			@include('front.parts.navbar')

			<section id="slide">
				<div class="container-fluid">
					<div class="row">

						<div class="col-xl-12">

							<div class="head" style="background-image: url('{{asset('assets/img/about/about-bg.jpg')}}');">
								
								<div class="text">
									<h1>A second language is a great skill to have in your back pocket</h1>
									<a href="{{route('register.student')}}" class="btn btn-purple">Book a free trial</a>
								</div>
						
							</div>

						</div>
						
					</div>
				</div>
			</section>

		</section>

		<section id="info">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-sm-6">
						<div class="item">
							<img src="{{asset('assets/img/about/info1.png')}}" alt="Info1" class="img-fluid">
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="item">
							<img src="{{asset('assets/img/about/info2.png')}}" alt="Info1" class="img-fluid">
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="item">
							<img src="{{asset('assets/img/about/info3.png')}}" alt="Info1" class="img-fluid">
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="item">
							<img src="{{asset('assets/img/about/info4.png')}}" alt="Info1" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="info2">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2>Get to know other cultures, in addition to the already <span>known benefits of a second language</span></h2>
					</div>

					<div class="col-lg-4 col-sm-6 item">
						<img src="{{asset('assets/img/about/icon1.png')}}" alt="Personal Growth" class="img-fluid">
						<h3>Personal Growth</h3>
					</div>

					<div class="col-lg-4 col-sm-6 item">
						<img src="{{asset('assets/img/about/icon2.png')}}" alt="Open doors to other countries" class="img-fluid">
						<h3>Open doors to other countries</h3>
					</div>

					<div class="col-lg-4 col-sm-6 item">
						<img src="{{asset('assets/img/about/icon3.png')}}" alt="Cultural awareness" class="img-fluid">
						<h3>Cultural awareness</h3>
					</div>

					<div class="col-lg-4 col-sm-6 item">
						<img src="{{asset('assets/img/about/icon4.png')}}" alt="Interpersonal Connections" class="img-fluid">
						<h3>Interpersonal Connections</h3>
					</div>

					<div class="col-lg-4 col-sm-6 item">
						<img src="{{asset('assets/img/about/icon5.png')}}" alt="Communcation Skills" class="img-fluid">
						<h3>Communication Skills</h3>
					</div>

					<div class="col-lg-4 col-sm-6 item">
						<img src="{{asset('assets/img/about/icon6.png')}}" alt="Enjoyable" class="img-fluid">
						<h3>Enjoyable</h3>
					</div>

				</div>
			</div>
		</section>

		<section id="info3" style="background-image: url('{{asset('assets/img/home/bg-info2.jpg')}}');">
			<div class="container">
				<div class="row">
					<div class="col-xl-5">
						<img src="{{asset('assets/img/about/ajolote-world.png')}}" alt="Ajolote" class="img-fluid">
					</div>
					<div class="col-xl-5">
						<div class="text">
							<h3>We will use language to expose young generations to a new and exciting world, that is not so far away</h3>
							<p>Through learning a little about other countries we hope that we can reinforce the integration of cultures and foster respect among people.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

@endsection