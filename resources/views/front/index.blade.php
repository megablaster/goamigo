@extends('layouts.app')

@section('title','Homepage')

@push('css')
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.6/sweetalert2.min.css">
	<link rel="stylesheet" href="{{asset('script/password/jquery.passwordRequirements.css')}}">
@endpush

@push('js')
	<script src="{{asset('script/owl/owl.carousel.min.js')}}"></script>
	<script src="{{asset('js/index.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.6/sweetalert2.min.js"></script>
	<script src="{{asset('script/password/jquery.passwordRequirements.min.js')}}"></script>
	<script>
		$(document).ready(function(){
			$("#password").passwordRequirements({
				numCharacters: 6,
				useLowercase:true,
				useUppercase:true,
				useNumbers:true,
				useSpecial:true
		    });
		});
	</script>

	@if (Session::has('success'))
		<script>
			Swal.fire({
			  title: 'Cuenta creada con exito',
			  text: "{{Session::get('success')}}",
			  icon: 'success',
			  showCancelButton: false,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ok, gracias'
			});
		</script>
	@endif

	@if($global_system->type_app == 'campaing')
        <script>
            $(document).ready(function(){
                setTimeout(function(){
                    $('#click-fancybox').trigger('click');
                },2000);

            });
        </script>
    @endif

    <a href="#fancybox" data-fancybox style="display: none;" id="click-fancybox"></a>

    <!--Registro-->
    <div id="fancybox">
        <section id="register-teacher">
            <div class="container">
                <div class="row">
                	<div class="col-lg-6 p-0">
                		<img src="{{asset('assets/img/img-fancy1.jpg')}}" class="img-fluid">
                	</div>
                    <div class="col-lg-6">
                        <form action="{{route('save.student')}}" method="POST">
                            @csrf
                            <!--campaing or live-->
                            <input type="hidden" name="type" value="campaing">
                            <div class="row">
                                <div class="col-xl-12">
                                	<h1 style="display: none;">{!!__('register_student.title')!!}</h1>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            @foreach ($errors->all() as $error)
                                                {{ $error }}
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                                <div class="col-xl-12">
                                    <div class="form-group">
                                        <input type="email"  class="form-control" placeholder="{{__('register_student.email')}}" name="email" value="{{@old('email')}}" required>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                	 <div class="form-group">
	                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
	                                    @error('password')
	                                        <span class="invalid-feedback" role="alert">
	                                            <strong>{{ $message }}</strong>
	                                        </span>
	                                    @enderror
	                                </div>
                                </div>
                                 <div class="col-xl-12" style="margin-bottom: 10px;">
                                 	 <div class="form-check">
									  <input class="form-check-input" name="privacy" type="checkbox" id="flexCheckDefault">
									  <label class="form-check-label" for="flexCheckDefault">
									    {{__('register_teacher.privacy')}} <a style="text-decoration: underline;color: #af5db0;font-size: 17px;font-family: 'ProgramOT';" href="{{route('front.privacy')}}">{{__('register_teacher.text_privacy')}}</a>
									  </label>
									</div>
                                 </div>
                                 <div class="col-xl-12" style="margin-bottom: 10px;">
                                 	{!! htmlFormSnippet() !!}
                                 </div>
                            </div>
                            <button type="submit" class="btn btn-submit">{{__('register_student.button')}}</button>
                        </form>

                    </div>

                </div>
            </div>
        </section>
    </div>

@endpush

@section('content')

	<div id="home">

		<div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">

			@include('front.parts.navbar')

			<section id="slide">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12">

							<div class="owl-carousel owl-theme owl-slide">

								@foreach ($data['slides'] as $slide)
									<div class="item" style="background-image: url('{{route('get.image',$slide->img)}}');">
										<div class="text">
											<h2>{{$slide->title}}</h2>
											<p>{{$slide->description}}</p>
											@if ($slide->button && $slide->url)
												<a href="{{$slide->url}}" class="btn btn-yellow">{{$slide->button}}</a>
											@endif
										</div>
									</div>
								@endforeach

							</div>

						</div>
					</div>
				</div>
			</section>

			<section id="info" class="p-80">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-4 offset-xl-2">
							<img src="{{asset('assets/img/home/info-bg.png')}}" class="img-fluid">
						</div>
						<div class="col-xl-5 text-center">
							<div class="text">
								<h3>{!!__('home.title')!!}</h3>
								<div class="row">
									<div class="col-xl-6 col-lg-4 col-md-6">
										<div class="cube">
											<img src="{{asset('assets/img/home/vineta-pink.png')}}" class="img-fluid"> Native Latin teachers
										</div>
									</div>
									<div class="col-xl-6 col-lg-4 col-md-6">
										<div class="cube">
											<img src="{{asset('assets/img/home/vineta-red.png')}}" class="img-fluid"> Cultural topics
										</div>
									</div>
									<div class="col-xl-6 col-lg-4 col-md-6">
										<div class="cube">
											<img src="{{asset('assets/img/home/vineta-blue.png')}}" class="img-fluid"> Easy to use
										</div>
									</div>
									<div class="col-xl-6 col-lg-4 col-md-6">
										<div class="cube">
											<img src="{{asset('assets/img/home/vineta-green.png')}}" class="img-fluid"> Observable results
										</div>
									</div>
								</div>
								<a href="{{route('front.about')}}" class="btn btn-green">About us</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>

		<section id="info2" class="p-80" style="background-image: url('{{asset('assets/img/home/bg-info2.jpg')}}');">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<h3>Why should your children <span>learn Spanish with us?</span></h3>
						<div class="owl-carousel owl-theme owl-info2">
							<div class="item">
								<div class="text">
									<img src="{{asset('assets/img/home/icon1.png')}}" alt="Schedueles" class="img-fluid">
									<h4 class="blue">Schedueles</h4>
									<p>Choose your teacher and pick a time that works for you, any time of the week.</p>
								</div>
							</div>
							<div class="item">
								<div class="text">
									<img src="{{asset('assets/img/home/icon2.png')}}" alt="Schedueles" class="img-fluid">
									<h4 class="green">Easy</h4>
									<p>No software required. Just book a lesson for your child, open the link and enjoy.</p>
								</div>
							</div>
							<div class="item">
								<div class="text">
									<img src="{{asset('assets/img/home/icon3.png')}}" alt="Schedueles" class="img-fluid">
									<h4 class="orange">Culture</h4>
									<p>For us, language is a connection to our culture, which builds bonds and integration between generations.</p>
								</div>
							</div>
							<div class="item">
								<div class="text">
									<img src="{{asset('assets/img/home/icon4.png')}}" alt="Schedueles" class="img-fluid">
									<h4 class="blue-light">Connection</h4>
									<p>Give your child more and better ties to their future travels, jobs and their world in general.</p>
								</div>
							</div>
							<div class="item">
								<div class="text">
									<img src="{{asset('assets/img/home/icon5.png')}}" alt="Schedueles" class="img-fluid">
									<h4 class="blue-light">Respect</h4>
									<p>Learning Spanish reinforces the integration of cultures and fosters respect among people.</p>
								</div>
							</div>
							<div class="item">
								<div class="text">
									<img src="{{asset('assets/img/home/icon6.png')}}" alt="Schedueles" class="img-fluid">
									<h4 class="blue-light">Skills</h4>
									<p>Giving new generations the communication tools to better understand the world and make it a better place.</p>
								</div>
							</div>
							<div class="item">
								<div class="text">
									<img src="{{asset('assets/img/home/icon7.png')}}" alt="Schedueles" class="img-fluid">
									<h4 class="blue-light">Community</h4>
									<p>We all have a lot to teach and to contribute.</p>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>

		<section id="why" class="p-80">
			<div class="container-fluid">
				<div class="row">

					<div class="col-xl-3 offset-xl-1">
						<h3><img src="{{asset('assets/img/home/ajolote-bg.png')}}" alt="Ajolote" class="ajolote"> Why <span>Spanish?</span></h3>
					</div>
					<div class="col-xl-8">
						<div class="row">
							<div class="col-lg-4">
								<div class="number">
								   <h4>21</h4>
								   <h5>Countries</h5>
								   <p>that have Spanish as an official language</p>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="number">
								   <h4>3rd</h4>
								   <h5>Most popular</h5>
								   <p>language in the world</p>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="number">
								   <h4>580</h4>
								   <h5>Million</h5>
								   <p>people speak Spanish worldwide</p>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>

		@if (!$data['teachers']->isEmpty())

			<section id="teacher" class="p-80" style="background-image: url('{{asset('assets/img/home/bg-teacher.jpg')}}');">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-10 offset-xl-1 text-center">
							<h3>Our teachers</h3>
							<p>More than teachers, our team want to make a change in future generations</p>
							<div class="owl-carousel owl-theme owl-teachers">

								@foreach ($data['teachers'] as $teacher)

									<a href="{{route('register.student')}}">
										<div class="item">
											<div class="teacher">
												<div class="img" style="background-image: url('{{route('get.image',$teacher->img)}}');">
												</div>
												<h4>{{$teacher->name}} {{$teacher->last_name}}</h4>
												<p style="color:black;margin:0;">{{$teacher->teacher->letter_presentation}}</p>
											</div>
										</div>
									</a>

								@endforeach

							</div>

							<a href="{{route('register.student')}}" class="btn btn-yellow">Book a free trial</a>

						</div>
					</div>
				</div>
			</section>

		@endif
	</div>

@endsection
