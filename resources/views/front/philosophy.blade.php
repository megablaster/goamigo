@extends('layouts.app')

@section('title','Teachers')

@push('css')
@endpush

@push('js')
@endpush

@section('content')

	<div id="philosophy-section">

		<div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">

			@include('front.parts.navbar')

			<section id="slide">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">

							<div class="head" style="background-image: url('{{asset('assets/img/philosophy/header.jpg')}}');">
							
								<div class="text">
									<h1>A second language will open their world and many doors</h1>
									<a href="{{route('register.student')}}" class="btn btn-yellow">Book a free trial</a>
								</div>
							
							</div>

						</div>
					</div>
				</div>
			</section>

			<section id="green">
				<div class="container">
					<div class="row">
						<div class="col-lg-4">
							<img src="{{asset('assets/img/philosophy/promo1.png')}}" alt="Promotion" class="img-fluid">
						</div>
						<div class="col-lg-5">
							<div class="text">
								<h3>Promotion of respect between people</h3>
								<p>We are living in a globalized world, a chaotic world. no matter how hard we try, we are not going to improve it in a short time, but we can work to generate small changes.</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5 order-xl-1 order-lg-1 order-2">
							<div class="text">
								<h3>Exposing the younger generations to new cultures</h3>
								<p>At GoAmigo we want to encourage new generations to connect with other cultures, and through another language understand the importance of respect and integration.</p>
							</div>
						</div>
						<div class="col-lg-5 offset-lg-1 order-xl-2 order-lg-2 order-1">
							<img src="{{asset('assets/img/philosophy/promo2.png')}}" alt="Promotion" class="img-fluid">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5">
							<img src="{{asset('assets/img/philosophy/promo3.png')}}" alt="Promotion" class="img-fluid">
						</div>
						<div class="col-lg-5">
							<div class="text">
								<h3>Reinforce integration between cultures</h3>
								<p>We want the new generations to have communication tools through language to better understand this world and make it a better place.</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5 order-xl-1 order-lg-1 order-2">
							<div class="text">
								<h3>Learn about other countries that are not so far</h3>
								<p>We want leisure moments to inspire growth through entertainment, and that generating income can be the consequence of working for a change, thanks to an effort for the good, that we win for good actions.</p>
							</div>
						</div>
						<div class="col-lg-4 offset-lg-1 order-xl-2 order-lg-2 order-1">
							<img src="{{asset('assets/img/philosophy/promo4.png')}}" alt="Promotion" class="img-fluid">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 offset-lg-1">
							<img src="{{asset('assets/img/philosophy/filosofy-pet.png')}}" alt="Promotion" class="img-fluid">
						</div>
						<div class="col-lg-5">
							<div class="text">
								<p style="font-weight: 500;font-size: 26px;}">We all have a lot to teach and to contribute.</p>
							</div>
						</div>
					</div>
				</div>
			</section>

		</section>

	</div>

@endsection