@extends('layouts.app')

@section('title','Teachers')

@push('css')
	<link rel="stylesheet" href="{{asset('script/password/jquery.passwordRequirements.css')}}">
@endpush

@push('js')
	<script src="{{asset('script/password/jquery.passwordRequirements.min.js')}}"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script>
		$(".pr-password").passwordRequirements({
			numCharacters: 6,
  			useLowercase:true,
  			useUppercase:true,
  			useNumbers:true,
  			useSpecial:true
        });
	</script>
	@if ($errors->any())
		<script>
			$(document).ready(function(){
				var ele =  $('#part');
			    $(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
			});
		</script>
	@endif

	@if (Session::has('success'))
	    <script>
	    	Swal.fire({
			  position: 'center',
			  icon: 'success',
			  title: 'Registro exitoso',
			  text: '{!! Session::get('success') !!}',
			  showConfirmButton: false,
			  timer: 3000
			});
	    </script>
	@endif

@endpush

@section('content')

	<div id="hiring-section">

		<div class="header" style="background-image:url('{{asset('/assets/img/home/bg-header.jpg')}}');">

			@include('front.parts.navbar')

			<section id="slide">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">

							<img src="{{asset('assets/img/hiring/filosofia.png')}}" class="img-fluid">

						</div>
					</div>
				</div>
			</section>

		</section>

		<section id="register">
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="card">
						    	<div class="card-body">
						    	<h1>Se parte de la generación que trabaja, administra su tiempo y es parte de un cambio, compartamos idiomas, cultura y conocimiento.</h1>
						        <p>El candidato ideal será...</p>
						        <div class="row">
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon1.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Nativo Hispanoablante</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon2.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Haber trabajado antes con niños entre 5 y 14 años</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon3.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Actitud enérgica y positiva</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon4.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Disponibilidad de horario</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon5.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Facilidad de palabra y habilidades de exposición</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon6.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Conexión a internet que asegure cero interrupciones</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon7.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Carrera universitaria terminada</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon8.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Contar con un espacio designado para tus clases</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon9.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Extrovertidos, comprometidos y amables</h4>
						        			</div>
						        		</div>
						        	</div>
						        	<div class="col-md-6 item">
						        		<div class="row">
						        			<div class="col-xl-3">
						        				<img src="{{asset('assets/img/hiring/icon10.png')}}" class="img-fluid">
						        			</div>
						        			<div class="col-xl-9">
						        				<h4>Ser puntual y organizado</h4>
						        			</div>
						        		</div>
						        	</div>
						        </div>
					        	<h2 id="part">Se parte del equipo de Go Amigo</h2>
					        	@if ($errors->any())
							        <div class="alert alert-danger">
						                @foreach ($errors->all() as $error)
						                    {{ $error }}
						                @endforeach
							        </div>
								@endif
					        	<div class="row">
						        	<form action="{{ route('save.teacher') }}" method="post" class="col-xl-12" enctype="multipart/form-data">
						        		<div class="row">
							        		@csrf
						        			<div class="col-xl-6">
						        				<div class="form-group">
						        					<label>Nombre</label>
													<input type="text"  class="form-control" name="name" value="{{old('name')}}" required>
							        			</div>
						        			</div>
						        			<div class="col-xl-6">
						        				<div class="form-group">
						        					<label>Apellidos</label>
							        				<input type="text" name="last_name" class="form-control" value="{{old('last_name')}}">
							        			</div>
						        			</div>
						        			<div class="col-xl-4">
						        				<div class="form-group">
						        					<label>Correo electrónico</label>
							        				<input type="email" class="form-control" name="email" value="{{old('email')}}">
							        			</div>
						        			</div>
						        			<div class="col-xl-4">
						        				<div class="form-group">
						        					<label>Teléfono</label>
							        				<input type="number" class="form-control" name="phone" value="{{old('phone')}}">
							        			</div>
						        			</div>
						        			<div class="col-xl-4">
						        				<div class="form-group">
						        					<label>País</label>
							        				<select name="country" class="form-control select-search">
														@foreach ($data['countries'] as $country)
															<option value="{{$country}}" {{old('country') == $country ? 'selected':''}}>{{$country}}</option>
														@endforeach
													</select>
							        			</div>
						        			</div>
                                            <div class="col-xl-12">
                                                <div class="form-group">
                                                    <label>Contraseña</label>
                                                    <input type="password"  class="form-control pr-password" value="{{old('password')}}" name="password" required>
                                                    <p style="font-size: 16px;">Tu contraseña puede ser cualquier combinación de letras, números y símbolos (#?!/+:;@$%^&_*-_"()=¡&).</p>
                                                </div>
                                            </div>
						        			<div class="col-xl-4">
						        				<label for="file" class="file">
						        					<img src="{{asset('assets/img/hiring/file.png')}}" class="img-fluid">
						        					<p>Añadir CV</p>
						        				</label>
						        				<input type="file" id="file" name="file" accept=
                                                "application/pdf" value="{{old('file')}}" required><br>
                                                <small>*El archivo debe tener el formato pdf y un peso máximo de 5mb.</small>
						        			</div>
						        			<div class="col-xl-8">
						        				<label>¿Cuentanos porqué te gustaría trabajar con nosotros?</label>
						        				<textarea name="experience" class="form-control" rows="10" required>{{old('experience')}}</textarea><br>
						        				<div class="form-check">
												  <input class="form-check-input" name="privacy" type="checkbox" id="flexCheckDefault">
												  <label class="form-check-label" for="flexCheckDefault">
												  Es necesario que aceptes el <a style="text-decoration: underline;color: #af5db0;font-size: 17px;font-family: 'ProgramOT';" href="{{route('front.privacy')}}">Aviso de privacidad.</a>
												  </label>
												</div>
												{!! htmlFormSnippet() !!}
						        				<button type="submit" class="btn btn-submit btn-lg">Registrarme</button>
						        			</div>
					        			</div>
					        		</form>
			        			</div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</div>

@endsection
