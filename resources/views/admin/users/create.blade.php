@extends('layouts.admin')

@section('title')
	{{__('user.create.title')}}
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('script/select/bootstrap-select.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
		        <div class="card-body">

		        	@if ($errors->any())
					    <div class="col-md-12">
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                    <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    </div>
					@endif
		            
		            <form action="{{route('users.store')}}" method="post">
        				@csrf
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>{{__('user.create.subtitle')}}</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.name')}}</label>
			            			<input type="text" name="name" class="form-control {{($errors->first('name'))? 'invalid':''}}" value="{{old('name')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.last_name')}}</label>
			            			<input type="text" name="last_name" class="form-control {{($errors->first('last_name'))? 'invalid':''}}" value="{{old('last_name')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.email')}}</label>
			            			<input type="email" name="email" class="form-control {{($errors->first('email'))? 'invalid':''}}" value="{{old('email')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.gender')}}</label>
			            			<select name="gender" class="form-control" required>
		            					<option value="male">Masculino</option>
		            					<option value="female">Femenino</option>
		            					<option value="undefined">Sin especificar</option>
			            			</select>
			            		</div>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.phone')}}</label>
			            			<input type="tel" name="phone" class="form-control {{($errors->first('phone'))? 'invalid':''}}" value="{{old('phone')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.city')}}</label>
			            			<input type="text" name="city" class="form-control" value="{{old('city')}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.country')}}</label>
			            			<select name="country" class="select form-control" data-live-search="true" value="{{old('country')}}">
			            				@foreach ($data['countries'] as $country)
			            					<option value="{{$country}}">{{$country}}</option>
			            				@endforeach
			            			</select>
			            		</div>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.role')}}</label>
			            			<select name="rol"  class="form-control" value="{{old('rol')}}">
			            				<option value="admin">{{__('general.role.admin')}}</option>
										<option value="manager">{{__('general.role.manager')}}</option>
										<option value="teacher">{{__('general.role.teacher')}}</option>
										<option value="student">{{__('general.role.student')}}</option>
			            			</select>
			            		</div>
			            	</div>
			            	<div class="col-12">
			            		<h4>{{__('user.create.password_title')}}</h4><hr>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.password')}}</label>
			            			<input type="password" name="password" class="form-control" {{($errors->first('password'))? 'invalid':''}} required>
			            		</div>
			            	</div>
			            	<div class="col-xl-4">
			            		<div class="form-group">
			            			<label>{{__('user.create.password_repeat')}}</label>
			            			<input type="password" name="password_confirmation" class="form-control" {{($errors->first('password_confirmation'))? 'invalid':''}} required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<span class="badge badge-warning" style="font-size: 14px;">La contraseña debe cumplir con mínimo 6 caracteres, 1 mayúscula, 1 minúscula y un signo.</span>
			            	</div><br><br>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('users.index')}}" class="btn btn-danger">{{__('general.cancel')}}</a>
			            		<button type="submit" class="btn btn-success">{{__('user.create.add')}}</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/select/bootstrap-select.min.js')}}"></script>
	<script>
		$('.select').selectpicker();
	</script>
@endpush