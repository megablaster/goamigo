@extends('layouts.admin')

@section('title')
	{{__('user.title')}}
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">

			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif

			<div class="card">
		        <div class="card-body">

		            <h4 class="card-title">{{__('user.all')}}</h4>
		            <div class="table-responsive">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
                            <thead>
                                <tr>
									<th>Estatus</th>
                                    <th>{{__('user.table.name')}}</th>
                                    <th>{{__('user.table.email')}}</th>
                                    <th width="1em">{{__('user.table.role')}}</th>
                                    <th width="1em">Destacar</th>
                                    <th width="1em">{{__('user.table.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>

                            	@foreach ($data['users'] as $user)
                            		<tr>  
										<td>{!!$user->getStep()!!}</td>
	                                    <td>{{$user->name}} {{$user->last_name}}</td>
	                                    <td>{{$user->email}}</td>
	                                    <td>{!!$user->getRole()!!}</td>
	                                    <td>
	                                    	@if($user->hasRole('teacher'))

	                                    		@if ($user->teacher->featured == 0 && $user->teacher->step == 'third' && $user->teacher->approve_info == 1)
	                                    			<form action="{{route('users.featured')}}" method="POST">
		                                    			@csrf
		                                    			<input type="hidden" name="user_id" value="{{$user->id}}">
		                                    			<input type="hidden" name="featured" value="1">
		                                    			<button type="submit" class="btn btn-sm btn-block btn-success">Destacar</button>
		                                    		</form>
		                                    	@elseif($user->teacher->featured == 1 && $user->teacher->step == 'third' && $user->teacher->approve_info == 1)
		                                    		<form action="{{route('users.featured')}}" method="POST">
		                                    			@csrf
		                                    			<input type="hidden" name="user_id" value="{{$user->id}}">
		                                    			<input type="hidden" name="featured" value="0">
		                                    			<button type="submit" class="btn btn-sm btn-block btn-danger">Quitar</button>
		                                    		</form>
		                                    	@else
	                                    		@endif
	                                    		
	                                    	@endif
	                                    </td>
	                                    <td>
	                                    	<a href="{{route('users.edit',$user->id)}}" class="btn btn-sm btn-info">{{__('general.edit')}}</a>
	                                    	@if (auth()->user()->id != $user->id)
												@if ($user->suspend == true)
		                                    		<a href="{{route('users.suspend',$user->id)}}" class="btn btn-sm btn-warning">Habilitar</a>
		                                    	@else
		                                    		<a href="{{route('users.suspend',$user->id)}}" class="btn btn-sm btn-warning">Suspender</a>
		                                    	@endif
	                                    	@endif
	                                    	<a href="#" class="btn btn-sm btn-danger delete" data-id="{{$user->id}}">{{__('general.delete')}}</a>
	                                    	<form action="{{route('users.destroy',$user->id)}}" method="post" style="display: inline-block;" id="delete-{{$user->id}}">
	                                    		@csrf
	                                    		@method('DELETE')
	                                    	</form>
	                                    </td>
	                                </tr>	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>

                    <a href="{{route('users.create')}}" class="btn btn-info">{{__('user.add')}}</a>

                    <div id="add-user" style="width: 100%;display: none;max-width: 750px;">

						@if ($errors->any())
						    <div class="col-md-12">
						        <div class="alert alert-danger">
						            <ul>
						                @foreach ($errors->all() as $error)
						                    <li>{{ $error }}</li>
						                @endforeach
						            </ul>
						        </div>
						    </div>
						@endif

            			<form action="{{route('users.store')}}" method="post">
            				@csrf
				            <div class="row">
				            	<div class="col-12">
				            		<h3><strong>Información personal</strong></h3><hr>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Nombre</label>
				            			<input type="text" name="name" class="form-control" required>
				            		</div>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Apellido</label>
				            			<input type="text" name="last_name" class="form-control" required>
				            		</div>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Correo electrónico</label>
				            			<input type="text" name="email" class="form-control" required>
				            		</div>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Genero</label>
				            			<select name="gender" class="form-control" required>
			            					<option value="male">Masculino</option>
			            					<option value="female">Femenino</option>
			            					<option value="undefined">Sin especificar</option>
				            			</select>
				            		</div>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Teléfono</label>
				            			<input type="tel" name="phone" class="form-control" required>
				            		</div>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Ciudad</label>
				            			<input type="text" name="city" class="form-control">
				            		</div>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>País</label>
				            			<input type="text" name="country" class="form-control">
				            		</div>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Rol</label>
				            			<select name="rol"  class="form-control">
				            				<option value="admin">Administración</option>
											<option value="manager">Gerente</option>
											<option value="teacher">Maestro</option>
											<option value="student">Estudiante</option>
				            			</select>
				            		</div>
				            	</div>
				            	<div class="col-12">
				            		<h4>Asignar contraseña</h4><hr>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Contraseña</label>
				            			<input type="password" name="password" class="form-control" required>
				            		</div>
				            	</div>
				            	<div class="col-xl-6">
				            		<div class="form-group">
				            			<label>Repetir contraseña</label>
				            			<input type="password" name="repeat_password" class="form-control" required>
				            		</div>
				            	</div>
				            </div>
				            <div class="row">
					            <div class="col-xl-12">
					            	<a id="close-modal" class="btn btn-danger" style="color:#fff;">Cancelar</a>
				            		<button type="submit" class="btn btn-success">Agregar usuario</button>
				            	</div>
			            	</div>
			            </form>
            		
                    </div>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
    	//Cerrar modal
    	$('#close-modal').click(function(){
		  $.fancybox.close();
		});

		//Preguntar si desea borrar
		$('body').on('click','.delete', function(e){

			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: '¿Estas seguro?',
			  text: "Esta a apunto de eliminar el usuario.",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cancelar',
			  confirmButtonText: '¡Si, eliminar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#delete-'+id).submit();
			  }
			})

		});
		
    </script>	
@endpush