@extends('layouts.admin')

@section('title')
    Crear entrada
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/ui/trumbowyg.min.css">
@endpush

@section('content')

    <div class="row">
        <div class="col-xl-12">
            @if ($errors->any())
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {!! Session::get('success') !!}
                </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <form action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <h3><strong>Información</strong></h3><hr>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>{{__('user.edit.name')}}</label>
                                    <input type="text" name="name" class="form-control" value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Categoría</label>
                                    <select name="category_id" class="form-control">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <textarea id="trumbowyg" name="description" class="form-control" rows="10">{{old('description')}}</textarea>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Portada</label><br>
                                    <p style="display: inline-block;font-size: 13px;background-color:#ff9129;color:white;padding:5px;">La imagen debe tener un tamaño de 900x543px y un peso máximo 512kb.</p>
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <h3>Configuración SEO</h3>
                                <div class="form-group">
                                    <label>Palabras clave</label>
                                    <textarea name="keywords" class="form-control" rows="3">{{old('keywords')}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Meta descripción</label>
                                    <textarea name="meta_description" class="form-control" rows="3">{{old('meta_description')}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">
                                <a href="{{route('admin.index')}}" class="btn btn-danger">{{__('user.edit.exit')}}</a>
                                <button type="submit" class="btn btn-success">{{__('user.edit.save')}}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/trumbowyg.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/langs/es.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/cleanpaste/trumbowyg.cleanpaste.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/pasteimage/trumbowyg.pasteimage.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/pasteembed/trumbowyg.pasteembed.min.js"></script>
    <script src="//rawcdn.githack.com/RickStrahl/jquery-resizable/0.35/dist/jquery-resizable.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/resizimg/trumbowyg.resizimg.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/colors/trumbowyg.colors.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/noembed/trumbowyg.noembed.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/fontsize/trumbowyg.fontsize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/base64/trumbowyg.base64.min.js"></script>
    <script>
        $('#trumbowyg').trumbowyg({
            lang: 'es',
            removeformatPasted: true,
            btns: [
                ['foreColor', 'backColor'],
                ['fromatting','strong', 'em', 'del','fontsize','fontfamily'],
                ['noembed'],
                ['link'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['unorderedList', 'orderedList'],
                ['horizontalRule'],
                ['removeformat'],
                ['fullscreen'],
                ['base64']
            ],
            plugins: {
                resizimg: {
                    minSize: 64,
                    step: 16,
                }
            },
            autogrow: true,
        });
    </script>
@endpush
