@extends('layouts.admin')

@section('title')
	Editar entrada
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/ui/trumbowyg.min.css">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			@if ($errors->any())
			    <div class="col-md-12">
			        <div class="alert alert-danger">
			            <ul>
			                @foreach ($errors->all() as $error)
			                    <li>{{ $error }}</li>
			                @endforeach
			            </ul>
			        </div>
			    </div>
			@endif

			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
			<div class="card">
		        <div class="card-body">
		            <form action="{{route('posts.update',$data['post']->id)}}" method="post" enctype="multipart/form-data">
		            	@csrf
		            	{{ method_field('PATCH') }}
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>Información</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('user.edit.name')}}</label>
			            			<input type="text" name="name" class="form-control" value="{{$data['post']->name}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Url amigable</label>
			            			<input type="text" name="url" class="form-control" value="{{$data['post']->url}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Categoría</label>
			            			<select name="category_id" class="form-control">
			            				@foreach($data['categories'] as $category)
			            					<option value="{{$category->id}}">{{$category->name}}</option>
			            				@endforeach
			            			</select>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Descripción</label>
                                    <div id="toolbar-container"></div>
									<textarea id="trumbowyg" name="description">{!!$data['post']->description!!}</textarea>
			            		</div>
			            	</div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Portada</label><br>
                                    <img src="{{route('get.image',$data['post']->img)}}" class="img-fluid" style="width:350px;"><br>
                                    <p style="display: inline-block;font-size: 13px;background-color:#ff9129;color:white;padding:5px;">La imagen debe tener un tamaño de 900x543px y un peso máximo 512kb.</p>
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
			            	<div class="col-xl-12">
                                <h3>Configuración SEO</h3>
			            		<div class="form-group">
			            			<label>Palabras clave</label>
                                    <textarea name="keywords" class="form-control" rows="3">{{$data['post']->keywords}}</textarea>
			            		</div>
                                <div class="form-group">
                                    <label>Meta descripción</label>
                                    <textarea name="meta_description" class="form-control" rows="3">{{$data['post']->meta_description}}</textarea>
                                </div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('admin.index')}}" class="btn btn-danger">{{__('user.edit.exit')}}</a>
			            		<button type="submit" class="btn btn-success">{{__('user.edit.save')}}</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>

		    <div class="card">
		        <div class="card-body">
		            <form action="{{route('upload.gallery')}}" method="post" enctype="multipart/form-data">
		            	@csrf
		            	<input type="hidden" name="post_id" value="{{$data['post']->id}}">
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>Galería</strong></h3><hr>
			            	</div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Foto</label><br>
                                    <p style="display: inline-block;font-size: 13px;background-color:#ff9129;color:white;padding:5px;">Las imagenes debe tener un tamaño de 550x550px y un peso máximo 512kb.</p>
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
			            		<button type="submit" class="btn btn-success">Subir foto</button>
			            	</div>
		            	</div><hr>
		            </form>
		            	<div class="row">
		            		<div class="col-xl-12">

		            			<div class="owl-carousel owl-theme owl-gallery">

		            				@foreach($data['post']->gallery as $gallery)
		            					<div class="item">
		            						<div class="img" style="background-image: url('{{route('get.image',$gallery->img)}}');">
		            							<form action="{{route('destroy.gallery')}}" method="post">
		                                    		@csrf
		                                    		<input type="hidden" name="id" value="{{$gallery->id}}">
		                                    		<input type="hidden" name="post_id" value="{{$data['post']->id}}">
		                                    		<button type="submit" class="btn btn-sm delete">Borrar</button>
		                                    	</form>
		            						</div>
		            					</div>
		            				@endforeach

		            			</div>

		            		</div>
		            	</div>
		        </div>
		    </div>
	    </div>
    </div>
@endsection

@push('js')
	<script src="{{asset('script/owl/owl.carousel.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/trumbowyg.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/langs/es.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/cleanpaste/trumbowyg.cleanpaste.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/pasteimage/trumbowyg.pasteimage.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/pasteembed/trumbowyg.pasteembed.min.js"></script>
	<script src="//rawcdn.githack.com/RickStrahl/jquery-resizable/0.35/dist/jquery-resizable.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/resizimg/trumbowyg.resizimg.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/colors/trumbowyg.colors.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/noembed/trumbowyg.noembed.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/fontsize/trumbowyg.fontsize.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.25.1/plugins/base64/trumbowyg.base64.min.js"></script>
	<script>
		$('.owl-gallery').owlCarousel({
		    loop:false,
		    margin:10,
		    nav:false,
		    mouseDrag: false,
		    touchDrag:false,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:3
		        },
		        1000:{
		            items:4
		        }
		    }
		});
	</script>	
    <script>
    	$('#trumbowyg').trumbowyg({
    		lang: 'es',
    		removeformatPasted: true,
		    btns: [
		        ['foreColor', 'backColor'],
		        ['fromatting','strong', 'em', 'del','fontsize','fontfamily'],
		        ['noembed'],
		        ['link'],
		        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
		        ['unorderedList', 'orderedList'],
		        ['horizontalRule'],
		        ['removeformat'],
		        ['fullscreen'],
		    	['base64']
		    ],
		    plugins: {
		    	resizimg: {
		            minSize: 64,
		            step: 16,
		        }
		    },
		    autogrow: true,
		});
	</script>
@endpush
