@extends('layouts.admin')

@section('title','Editar rotador')

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
			<div class="card">
		        <div class="card-body">		            
		            <form action="{{route('slides.update',$data['slide']->id)}}" method="post" enctype="multipart/form-data">
		            	@csrf
		            	{{ method_field('PATCH') }}
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>Información</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Nombre</label>
			            			<input type="text" name="title" class="form-control" value="{{$data['slide']->title}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Descripción</label>
			            			<textarea name="description" class="form-control">{{$data['slide']->description}}</textarea>
			            		</div>
			            	</div>
			            	<div class="col-xl-6">
			            		<div class="form-group">
			            			<label>Texto de botón</label>
			            			<input type="text" name="button" class="form-control" value="{{$data['slide']->button}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-6">
			            		<div class="form-group">
			            			<label>Url</label>
			            			<input type="text" name="url" class="form-control"  value="{{$data['slide']->url}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Imagen</label>
			            			<input type="file" name="image" class="form-control">
			            			@if ($data['slide']->img)
			            				<a href="{{route('get.image',$data['slide']->img)}}" target="_blank" class="btn btn-sm btn-warning">Imagen</a>
			            			@endif
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('slides.index')}}" class="btn btn-danger">Salir sin guardar</a>
			            		<button type="submit" class="btn btn-success">Guardar cambios</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
@endpush