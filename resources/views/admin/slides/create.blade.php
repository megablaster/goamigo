@extends('layouts.admin')

@section('title')
	{{__('slide.create.title')}}
@endsection

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
		        <div class="card-body">

		        	@if ($errors->any())
					    <div class="col-md-12">
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                    <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    </div>
					@endif
		            
		            <form action="{{route('slides.store')}}" method="post" enctype="multipart/form-data">
        				@csrf
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>{{__('slide.create.subtitle')}}</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('slide.create.name')}}</label>
			            			<input type="text" name="title" class="form-control {{($errors->first('title'))? 'invalid':''}}" value="{{old('title')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('slide.create.description')}}</label>
			            			<textarea name="description" class="form-control {{($errors->first('description'))? 'invalid':''}}" value="{{old('description')}}"></textarea>
			            		</div>
			            	</div>
			            	<div class="col-xl-6">
			            		<div class="form-group">
			            			<label>{{__('slide.create.button')}}</label>
			            			<input type="text" name="button" class="form-control {{($errors->first('button'))? 'invalid':''}}" value="{{old('button')}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-6">
			            		<div class="form-group">
			            			<label>Url</label>
			            			<input type="text" name="url" class="form-control {{($errors->first('url'))? 'invalid':''}}" value="{{old('url')}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('slide.create.image')}}</label>
			            			<input type="file" name="image" class="form-control" required>
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('slides.index')}}" class="btn btn-danger">{{__('slide.create.cancel')}}</a>
			            		<button type="submit" class="btn btn-success">{{__('slide.create.add')}}</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
@endpush