@extends('layouts.admin')

@section('title')
	{{__('slide.title')}}
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">

			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif

			<div class="card">
		        <div class="card-body">

		            <h4 class="card-title">{{__('slide.subtitle')}}</h4>
		            <div class="table-responsive">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('slide.table.name')}}</th>
                                    {{-- <th>{{__('slide.table.description')}}</th> --}}
                                    <th>Url</th>
                                    <th>{{__('slide.table.date')}}</th>
                                    <th width="1em">{{__('slide.table.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>

                            	@foreach ($data['slides'] as $slide)
                            		<tr>
	                                    <td>{{$slide->id}}</td>
	                                    <td>{{$slide->title}}</td>
	                                    {{-- <td>{{$slide->description}}</td> --}}
	                                    <td>{{$slide->url}}</td>
	                                    <td>{{$slide->getDate()}}</td>
	                                    <td>
	                                    	<a href="{{route('slides.edit',$slide->id)}}" class="btn btn-sm btn-info">{{__('general.edit')}}</a>
	                                    	<a href="#" class="btn btn-sm btn-danger delete" data-id="{{$slide->id}}">{{__('general.delete')}}</a>
	                                    	<form action="{{route('slides.destroy',$slide->id)}}" method="post" style="display: inline-block;" id="delete-{{$slide->id}}">
	                                    		@csrf
	                                    		@method('DELETE')
	                                    	</form>
	                                    </td>
	                                </tr>	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>

                    <a href="{{route('slides.create')}}" class="btn btn-info">{{__('slide.add')}}</a>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
    	//Cerrar modal
    	$('#close-modal').click(function(){
		  $.fancybox.close();
		});

		//Preguntar si desea borrar
		$('.delete').on('click', function(e){

			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: '¿Estas seguro?',
			  text: "Esta a apunto de eliminar el rotador.",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cancelar',
			  confirmButtonText: '¡Si, eliminar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#delete-'+id).submit();
			  }
			})

		});
		
    </script>	
@endpush