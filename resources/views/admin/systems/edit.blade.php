@extends('layouts.admin')

@section('title','Editar sistema')

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
			<div class="card">
				<form action="{{route('systems.update',$system->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <h3><strong>Pago a profesores</strong></h3><hr>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Pago x clase pagada:</label>
                                    <input type="text" class="form-control" name="paymentfull" value="{{$system->paymentfull}}">
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Pago x clase penalizada:</label>
                                    <input type="text" class="form-control" name="paymenthalf" value="{{$system->paymenthalf}}">
                                </div>
                                <small>Recuerda que este ajuste se hace de manera inmediata inclusive a los pagos que estan por hacerse anteriores.</small>
                            </div>
                        </div>
                    </div>
			</div>
			<div class="card">
		        <div class="card-body">
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>Sistema</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Logo:</label>
			            			<input type="file" name="logo" class="form-control">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Nombre:</label>
			            			<input type="text" name="name" class="form-control" value="{{$system->name}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<h3>Redes sociales</h3>
			            		<div class="form-group">
			            			<label>Facebook:</label>
			            			<input type="text" name="facebook" class="form-control" value="{{$system->facebook}}">
			            		</div>
			            		<div class="form-group">
			            			<label>Youtube:</label>
			            			<input type="text" name="youtube" class="form-control" value="{{$system->youtube}}">
			            		</div>
			            		<div class="form-group">
			            			<label>Instagram:</label>
			            			<input type="text" name="instagram" class="form-control" value="{{$system->instagram}}">
			            		</div>
			            	</div>

			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Correos de sistema: </label>
			            			<input type="text" name="emails" class="form-control" value="{{$system->emails}}" required>
			            			<small>Si desea agregar más de un correo, utilice coma para separar.</small>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Copyright:</label>
			            			<input type="text" name="copyright" class="form-control" value="{{$system->copyright}}">
			            		</div>
			            	</div>
			            </div>
		        </div>
		    </div>
            <div class="card">
                <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <h3><strong>Cupones</strong></h3><hr>
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group">
                            <label>Modo de aplicación:</label>
                            <select name="type_app" class="form-control">
                                <option value="live" {{($system->type_app == 'live')?'selected':''}}>Producción</option>
                                <option value="campaing" {{($system->type_app == 'campaing')?'selected':''}}>Campaña</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="form-group">
                            <label>Permitido hasta:</label>
                            <input type="date" class="form-control" name="date_coupon" value="{{$system->date_coupon}}">
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label>Tipo de cupón:</label>
                            <select name="type_coupon" class="form-control">
                                <option value="percentage" {{($system->type_coupon == 'percentage')?'selected':''}}>Porcentaje</option>
                                <option value="amount" {{($system->type_coupon == 'amount')?'selected':''}}>Cantidad</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label>Porcentaje:</label>
                            <input type="text" class="form-control"  name="percentage_coupon" value="{{$system->percentage_coupon}}">
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="form-group">
                            <label>Cantidad:</label>
                            <input type="text" class="form-control" name="amount_coupon" value="{{$system->amount_coupon}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <a href="{{route('systems.index')}}" class="btn btn-danger">Salir sin guardar</a>
                        <button type="submit" class="btn btn-success">Guardar cambios</button>
                    </div>
                </div>
                </form>
            </div>
            </div>
	    </div>
        <div class="col-xl-12">
            <div class="card">
                <form action="{{route('systems.update',$system->id)}}" method="post">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <h3><strong>Privacidad & términos</strong></h3><hr>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Aviso de privacidad</label>
                                    <textarea name="privacy" class="editor form-control">{{$system->privacy}}</textarea>
                                </div>
                            </div>
							<div class="col-xl-12">
                                <div class="form-group">
                                    <label>Términos y condiciones</label>
                                    <textarea name="terms" class="terms form-control">{{$system->terms}}</textarea>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="form-group">
                                    <label>Preguntas frecuentes</label>
                                    <textarea name="faqs" class="faqs form-control">{{$system->faqs}}</textarea>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <button type="submit" class="btn btn-primary">Guardar cambios</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection

@push('js')
    <script src="{{asset('plugins/ckeditor/build/ckeditor.js')}}"></script>
    <script>ClassicEditor
		.create( document.querySelector( '.editor' ), {
			toolbar: {
				items: [
					'heading',
					'|',
					'fontSize',
					'fontColor',
					'horizontalLine',
					'bold',
					'italic',
					'link',
					'removeFormat',
					'bulletedList',
					'numberedList',
					'|',
					'indent',
					'outdent',
					'|',
					'imageUpload',
					'imageInsert',
					'insertTable',
					'mediaEmbed',
					'undo',
					'redo'
				]
			},
			language: 'es',
			image: {
				toolbar: [
					'imageTextAlternative',
					'imageStyle:full',
					'imageStyle:side'
				]
			},
			table: {
				contentToolbar: [
					'tableColumn',
					'tableRow',
					'mergeTableCells'
				]
			},
			licenseKey: '',
			
		} )
		.then( editor => {
			window.editor = editor;
		})
		.catch( error => {
			console.error( 'Oops, something went wrong!' );
			console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
			console.warn( 'Build id: refjhhoyjdf1-pc5cy97iyk66' );
			console.error( error );
		});
	</script>
	<script>ClassicEditor
		.create( document.querySelector( '.terms' ), {
			toolbar: {
				items: [
					'heading',
					'|',
					'fontSize',
					'fontColor',
					'horizontalLine',
					'bold',
					'italic',
					'link',
					'removeFormat',
					'bulletedList',
					'numberedList',
					'|',
					'indent',
					'outdent',
					'|',
					'imageUpload',
					'imageInsert',
					'insertTable',
					'mediaEmbed',
					'undo',
					'redo'
				]
			},
			language: 'es',
			image: {
				toolbar: [
					'imageTextAlternative',
					'imageStyle:full',
					'imageStyle:side'
				]
			},
			table: {
				contentToolbar: [
					'tableColumn',
					'tableRow',
					'mergeTableCells'
				]
			},
			licenseKey: '',
			
		} )
		.then( editor => {
			window.editor = editor;
		})
		.catch( error => {
			console.error( 'Oops, something went wrong!' );
			console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
			console.warn( 'Build id: refjhhoyjdf1-pc5cy97iyk66' );
			console.error( error );
		});
	</script>
	<script>ClassicEditor
		.create( document.querySelector( '.faqs' ), {
			toolbar: {
				items: [
					'heading',
					'|',
					'fontSize',
					'fontColor',
					'horizontalLine',
					'bold',
					'italic',
					'link',
					'removeFormat',
					'bulletedList',
					'numberedList',
					'|',
					'indent',
					'outdent',
					'|',
					'imageUpload',
					'imageInsert',
					'insertTable',
					'mediaEmbed',
					'undo',
					'redo'
				]
			},
			language: 'es',
			image: {
				toolbar: [
					'imageTextAlternative',
					'imageStyle:full',
					'imageStyle:side'
				]
			},
			table: {
				contentToolbar: [
					'tableColumn',
					'tableRow',
					'mergeTableCells'
				]
			},
			licenseKey: '',
			
		} )
		.then( editor => {
			window.editor = editor;
		})
		.catch( error => {
			console.error( 'Oops, something went wrong!' );
			console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
			console.warn( 'Build id: refjhhoyjdf1-pc5cy97iyk66' );
			console.error( error );
		});
	</script>
@endpush
