@extends('layouts.admin')

@section('title')
	Paquetes
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">

			<div class="card-group">
				 <div class="card border-right">
		            <div class="card-body">
		                <div class="d-flex d-lg-flex d-md-block align-items-center">
		                    <div>
		                        <div class="d-inline-flex align-items-center">
		                            <h2 class="text-dark mb-1 font-weight-medium"></h2>
		                        </div>
		                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Ingreso bruto</h6>
		                    </div>
		                    <div class="ml-auto mt-md-3 mt-lg-0">
		                        <span class="opacity-7 text-muted"><i data-feather="book-open"></i></span>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="card border-right">
		            <div class="card-body">
		                <div class="d-flex d-lg-flex d-md-block align-items-center">
		                    <div>
		                        <div class="d-inline-flex align-items-center">
		                            <h2 class="text-dark mb-1 font-weight-medium"></h2>
		                        </div>
		                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Ganancia mensual</h6>
		                    </div>
		                    <div class="ml-auto mt-md-3 mt-lg-0">
		                        <span class="opacity-7 text-muted"><i data-feather="book-open"></i></span>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>

			<div class="card">
		        <div class="card-body">

		            <h4 class="card-title">Últimas ingresos</h4>
		            <div class="table-responsive">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Estudiante</th>
                                    <th>Producto</th>
                                    <th>Créditos</th>
                                    <th>Vigencia</th>
                                    <th>Costo</th>
                                    <th>Ganancia</th>
                                    <th>Fecha de compra</th>
                                </tr>
                            </thead>
                            <tbody>

                            	@foreach ($data['invoices'] as $invoice)
                            		<tr>
	                                    <td>{{$invoice->id}}</td>
	                                    <td>{{$invoice->user->name}} {{$invoice->user->last_name}}</td>
                                        <td>{{$invoice->product->name}}</td>
                                        <td>{{$invoice->product->credits}}</td>
                                        <td>{{$invoice->product->days}}</td>
                                        <td>{{$invoice->product->price}} USD</td>
                                        <td>{!!$invoice->product->gain!!}</td>
                                        <td>{{$invoice->created_at}}</td>
	                                </tr>	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
    	//Cerrar modal
    	$('#close-modal').click(function(){
		  $.fancybox.close();
		});

		//Preguntar si desea borrar
		$('.delete').on('click', function(e){

			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: '¿Estas seguro?',
			  text: "Esta a apunto de eliminar el rotador.",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cancelar',
			  confirmButtonText: '¡Si, eliminar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#delete-'+id).submit();
			  }
			})

		});
		
    </script>	
@endpush