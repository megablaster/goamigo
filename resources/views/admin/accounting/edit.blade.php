@extends('layouts.admin')

@section('title','Editar sistema')

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-6">
			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
			<div class="card">
				<form action="{{route('systems.update',$system->id)}}" method="post" enctype="multipart/form-data">
				<div class="card-body">
					<div class="row">
						<div class="col-12">
		            		<h3><strong>Pago a profesores</strong></h3><hr>
		            	</div>
		            	<div class="col-xl-12">
		            		<div class="form-group">
		            			<label>Pago x clase pagada:</label>
		            			<input type="text" class="form-control" name="paymentfull" value="{{$system->paymentfull}}">
		            		</div>
		            	</div>
		            	<div class="col-xl-12">
		            		<div class="form-group">
		            			<label>Pago x clase penalizada:</label>
		            			<input type="text" class="form-control" name="paymenthalf" value="{{$system->paymenthalf}}">
		            		</div>
		            		<small>Recuerda que este ajuste se hace de manera inmediata inclusive a los pagos que estan por hacerse anteriores.</small>
		            	</div>
					</div>
				</div>
			</div>
			<div class="card">
		        <div class="card-body">
		            	@csrf
		            	{{ method_field('PATCH') }}
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>Sistema</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Logo:</label>
			            			<input type="file" name="logo" class="form-control">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Nombre:</label>
			            			<input type="text" name="name" class="form-control" value="{{$system->name}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<h3>Redes sociales</h3>
			            		<div class="form-group">
			            			<label>Facebook:</label>
			            			<input type="text" name="facebook" class="form-control" value="{{$system->facebook}}">
			            		</div>
			            		<div class="form-group">
			            			<label>Youtube:</label>
			            			<input type="text" name="youtube" class="form-control" value="{{$system->youtube}}">
			            		</div>
			            		<div class="form-group">
			            			<label>Instagram:</label>
			            			<input type="text" name="instagram" class="form-control" value="{{$system->instagram}}">
			            		</div>
			            	</div>
		            	
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Correos de sistema: </label>
			            			<input type="text" name="emails" class="form-control" value="{{$system->emails}}" required>
			            			<small>Si desea agregar más de un correo, utilice coma para separar.</small>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Copyright:</label>
			            			<input type="text" name="copyright" class="form-control" value="{{$system->copyright}}">
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('systems.index')}}" class="btn btn-danger">Salir sin guardar</a>
			            		<button type="submit" class="btn btn-success">Guardar cambios</button>
			            	</div>
		            	</div>
		            </form>
		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
@endpush