@php
    $current = Route::currentRouteName();
@endphp
@role('admin')
    <li class="sidebar-item {{($current == 'users.create')}}">
        <a class="sidebar-link has-arrow" href="javascript:void(0)" aria-expanded="false"><i data-feather="users" class="feather-icon"></i>
            <span class="hide-menu">{{__('admin.users')}}</span>
        </a>
        <ul aria-expanded="false" class="collapse  first-level base-level-line {{($current == 'users.create')?'in':''}}">
            <li class="sidebar-item {{($current == 'users.create')?'active':''}}">
                <a href="{{route('users.index')}}" class="sidebar-link {{($current == 'users.create')?'active':''}}">
                    <span class="hide-menu">Todos los usuarios</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{route('users.type','teacher')}}" class="sidebar-link">
                    <span class="hide-menu">Profesores</span>
                </a>
            </li>
            <li class="sidebar-item">
                <a href="{{route('users.type','student')}}" class="sidebar-link">
                    <span class="hide-menu">Estudiantes</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="nav-small-cap">
        <span class="hide-menu">{{__('admin.history')}}</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link sidebar-link" href="{{route('accounting.income')}}" aria-expanded="false">
            <i data-feather="dollar-sign" class="feather-icon"></i>
            <span class="hide-menu">Clases</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link sidebar-link" href="{{route('accounting.lesson')}}" aria-expanded="false">
            <i data-feather="box" class="feather-icon"></i>
            <span class="hide-menu">{{__('admin.payment.title')}}</span>
        </a>
    </li>
    <li class="nav-small-cap">
        <span class="hide-menu">{{__('admin.administration')}}</span>
    </li>
    <li class="sidebar-item {{($current == 'testimonials.create' || $current == 'testimonials.edit')?'selected':''}}">
        <a class="sidebar-link" href="{{route('testimonials.index')}}" aria-expanded="false">
            <i data-feather="star" class="feather-icon"></i>
            <span class="hide-menu">{{__('admin.testimonials')}}</span>
        </a>
    </li>
    <li class="sidebar-item {{($current == 'slides.create' || $current == 'slides.edit')?'selected':''}}">
        <a class="sidebar-link" href="{{route('slides.index')}}" aria-expanded="false">
            <i data-feather="sliders" class="feather-icon"></i>
            <span class="hide-menu">{{__('admin.sliders')}}</span>
        </a>
    </li>
    <li class="sidebar-item {{($current == 'levels.create' || $current == 'levels.edit')?'selected':''}}">
        <a class="sidebar-link has-arrow" href="javascript:void(0)" aria-expanded="false"><i data-feather="file-text" class="feather-icon"></i>
            <span class="hide-menu">{{__('admin.educational')}}</span>
        </a>
        <ul aria-expanded="false" class="collapse  first-level base-level-line {{($current == 'levels.create' || $current == 'levels.edit')?'in':''}}">
            <li class="sidebar-item {{($current == 'levels.create' || $current == 'levels.edit')?'active':''}}">
                <a href="{{route('levels.index')}}" class="sidebar-link {{($current == 'levels.create' || $current == 'levels.edit')?'active':''}}">
                    <span class="hide-menu"> {{__('admin.levels')}}</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="sidebar-item {{($current == 'systems.edit')?'selected':''}}">
        <a class="sidebar-link" href="{{route('systems.edit',1)}}" aria-expanded="false">
            <i data-feather="sliders" class="feather-icon"></i>
            <span class="hide-menu">Configuración</span>
        </a>
    </li>
    <li class="nav-small-cap">
        <span class="hide-menu">{{__('admin.payment.payment')}}</span>
    </li>
    <li class="sidebar-item {{($current == 'products.create' || $current == 'products.edit')?'selected':''}}">
        <a class="sidebar-link" href="{{route('products.index')}}" aria-expanded="false">
            <i data-feather="archive" class="feather-icon"></i>
            <span class="hide-menu">{{__('admin.payment.title')}}</span>
        </a>
    </li>
@endrole
@role('manager|admin')
    <li class="nav-small-cap">
        <span class="hide-menu">Blog</span>
    </li>
    <li class="sidebar-item {{($current == 'categories.create' || $current == 'categories.edit')?'selected':''}}">
        <a class="sidebar-link" href="{{route('categories.index')}}" aria-expanded="false">
            <i data-feather="list" class="feather-icon"></i>
            <span class="hide-menu">Categorías</span>
        </a>
    </li>
    <li class="sidebar-item {{($current == 'posts.create' || $current == 'posts.edit')?'selected':''}}">
        <a class="sidebar-link" href="{{route('posts.index')}}" aria-expanded="false">
            <i data-feather="edit-3" class="feather-icon"></i>
            <span class="hide-menu">Entradas</span>
        </a>
    </li>
@endrole
<li class="list-divider"></li>
<li class="nav-small-cap">
    <span class="hide-menu">{{__('admin.account.account')}}</span>
</li>
<li class="sidebar-item">
    <a class="sidebar-link sidebar-link" href="{{route('users.edit',auth()->user()->id)}}" aria-expanded="false">
        <i data-feather="user-check" class="feather-icon"></i>
        <span class="hide-menu">Mi cuenta</span>
    </a>
</li>
<li class="sidebar-item">
    <a class="sidebar-link sidebar-link" href="{{route('logout.click')}}" aria-expanded="false">
        <i data-feather="log-out" class="feather-icon"></i>
        <span class="hide-menu">{{__('admin.account.logout')}}</span>
    </a>
</li>
