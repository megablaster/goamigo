@extends('layouts.admin')

@section('title')
	Editar producto
@endsection

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-6">
			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
			<div class="card">
		        <div class="card-body">		            
		            <form action="{{route('products.update',$data['product']->id)}}" method="post">
		            	@csrf
		            	{{ method_field('PATCH') }}
			            <div class="row">
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Nombre</label>
			            			<input type="text" name="name" class="form-control" value="{{$data['product']->name}}">
			            		</div>
			            	</div>
							<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Costo</label>
			            			<input type="text" name="price" class="form-control" value="{{$data['product']->price}}">
			            		</div>
			            	</div>
							<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Dias</label>
			            			<input type="number" name="days" class="form-control" value="{{$data['product']->days}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Créditos</label>
			            			<input type="number" name="credits" class="form-control" value="{{$data['product']->credits}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Descripción</label>
			            			<textarea name="description"  class="form-control" rows="5">{{$data['product']->description}}</textarea>
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('products.index')}}" class="btn btn-danger">Cancelar</a>
			            		<button type="submit" class="btn btn-success">Guardar cambios</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
@endpush