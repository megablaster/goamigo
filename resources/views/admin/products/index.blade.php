@extends('layouts.admin')

@section('title')
Productos
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">

			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif

			<div class="card">
		        <div class="card-body">

		            <h4 class="card-title"></h4>
		            <div class="table-responsive">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Costo</th>
                                    <th>Dias</th>
                                    <th>Créditos</th>
                                    <th width="1em">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            	@foreach ($data['products'] as $product)
                            		<tr>
	                                    <td>{{$product->id}}</td>
										<td>{{$product->name}}</td>
	                                    <td>{{$product->price}}</td>
										<td>{{$product->days}}</td>
										<td>{{$product->credits}}</td>
	                                    <td>
	                                    	<a href="{{route('products.edit',$product->id)}}" class="btn btn-sm btn-info">{{__('general.edit')}}</a>
	                                    	<a href="#" class="btn btn-sm btn-danger delete" data-id="{{$product->id}}">{{__('general.delete')}}</a>
	                                    	<form action="{{route('products.destroy',$product->id)}}" method="post" style="display: inline-block;" id="delete-{{$product->id}}">
	                                    		@csrf
	                                    		@method('DELETE')
	                                    	</form>
	                                    </td>
	                                </tr>	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>

                    <a href="{{route('products.create')}}" class="btn btn-info">Agregar producto</a>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
    	//Cerrar modal
    	$('#close-modal').click(function(){
		  $.fancybox.close();
		});

		//Preguntar si desea borrar
		$('.delete').on('click', function(e){

			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: '¿Estas seguro?',
			  text: "Esta a apunto de eliminar el producto.",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cancelar',
			  confirmButtonText: '¡Si, eliminar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#delete-'+id).submit();
			  }
			})

		});
		
    </script>	
@endpush