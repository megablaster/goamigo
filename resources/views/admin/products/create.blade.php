@extends('layouts.admin')

@section('title')
	Agregar producto
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('script/select/bootstrap-select.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-6">
			<div class="card">
		        <div class="card-body">

		        	@if ($errors->any())
					    <div class="col-md-12">
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                    <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    </div>
					@endif
		            
		            <form action="{{route('products.store')}}" method="post">
        				@csrf
			            <div class="row">
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Nombre</label>
			            			<input type="text" name="name" class="form-control {{($errors->first('name'))? 'invalid':''}}" value="{{old('name')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Costo</label>
			            			<input type="text" name="price" class="form-control {{($errors->first('price'))? 'invalid':''}}" value="{{old('price')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Dias</label>
			            			<input type="number" name="days" class="form-control {{($errors->first('days'))? 'invalid':''}}" value="{{old('days')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Créditos</label>
			            			<input type="number" name="credits" class="form-control {{($errors->first('credits'))? 'invalid':''}}" value="{{old('credits')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Descripción</label>
			            			<textarea name="description"  class="form-control {{($errors->first('credits'))? 'invalid':''}}" value="{{old('credits')}}" rows="5"></textarea>
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('products.index')}}" class="btn btn-danger">Cancelar</a>
			            		<button type="submit" class="btn btn-success">Agregar</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/select/bootstrap-select.min.js')}}"></script>
	<script>
		$('.select').selectpicker();
	</script>
@endpush