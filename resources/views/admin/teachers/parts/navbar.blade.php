@role('teacher')
    @if (auth()->user()->teacher->step == 'third')
    <li class="nav-small-cap">
        <span class="hide-menu">Calendario & clases</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('teacher.calendar')}}" aria-expanded="false">
            <i data-feather="calendar" class="feather-icon"></i>
            <span class="hide-menu">Calendario</span>
        </a>
    </li>    
    <li class="nav-small-cap">
        <span class="hide-menu">Configuración</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('teacher.availability')}}" aria-expanded="false">
            <i data-feather="clock" class="feather-icon"></i>
            <span class="hide-menu">Disponibilidad</span>
        </a>
    </li>
    @endif
@endrole --}}