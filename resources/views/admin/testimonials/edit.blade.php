@extends('layouts.admin')

@section('title','Editar testimonio')

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
			<div class="card">
		        <div class="card-body">		            
		            <form action="{{route('testimonials.update',$data['testimonial']->id)}}" method="post" enctype="multipart/form-data">
		            	@csrf
		            	{{ method_field('PATCH') }}
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>Información</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Nombre</label>
			            			<input type="text" name="name" class="form-control" value="{{$data['testimonial']->name}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Descripción</label>
			            			<textarea name="description" class="form-control">{{$data['testimonial']->description}}</textarea>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Profesión</label>
			            			<input type="text" name="profession" class="form-control" value="{{$data['testimonial']->profession}}">
			            		</div>
			            	</div>

			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Fotografía</label>
			            			<input type="file" name="image" class="form-control">
			            			@if ($data['testimonial']->img)
			            				<a href="{{route('get.image',$data['testimonial']->img)}}" target="_blank" class="btn btn-sm btn-warning">Fotográfia</a>
			            			@endif
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('testimonials.index')}}" class="btn btn-danger">Salir sin guardar</a>
			            		<button type="submit" class="btn btn-success">Guardar cambios</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
@endpush