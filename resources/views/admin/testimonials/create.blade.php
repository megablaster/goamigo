@extends('layouts.admin')

@section('title')
	{{__('testimonial.create.title')}}
@endsection

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
		        <div class="card-body">

		        	@if ($errors->any())
					    <div class="col-md-12">
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                    <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    </div>
					@endif
		            
		            <form action="{{route('testimonials.store')}}" method="post" enctype="multipart/form-data">
        				@csrf
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>{{__('testimonial.create.subtitle')}}</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('testimonial.create.name')}}</label>
			            			<input type="text" name="name" class="form-control {{($errors->first('name'))? 'invalid':''}}" value="{{old('name')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('testimonial.create.description')}}</label>
			            			<textarea name="description" class="form-control {{($errors->first('description'))? 'invalid':''}}" value="{{old('description')}}"></textarea>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('testimonial.create.profession')}}</label>
			            			<input type="text" name="profession" class="form-control {{($errors->first('profession'))? 'invalid':''}}" value="{{old('profession')}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('testimonial.create.photography')}}</label>
			            			<input type="file" name="image" class="form-control" required>
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('levels.index')}}" class="btn btn-danger">{{__('testimonial.create.cancel')}}</a>
			            		<button type="submit" class="btn btn-success">{{__('testimonial.create.add')}}</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
@endpush