@extends('layouts.admin')

@section('title')
	Editar categoría
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/owl/assets/owl.theme.default.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-8">
			@if ($errors->any())
			    <div class="col-md-12">
			        <div class="alert alert-danger">
			            <ul>
			                @foreach ($errors->all() as $error)
			                    <li>{{ $error }}</li>
			                @endforeach
			            </ul>
			        </div>
			    </div>
			@endif

			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
			<div class="card">
		        <div class="card-body">
		            <form action="{{route('categories.update',$data['category']->id)}}" method="post" enctype="multipart/form-data">
		            	@csrf
		            	{{ method_field('PATCH') }}
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>Información</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('user.edit.name')}}</label>
			            			<input type="text" name="name" class="form-control" value="{{$data['category']->name}}">
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Descripción</label>
			            			<textarea name="description" class="form-control">{{$data['category']->description}}</textarea>
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('categories.index')}}" class="btn btn-danger">{{__('user.edit.exit')}}</a>
			            		<button type="submit" class="btn btn-success">{{__('user.edit.save')}}</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>
@endsection

@push('js')
	<script src="{{asset('script/owl/owl.carousel.min.js')}}"></script>
@endpush
