@extends('layouts.admin')

@section('title')
	Categorías
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">

			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif

			@if (Session::has('failed'))
			    <div class="alert alert-warning">
			        {!! Session::get('failed') !!}
			    </div>
			@endif

			<div class="card">
		        <div class="card-body">

		            <h4 class="card-title">Todas las categorías</h4>
		            <div class="table-responsive">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Entradas asignadas</th>
                                    <th>Visualizaciones</th>
                                    <th>{{__('user.table.date')}}</th>
                                    <th width="1em">{{__('user.table.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>

                            	@foreach ($data['categories'] as $category)
                            		<tr>
	                                    <td>{{$category->id}}</td>
	                                    <td>{{$category->name}}</td>
	                                    <td>{{$category->description}}</td>
	                                    <td>
	                                    	<span class="badge badge-primary">{{$category->posts->count()}}</span>
	                                    </td>
                                        <td>{{$category->views}}</td>
	                                    <td>{{$category->getDate()}}</td>
	                                    <td>
	                                    	<a href="{{route('categories.edit',$category->id)}}" class="btn btn-sm btn-info">{{__('general.edit')}}</a>
	                                    	<a href="#" class="btn btn-sm btn-danger delete" data-id="{{$category->id}}">{{__('general.delete')}}</a>
	                                    	<form action="{{route('categories.destroy',$category->id)}}" method="post" style="display: inline-block;" id="delete-{{$category->id}}">
	                                    		@csrf
	                                    		@method('DELETE')
	                                    	</form>
	                                    </td>
	                                </tr>
                            	@endforeach

                            </tbody>
                        </table>
                    </div>

                    <a href="{{route('categories.create')}}" class="btn btn-info">Agregar categoría</a>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
    	//Cerrar modal
    	$('#close-modal').click(function(){
		  $.fancybox.close();
		});

		//Preguntar si desea borrar
		$('.delete').on('click', function(e){

			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: '¿Estas seguro?',
			  text: "Esta a apunto de eliminar la categoría.",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cancelar',
			  confirmButtonText: '¡Si, eliminar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#delete-'+id).submit();
			  }
			})

		});

    </script>
@endpush
