@extends('layouts.admin')

@section('title')
	{{__('level.title')}}
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">

			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif

			<div class="card">
		        <div class="card-body">

		            <h4 class="card-title">{{__('level.all')}}</h4>
		            <div class="table-responsive">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{__('level.table.name')}}</th>
                                    <th>{{__('level.table.description')}}</th>
                                    <th>{{__('level.table.lesson')}}</th>
                                    <th width="1em">{{__('level.table.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>

                            	@foreach ($data['levels'] as $level)
                            		<tr>
	                                    <td>{{$level->id}}</td>
	                                    <td>{{$level->name}}</td>
	                                    <td>{{$level->description}}</td>
	                                    <td>{{$level->lessons}}</td>
	                                    <td>
	                                    	<a href="{{route('levels.edit',$level->id)}}" class="btn btn-sm btn-info">{{__('general.edit')}}</a>
	                                    	<a href="#" class="btn btn-sm btn-danger delete" data-id="{{$level->id}}">{{__('general.delete')}}</a>
	                                    	<form action="{{route('levels.destroy',$level->id)}}" method="post" style="display: inline-block;" id="delete-{{$level->id}}">
	                                    		@csrf
	                                    		@method('DELETE')
	                                    	</form>
	                                    </td>
	                                </tr>	
                            	@endforeach

                            </tbody>
                        </table>
                    </div>

                    <a href="{{route('levels.create')}}" class="btn btn-info">{{__('level.add')}}</a>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
    	//Cerrar modal
    	$('#close-modal').click(function(){
		  $.fancybox.close();
		});

		//Preguntar si desea borrar
		$('.delete').on('click', function(e){

			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: '¿Estas seguro?',
			  text: "Esta a apunto de eliminar el nivel.",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cancelar',
			  confirmButtonText: '¡Si, eliminar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#delete-'+id).submit();
			  }
			})

		});
		
    </script>	
@endpush