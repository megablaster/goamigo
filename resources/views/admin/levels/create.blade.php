@extends('layouts.admin')

@section('title')
	{{__('level.create.title')}}
@endsection

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
		        <div class="card-body">

		        	@if ($errors->any())
					    <div class="col-md-12">
					        <div class="alert alert-danger">
					            <ul>
					                @foreach ($errors->all() as $error)
					                    <li>{{ $error }}</li>
					                @endforeach
					            </ul>
					        </div>
					    </div>
					@endif
		            
		            <form action="{{route('levels.store')}}" method="post">
        				@csrf
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>{{__('level.create.subtitle')}}</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('level.create.name')}}</label>
			            			<input type="text" name="name" class="form-control {{($errors->first('name'))? 'invalid':''}}" value="{{old('name')}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('level.create.description')}}</label>
			            			<textarea name="description" class="form-control {{($errors->first('description'))? 'invalid':''}}" value="{{old('description')}}"></textarea>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>{{__('level.create.lesson')}}</label>
			            			<input type="number" name="lessons" class="form-control {{($errors->first('lessons'))? 'invalid':''}}" value="{{old('lessons')}}" required>
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('levels.index')}}" class="btn btn-danger">{{__('level.create.cancel')}}</a>
			            		<button type="submit" class="btn btn-success">{{__('level.create.add')}}</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
@endpush