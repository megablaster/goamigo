@extends('layouts.admin')

@section('title','Editar nivel')

@push('css')
	
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
			<div class="card">
		        <div class="card-body">		            
		            <form action="{{route('levels.update',$data['level']->id)}}" method="post">
		            	@csrf
		            	{{ method_field('PATCH') }}
			            <div class="row">
			            	<div class="col-12">
			            		<h3><strong>Información</strong></h3><hr>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Nombre</label>
			            			<input type="text" name="name" class="form-control" value="{{$data['level']->name}}" required>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>Descripción</label>
			            			<textarea name="description" class="form-control">{{$data['level']->description}}</textarea>
			            		</div>
			            	</div>
			            	<div class="col-xl-12">
			            		<div class="form-group">
			            			<label>No. de clases</label>
			            			<input type="number" name="lessons" class="form-control" value="{{$data['level']->lessons}}" required>
			            		</div>
			            	</div>
			            </div>
			            <div class="row">
				            <div class="col-xl-12">
				            	<a href="{{route('levels.index')}}" class="btn btn-danger">Salir sin guardar</a>
			            		<button type="submit" class="btn btn-success">Guardar cambios</button>
			            	</div>
		            	</div>
		            </form>

		        </div>
		    </div>
	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
@endpush