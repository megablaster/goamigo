@extends('teacher.layouts.admin')

@section('title')
	Próximas clases
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">

			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif

			<div class="card">
		        <div class="card-body">
		            <h4 class="card-title">Mis clases</h4>

		            <div class="table-responsive">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Alumno</th>
                                    <th>Fecha de clase</th>
                                    <th>Hora de la clase</th>
                                    <th>Estatus</th>
                                    <th width="1em">{{__('user.table.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>

                            	@foreach ($data['schedules'] as $schedule)
                            		<tr>
	                                    <td>{{$schedule->student->name}} {{$schedule->student->last_name}}</td>
	                                    <td>{{ Carbon\Carbon::parse($schedule->schedule->start)->format('d-m-Y') }}</td>
	                                    <td>{{ Carbon\Carbon::parse($schedule->schedule->start)->format('h:i:s A') }}</td>
	                                    <td>{!!$schedule->Estatus!!}</td>
	                                    <td>
	                                    	{{--@if (\Carbon\Carbon::now() >= \Carbon\Carbon::parse($schedule->schedule->start)->subMinutes(5))--}}
	                                    		<a href="{{route('classroomteacher.create_room',$schedule->schedule->id)}}" target="_blank" class="btn btn-success btn-sm">Comenzar</a>
	                                    	{{--@endif--}}
	                                    	{{--@if (\Carbon\Carbon::now() >= \Carbon\Carbon::parse($schedule->schedule->start)->addMinutes(30))--}}
	                                    		<a href="{{route('classroomteacher.rate',$schedule->id)}}" class="btn btn-primary btn-sm">Calificar</a>
                                            {{--@endif--}}
	                                    	@if($schedule->status != 'cancel')
	                                    		{!!$schedule->schedule->cancelteacher()!!}
	                                    	@endif
	                                    	<form id="cancelMore-{{$schedule->id}}" action="{{route('teacher.schedule.cancelteacher')}}" method="post" >
		                                		<input type="hidden" name="teacher" value="{{$schedule->teacher}}">
		                                		<input type="hidden" name="id" value="{{$schedule->id}}">
		                                		<input type="hidden" name="type" value="more">
		                                		@csrf
		                                	</form>
		                                	<form id="cancelMin-{{$schedule->id}}" action="{{route('teacher.schedule.cancelteacher')}}" method="post" style="display: none;">
		                                		<input type="hidden" name="teacher" value="{{$schedule->teacher}}">
		                                		<input type="hidden" name="id" value="{{$schedule->id}}">
		                                		<input type="hidden" name="type" value="min">
		                                		@csrf
		                                	</form>
	                                    </td>
	                                </tr>
                            	@endforeach

                            </tbody>
                        </table>
		        	</div>

		    	</div>
	    	</div>

	    </div>
    </div>

@endsection

@push('js')
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
    	//Cerrar modal
    	$('#close-modal').click(function(){
		  $.fancybox.close();
		});
    </script>
    <script>
		//Preguntar si desea cancelar
		$('.cancelMore24').on('click', function(e){
			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: 'Solicitar cancelación',
			  text: $(this).data('text'),
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cerrar',
			  confirmButtonText: '¡Si, cancelar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#cancelMore-'+id).submit();
			  }
			})

		});

		$('.cancelMin24').on('click', function(e){

			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: 'Cancelación',
			  text: $(this).data('text'),
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cerrar',
			  confirmButtonText: '¡Si, cancelar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#cancelMin-'+id).submit();
			  }
			})

		});
	</script>
@endpush
