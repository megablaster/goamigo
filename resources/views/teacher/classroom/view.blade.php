@extends('teacher.layouts.admin')

@section('title')
	Clase del día - {{ Carbon\Carbon::parse($data['schedule']->start)->format('d-m-Y') }}
@endsection

@section('subtitle')
@endsection

@push('css')
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')

    <section class="classroom_teacher_rate">
        <div class="row">
            <div class="col-xl-12">
                @if (Session::has('success'))
                    <div class="alert alert-success">
                        {!! Session::get('success') !!}
                    </div>
                @endif
            </div>
            <div class="col-xl-3">
                <div class="card card-student">
                    <div class="card-body">
                        <h4 class="card-title">Información del estudiante</h4>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="info">
                                    <div class="img" style="background-image: url('{{route('get.image',$data['schedule']->student->img)}}');"></div>
                                    <p>
                                        <strong>Nickname:</strong> {{$data['schedule']->student->student->nickname}}<br>
                                        <strong>Correo:</strong> {{$data['schedule']->student->email}}
                                    </p>
                                    <a href="#" class="btn btn-primary">Iniciar clase</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-7">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Califica la clase.</h4>
                        <div class="row">
                            <div class="col-lg-12">

                                <form action="{{route('classroomteacher.rate.save')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$data['schedule']->id}}">
                                    <div class="form-group">
                                        <label>Comparte los comentarios sobre el alumno:</label>
                                        <textarea name="comments" class="form-control" rows="4"  {{($data['schedule']->comments)?'disabled':''}}>{{$data['schedule']->comments}}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-submit btn-success" {{($data['schedule']->comments)?'disabled':''}}>Enviar comentarios</button>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/countdown/2.6.0/countdown.min.js"></script>
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
@endpush
