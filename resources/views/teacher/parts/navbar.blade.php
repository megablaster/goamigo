@php
    $current = Route::currentRouteName();
@endphp

@if (auth()->user()->teacher->step == 'third')
    <li class="nav-small-cap">
        <span class="hide-menu">Calendario & clases</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('classroomteacher.index')}}" aria-expanded="false">
            <i data-feather="calendar" class="feather-icon"></i>
            <span class="hide-menu">Próximas clases</span>
        </a>
    </li>    
    <li class="nav-small-cap">
        <span class="hide-menu">Configuración</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('teacher.availability')}}" aria-expanded="false">
            <i data-feather="clock" class="feather-icon"></i>
            <span class="hide-menu">Disponibilidad</span>
        </a>
    </li>
    <li class="nav-small-cap">
        <span class="hide-menu">Mis facturas</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('invoicesteacher.index')}}" aria-expanded="false">
            <i data-feather="list" class="feather-icon"></i>
            <span class="hide-menu">Mis facturas</span>
        </a>
    </li>
    <li class="list-divider"></li>
@endif
    <li class="nav-small-cap">
        <span class="hide-menu">{{__('admin.account.account')}}</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link sidebar-link" href="{{route('users_teacher.edit',auth()->user()->id)}}" aria-expanded="false">
            <i data-feather="user-check" class="feather-icon"></i>
            <span class="hide-menu">Mi cuenta</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link sidebar-link" href="{{route('logout.click')}}" aria-expanded="false">
            <i data-feather="log-out" class="feather-icon"></i>
            <span class="hide-menu">{{__('admin.account.logout')}}</span>
        </a>
    </li>