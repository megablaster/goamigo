@extends('teacher.layouts.admin')

@section('title')
	Disponibilidad
@endsection

@push('css')
	<link href='https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.1/css/all.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
    <link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.min.css">
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">
			
			<div class="card">
		        <div class="card-body">

			        <div id='calendar'></div>

		        </div>
		    </div>

	    </div>
    </div>

@endsection

@push('js')
	<script src="https://momentjs.com/downloads/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.min.js"></script>
    <script>
    	$(document).ready(function () {
    		var today = moment();
    		today = today.format('YYYY-MM-DD');

		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });

		    var calendarEl = document.getElementById('calendar');
	        var calendar = new FullCalendar.Calendar(calendarEl, {
	        	selectAllow: function(select) {
				      return moment().diff(select.start) <= 0
				},
				defaultDate: today,
				eventOverlap: false,
				// timeZone: 'UTC',
	        	locale: 'es',
	        	selectable: true,
				editable: false,
				droppable: false,
				initialView: 'timeGridWeek',
				initialDate: today,
				expandRows: false,
				allDaySlot: false,
				snapDuration: "01:30:00",
				selectOverlap: false,
    			dayHeaders: true,
				scrollTime: '12:00:00',
				forceEventDuration : true,
  				defaultTimedEventDuration : "00:30:00",
				slotDuration: '00:30:00',
				themeSystem: 'bootstrap',
				nowIndicator: true,
				displayEventTime: false,
				buttonText: {
			      dayGridMonth: 'Mes',
			      timeGridWeek: 'Semana',
			      timeGridDay: 'Día',
			      listWeek: 'Lista'
			    },
				slotLabelFormat: {
					hour: 'numeric',
					minute: '2-digit',
					omitZeroMinute: false,
					hour12: false
				},
				eventTimeFormat: {
					hour: 'numeric',
					minute: '2-digit',
					omitZeroMinute: false,
					hour12: false 
				},
		        events: "{{route('teacher.schedule.get', auth()->user()->id )}}",
		       	eventColor: '#378006',
		       	eventDisplay: 'block',
		        displayEventTime: true,
		        headerToolbar: {
		        	start: 'listWeek,timeGridWeek',
		        	center:'title',
		        	end: 'prev,next',
		        },
		        select: function(arg) {

		        	var start = moment(arg.start);
		        	var end = moment(arg.end);
		        	var days = arg.start.getDay();

					console.log(start);

			        var title = "{{auth()->user()->name}}";

			        if (title) {

			        	$.ajax({
		                    url: "{{route('teacher.schedule.ajax')}}",
		                    data: {
		                        title: title,
		                        start: start.format("YYYY-MM-DD HH:mm:ss"),
		                        user_id: "{{auth()->user()->id}}",
		                        end: end.format("YYYY-MM-DD HH:mm:ss"),
		                        days: days,
		                        type: 'create'
		                    },
		                    type: "POST",
		                    success: function (data) {

		                    	calendar.addEvent({
		                    		id: data.id,
						            title: title,
						            start: arg.start,
						            end: arg.end,
						            allDay: arg.allDay
						          })

		                    	calendar.unselect();

		                    }
	                	});
			          
			        }
			    },
			    eventClick: function(arg) {

			        if (confirm('Estas seguro de querer eliminar el apartado?')) {

			        	$.ajax({
		                    url: "{{route('teacher.schedule.ajax')}}",
		                    data: {
		                        id: arg.event.id,
		                        type: 'delete'
		                    },
		                    type: "POST",
		                    success: function (data) {
		                    	arg.event.remove();
		                    	displayMessage("Se ha borrado correctamente.");
		                    }
	                	});
			          	
			        }
			    },

	        });

	        calendar.render();

	        function displayMessage(message) {
			    toastr.success(message, 'Apartado');          
			}
		});
    </script>
@endpush