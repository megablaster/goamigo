@extends('teacher.layouts.admin')

@section('title')
    Facturas
@endsection

@section('subtitle')
@endsection

@push('css')
    <link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')

    <div class="row">
        <div class="col-xl-12">

            @if (Session::has('success'))
                <div class="alert alert-success">
                    {!! Session::get('success') !!}
                </div>
            @endif

            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">Mis facturas</h4>
                    <div class="table-responsive">
                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Alumno</th>
                                    <th>Estatus</th>
                                    <th>Fecha de clase</th>
                                    <th>Ingreso</th>
                                    <th width="1em">{{__('user.table.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['invoices'] as $invoice)
                                    <tr>
                                        <td>{{$invoice->id}}</td>
                                        <td>{{$invoice->code}}</td>
                                        <td>{{$invoice->student->name}} {{$invoice->student->last_name}}</td>
                                        <td>{!!$invoice->estatus!!}</td>
                                        <td>{{$invoice->schedule->start}}</td>
                                        <td>{!!$invoice->income!!}</td>
                                        <td>
                                            <a href="{{route('schedulestudent.show',$invoice->schedule->id)}}" class="btn btn-sm btn-info" data-id="{{$invoice->id}}">Ver clase</a>
                                        </td>
                                    </tr>   
                                @endforeach

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
        //Preguntar si desea borrar
        $('.delete').on('click', function(e){

            e.preventDefault();
            var id = $(this).data('id');

            Swal.fire({
              title: '¿Estas seguro?',
              text: "Esta a apunto de eliminar el usuario.",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              cancelButtonText: 'Cancelar',
              confirmButtonText: '¡Si, eliminar!'
            }).then((result) => {
              if (result.isConfirmed) {
                $('#delete-'+id).submit();
              }
            })

        });
        
    </script>   
@endpush