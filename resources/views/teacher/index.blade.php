@extends('teacher.layouts.admin')

@section('title')
    Estadísticas
@endsection

@section('subtitle')
    Bienvenido, {{Auth::user()->name}}
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link rel="stylesheet" href="{{asset('admin/assets/libs/owl/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/libs/owl/assets/owl.theme.default.min.css')}}">
@endpush

@section('content')

    @if($data['classes']->count() > 1)
        <div class="card-group">
        <div class="card border-right">
            <div class="card-body">
                <h3>Próximas clases</h3>

                <div class="owl-carousel owl-theme owl-class-next">

                    @foreach($data['classes'] as $class)

                        <div class="item">
                            <a href="{{route('classroomteacher.rate',$class->schedule->id)}}">
                                <div class="img" style="background-image: url('{{route('get.image',$class->student->img)}}');">
                                    <div class="text">
                                        <p>
                                            <strong>Nickname:</strong> {{$class->student->student->nickname}}<br>
                                            @php
                                                $date = Carbon\Carbon::parse($class->schedule->start)
                                            @endphp
                                            <strong>Falta:</strong> {{$date->diffForHumans()}} <br>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>

                    @endforeach

                </div>

            </div>
        </div>
    </div>
    @endif
    <div class="row" id="profile-status">
        <a href="#" class="btn btn-xs btn-float">Editar información</a>
        <div class="col-lg-4 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4">
                            <div class="img-profile" style="background-image: url('{{route('get.image',auth()->user()
                            ->img)}}');"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="col-xl-8">
                                <p>
                                    <strong>Nombre:</strong> {{auth()->user()->name}} {{auth()->user()->last_name}}<br>
                                    <strong>Zona horaria:</strong> {{auth()->user()->timezone->name}} <br>
                                    <strong>Nivel educativo:</strong> {{auth()->user()->teacher->career}}<br>
                                    <strong>Puntos acumulados: </strong> 13<br>
                                    <strong>Experiencia: </strong> {{auth()->user()->teacher->experience}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <ul class="stars-profile">
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="far fa-star"></i></li>
                        <li><i class="far fa-star"></i></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-12" id="student-comments">
            <div class="card">
                <div class="card-body">
                    <h3 class="card-title">Comentarios de alumnos</h3>

                        <section class="comment-item">
                            <div class="row">
                                <div class="col-xl-2 p-right">
                                    <div class="img" style="background-image: url('#')"></div>
                                </div>
                                <div class="col-xl-10 p-left">
                                    <div class="info">
                                        <ul class="stars">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                        </ul>
                                        <h4>Domingo 6 de marzo 2020</h4>
                                        <p>Excelente maestro, estoy aprendiendo mucho con el.</p>
                                    </div>
                                </div>
                            </div>
                        </section>

                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script src="{{asset('admin/assets/libs/owl/owl.carousel.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            //Owl Carousel
            $('.owl-class-next').owlCarousel({
                loop:false,
                margin:20,
                nav:false,
                responsive:{
                    0:{
                        items:2
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            });

        });
    </script>
@endpush
