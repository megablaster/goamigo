<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('admin/assets/images/favicon.png')}}">
    <title>Inicia sesión - {{config('app.name')}}</title>
    <style>
        @font-face {
            font-family: 'ProgramOT';
            src: url('{{asset('fonts/ProgramOT-Bold.woff2')}}') format('woff2'),
                url('{{asset('fonts/ProgramOT-Bold.woff')}}') format('woff'),
                url('{{asset('fonts/ProgramOT-Bold.ttf')}}') format('woff');
            font-weight: bold;
            font-style: normal;
            font-display:swap;
        }
    </style>
    <link href="{{asset('/admin/dist/css/style.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/admin.css')}}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="main-wrapper" style="background-image: url('{{asset('assets/img/bg-stars.jpg')}}');">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-8 bg-white offset-md-3 offset-2">
                    <h2 class="text-center">¡Bienvenido!</h2>
                    <a href="{{route('front.index')}}">
                        <img src="{{asset('assets/img/logo.svg')}}" class="logo img-fluid" style="margin:10px auto;">
                    </a>
                    <img src="{{asset('assets/img/mascota.png')}}" alt="Mascota" class="img-fluid mascota">
                    <form method="POST" action="{{ route('login') }}" class="login-form">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                @if (session('userCheck'))
                                    <div class="alert alert-success">
                                        {{ session('userCheck') }}
                                    </div>
                                @endif
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-12 text-center">
                                <button type="submit" class="btn btn-block btn-blue">{{ __('Login') }}</button><hr>
                                <a href="{{route('register.student')}}" class="btn btn-block btn-register">{{__('register_student.button')}}</a>
                            </div>
                            <a href="{{ route('password.request') }}" class="p" style="margin-top:15px;">{{ __('Forgot Your Password?') }}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script>
        $(".preloader ").fadeOut();
    </script>
</body>

</html>