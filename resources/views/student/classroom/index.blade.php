@extends('student.layouts.admin')

@section('title')
	Próximas clases
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href="{{asset('admin/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('script/sweet/sweetalert2.min.css')}}">
	<link rel="stylesheet" href="{{asset('script/fancybox/jquery.fancybox.min.css')}}">
@endpush

@section('content')


<section id="package" class="section-admin">

    <section class="header" style="background-image: url('{{asset('assets/img/section/banner.png')}}')">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <h2 style="margin-bottom:0;">My classes</h2>
                </div>
            </div>
        </div>
    </section>

	<div class="container-fluid">

		<div class="row">
			<div class="col-xl-12">

				@if (Session::has('success'))
				    <div class="alert alert-success">
				        {!! Session::get('success') !!}
				    </div>
				@endif

				<div class="card">
			        <div class="card-body">
			            <h4 class="card-title">Mis clases</h4>

			            <div class="table-responsive">
	                        <table id="multi_col_order" class="table table-striped table-bordered display no-wrap table-sm" style="width:100%">
	                            <thead>
	                                <tr>
	                                	<th>#</th>
	                                    <th>Profesor</th>
	                                    <th>Fecha de clase</th>
	                                    <th>Hora de la clase</th>
	                                    <th>Status</th>
	                                    <th width="1em">{{__('user.table.action')}}</th>
	                                </tr>
	                            </thead>
	                            <tbody>

	                            	@foreach ($data['schedules'] as $key => $schedule)
	                            		<tr>
	                            			<td>{{$schedule->id}}</td>
		                                    <td>{{$schedule->teacher->name}} {{$schedule->teacher->last_name}}</td>
		                                    <td>{{ Carbon\Carbon::parse($schedule->schedule->start)->format('d-m-Y') }}</td>
		                                    <td>{{ Carbon\Carbon::parse($schedule->schedule->start)->format('h:i:s A') }}</td>
		                                    <td>{!!$schedule->estatus!!}</td>
		                                    <td>
	                                            @if($schedule->classroom->url)
	                                                <a href="{{$schedule->classroom->url}}" target="_blank" class="btn btn-success btn-sm">Entrar a la clase</a>
	                                            @endif
		                                    	@if ($schedule->status == 'active')
		                                    		{{-- @if (\Carbon\Carbon::now() >= $schedule->schedule->end)) --}}
		                                    			<a href="{{route('schedulestudent.show',$schedule->id)}}" class="btn btn-primary btn-sm">
			                                    			Calificar clase
			                                    		</a>
		                                    		{{-- @endif --}}
			                                    	<form id="cancelMore-{{$schedule->id}}" action="{{route('student.schedule.cancel')}}" method="post" style="display: none;">
			                                    		<input type="hidden" name="teacher" value="{{$schedule->teacher}}">
			                                    		<input type="hidden" name="id" value="{{$schedule->id}}">
			                                    		<input type="hidden" name="type" value="more">
			                                    		@csrf
			                                    	</form>
			                                    	<form id="cancelMin-{{$schedule->id}}" action="{{route('student.schedule.cancel')}}" method="post" style="display: none;">
			                                    		<input type="hidden" name="teacher" value="{{$schedule->teacher}}">
			                                    		<input type="hidden" name="id" value="{{$schedule->id}}">
			                                    		<input type="hidden" name="type" value="min">
			                                    		@csrf
			                                    	</form>
			                                    	{!!$schedule->cancel()!!}
		                                    	@endif
		                                    </td>
		                                </tr>
	                            	@endforeach

	                            </tbody>
	                        </table>
			        	</div>

			    	</div>
		    	</div>

		    </div>
	    </div>
   	</div>

</section>

@endsection

@push('js')
	<script src="{{asset('script/fancybox/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
    <script src="{{asset('script/sweet/sweetalert2.min.js')}}"></script>
    <script>
		//Preguntar si desea cancelar
		$('.cancelMore24').on('click', function(e){
			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: 'Solicitar cancelación',
			  text: $(this).data('text'),
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cerrar',
			  confirmButtonText: '¡Si, cancelar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#cancelMore-'+id).submit();
			  }
			})

		});

		$('.cancelMin24').on('click', function(e){

			e.preventDefault();
			var id = $(this).data('id');

			Swal.fire({
			  title: 'Cancelación',
			  text: $(this).data('text'),
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  cancelButtonText: 'Cerrar',
			  confirmButtonText: '¡Si, cancelar!'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	$('#cancelMin-'+id).submit();
			  }
			})

		});
	</script>
@endpush
