@extends('student.layouts.admin')

@section('title')
	Mi clase
@endsection

@section('subtitle')
@endsection

@push('css')
@endpush

@section('content')

    <div class="container-fluid">
        <div class="row" id="schedulestudent">
            <div class="col-lg-8">

                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <h3>Datos del profesor</h3>
                                <div class="info-teacher">
                                    <div class="img" style="background-image: url('{{route('get.image',$data['schedule']->teacher->img)}}')"></div>
                                    <strong>Tutor:</strong> {{$data['schedule']->teacher->name}} {{$data['schedule']->teacher->last_name}}<br>
                                    <strong>Educación:</strong> {{$data['schedule']->teacher->teacher->career}}<br>
                                    <strong>País:</strong> {{$data['schedule']->teacher->country}}<br>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <h3>Calificar la clase</h3>
                                <form action="{{route('reviews.update',$data['schedule']->classroom->review->id)}}" method="post">
                                    @csrf
                                    <input type="hidden" name="teacher_id" value="{{$data['schedule']->teacher->id}}">
                                    <input type="hidden" name="review_id" value="{{$data['schedule']->classroom->review->id}}">
                                    <input type="hidden" name="schedule_id" value="{{$data['schedule']->schedule_id}}">
                                    {{ method_field('PATCH') }}
                                    <div class="form-group">
                                        <label>Selecciona las estrellas:</label>
                                        <select name="stars" class="form-control">
                                            <option value="1" {{($data['schedule']->classroom->review->stars == 1)?'selected':''}}>1 Estrella</option>
                                            <option value="2" {{($data['schedule']->classroom->review->stars == 2)?'selected':''}}>2 Estrellas</option>
                                            <option value="3" {{($data['schedule']->classroom->review->stars == 3)?'selected':''}}>3 Estrellas</option>
                                            <option value="4" {{($data['schedule']->classroom->review->stars == 4)?'selected':''}}>4 Estrellas</option>
                                            <option value="5" {{($data['schedule']->classroom->review->stars == 5)?'selected':''}}>5 Estrellas</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Comparte los comentarios sobre el profesor:</label>
                                        <textarea name="comments" class="form-control" rows="6" required>{{$data['schedule']->classroom->review->comment_dad}}</textarea>
                                    </div>
                                    <button type="submit" class="btn btn-submit btn-success">Calificar</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Últimos comentarios</h4>

                        <div class="info">
                            @if ($data['lastcomments'])

                                @foreach($data['lastcomments'] as $comment)

                                    <div class="comment">
                                        <ul>
                                            <li>{!!@$comment->classroom->review->starsfull!!}</li>
                                            <li>|</li>
                                            <li>{{$comment->schedule->start}}</li>
                                        </ul>
                                        <p>{{@$comment->classroom->review->comment_dad}}</p>
                                    </div>

                                @endforeach

                            @else
                                <h5>No hay comentarios por el momento.</h5>
                            @endif
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@push('js')
	<script src="https://kit.fontawesome.com/14823deefc.js" crossorigin="anonymous"></script>
@endpush
