@extends('student.layouts.admin')

@section('title')
    Paquetes
@endsection

@section('subtitle')
    Bienvenido, {{Auth::user()->name}}
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.min.css">
@endpush

@section('content')

<section id="package" class="section-admin">

    <section class="header" style="background-image: url('{{asset('assets/img/section/banner.png')}}')">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <h2 style="margin-bottom:0;">Packages</h2>
                </div>
            </div>
        </div>
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-xl-10 offset-xl-1">
                <h3 class="text-center">Para tener acceso a una clase, recuerde que necesita adquirir créditos, los cuales tienen vigencia para su uso.</h3><br>
            </div>

             @foreach ($data['products'] as $product)

                <div class="col-xl-4 text-center">
                    <div class="card-group">
                        <div class="card">
                            <div class="card-body">
                                <h1>{{$product->name}}</h1>
                                <h3>${{$product->price}} USD x {{$product->days}} días</h3>
                                <a href="#form-pay" data-product="{{$product->id}}" data-fancybox class="btn btn-info click-form">Adquirir</a>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

            <div id="form-pay" style="display: none;width: 100%;max-width: 600px;">

                <form id="card-form">
                    <div class="row">
                        <div class="col-xl-12">
                            <h2>Método de pago</h2>
                        </div>
                    </div>

                    <div class="form group">
                        <label>Nombre del titular</label>
                        <input id="card-holder-name" type="text" class="form-control" required>
                    </div>

                    <!-- Stripe Elements Placeholder -->
                    <div class="form-group">
                        <label>No. de tarjeta</label>
                        <div id="card-element" class="form-control"></div>
                        <span id="card-error"></span>
                    </div>

                    <button id="card-button" class="btn btn-primary btn-block">Realizar pago</button>
                </form>

            </div>
        </div>
    </div>

</section>

@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        $(document).ready(function(){

            var product_id = '';
            var form = $('#card-form');

            //Obtenemos el id de producto
            $('.click-form').on('click', function(){
                var product_id = $(this).data('product');
                stripe(product_id);
            });

        });
    </script>
    <script>
        function stripe(product_id){
            const stripe = Stripe("{{$global_system->stripe_key}}");

            const elements = stripe.elements();
            const cardElement = elements.create('card');

            cardElement.mount('#card-element');

            //Método de pago
            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const cardForm = document.getElementById('card-form');

            cardForm.addEventListener('submit', async (e) => {
                e.preventDefault();

                //Change button send
                var button = $('#card-button');
                button.prop('disabled',true);
                button.html('Procesando la solicitud...');

                const { paymentMethod, error } = await stripe.createPaymentMethod(
                    'card', cardElement, {
                        billing_details: { name: cardHolderName.value }
                    }
                );

                if (error) {

                    document.getElementById('card-error').textContent = error.message;

                } else {

                    $.ajax({
                        url: '{{route('payment.stripe')}}',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": paymentMethod.id,
                            'product_id': product_id
                        },
                        method: "POST",
                        success: function(res){

                            console.log(res.er);

                            if(res.error){

                                //Close Fancybox
                                $.fancybox.close();

                                //Change button send
                                button.prop('disabled',false);
                                button.html('Realizar pago');

                                //Alert success
                                Swal.fire({
                                  position: 'top-end',
                                  icon: 'error',
                                  title: 'Ha ocurrido un error',
                                  text: res.error,
                                  showConfirmButton: false,
                                  timer: 2500
                                });

                            } else {

                                //Close Fancybox
                                $.fancybox.close();

                                //Change button send
                                button.prop('disabled',false);
                                button.html('Realizar pago');

                                //Alert success
                                Swal.fire({
                                  position: 'top-end',
                                  icon: 'success',
                                  title: 'Comprado correctamente',
                                  text: res.success,
                                  showConfirmButton: false,
                                  timer: 2000
                                });

                                //Reload page
                                window.setTimeout(
                                  function(){
                                    location.reload(true)
                                  },
                                  2000
                                );

                            }
                        },
                        fail: function(e){
                            alert(e.error);

                            //Change button send
                            button.prop('disabled',false);
                            button.html('Realizar pago');

                            //Alert error
                            Swal.fire({
                              position: 'top-end',
                              icon: 'error',
                              title: 'Ha ocurrido un error',
                              text: 'Intenta de nuevo más tarde',
                              showConfirmButton: false,
                              timer: 2500
                            });
                        }
                    });
                }
            });
        }

    </script>
@endpush
