@extends('student.layouts.admin')

@section('title','Profesor')

@push('css')
	<link href='https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.1/css/all.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
@endpush

@section('content')

	<div class="row info-student">
		<div class="col-12">
			@if (Session::has('success'))
			    <div class="alert alert-success">
			        {!! Session::get('success') !!}
			    </div>
			@endif
		</div>
		<div class="col-xl-7">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-3">
							<div class="img" style="background-image: url('{{route('get.image',$data['teacher']->img)}}');"></div>
						</div>
						<div class="col-lg-9">
							<h2>{{$data['teacher']->name}} {{$data['teacher']->last_name}}</h2>
							<p>{{$data['teacher']->teacher->letter_presentation}}</p><hr>
							<a href="#reviews" data-fancybox class="btn btn-info">Ver opiniones</a>
							@if ($data['favorite'])
								<form action="{{route('favorites.store')}}" method="post" style="margin-bottom: 10px;display: inline-block;">
									@csrf
									<input type="hidden" name="id" value="{{$data['favorite']->id}}">
									<input type="hidden" name="type" value="remove">
									<input type="hidden" name="teacher_id" value="{{$data['teacher']->id}}">
									<button type="submit" class="btn btn-danger"><i class="fas fa-heart"></i> Quitar favorito</button>
								</form>
							@else
								<form action="{{route('favorites.store')}}" method="post" style="margin-bottom: 10px;display: inline-block;">
									@csrf
									<input type="hidden" name="type" value="add">
									<input type="hidden" name="user_id" value="{{auth()->user()->id}}">
									<input type="hidden" name="teacher_id" value="{{$data['teacher']->id}}">
									<button type="submit" class="btn btn-danger"><i class="far fa-heart"></i> Añadir favorito</button>
								</form>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="reviews" style="width: 100%;max-width: 650px;display: none;">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h3>¿Que opinan de <strong>{{$data['teacher']->name}}</strong>?</h3>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing, elit. Aut vitae aspernatur natus, vero repudiandae ea aliquam praesentium harum architecto est doloribus, a, asperiores et suscipit sequi autem necessitatibus voluptate dolore?</p>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-12">
			
			<div class="card">
		        <div class="card-body">

			        <div id='calendar'></div>

		        </div>
		    </div>

	    </div>
    </div>

@endsection

@push('js')
	<script src="https://momentjs.com/downloads/moment.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script>
    	$(document).ready(function () {
    		var today = moment();
    		today = today.format('YYYY-MM-DD');

		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });

		    var calendarEl = document.getElementById('calendar');
	        var calendar = new FullCalendar.Calendar(calendarEl, {
	        	selectAllow: function(select) {
				      return moment().diff(select.start) <= 0
				},
				eventOverlap: false,
				// timeZone: 'UTC',
	        	locale: 'es',
	        	selectable: true,
				editable: false,
				droppable: false,
				initialView: 'timeGridWeek',
				initialDate: today,
				expandRows: false,
				allDaySlot: false,
				selectOverlap: false,
    			dayHeaders: true,
				scrollTime: '00:00:00',
				slotDuration: '00:30:00',
				themeSystem: 'bootstrap',
				nowIndicator: true,
				displayEventTime: false,
				buttonText: {
			      dayGridMonth: 'Mes',
			      timeGridWeek: 'Semana',
			      timeGridDay: 'Día',
			      listWeek: 'Lista'
			    },
				slotLabelFormat: {
					hour: 'numeric',
					minute: '2-digit',
					omitZeroMinute: false,
					hour12: false 
				},
				eventTimeFormat: {
					hour: 'numeric',
					minute: '2-digit',
					omitZeroMinute: false,
					hour12: false 
				},
		        events: "{{route('student.schedule.get', $data['teacher']->id )}}",
		       	eventColor: '#378006',
		       	eventDisplay: 'block',
		        displayEventTime: true,
		        headerToolbar: {
		        	start: 'timeGridWeek',
		        	center:'title',
		        	end: 'prev,next',
		        },
		        select: function(arg) {
			    },
			    eventClick: function(arg) {

			    	Swal.fire({
					  title: '¿Deseas agendar esta clase?',
					  showDenyButton: false,
					  showCancelButton: true,
					  confirmButtonText: 'Agendar',
					  cancelButtonText: 'Cancelar',
					}).then((result) => {
					  
					  if (result.isConfirmed) {

					  	//Add Schedule
					    $.ajax({
						   url: "{{route('teacher.schedule')}}",
						   data: {
						       schedule_id: arg.event.id,
						       user_id: {{auth()->user()->id}},
						       name: '{{ $data['teacher']->name }}',
						       teacher_id: {{$data['teacher']->id}}
						   },
						   type: "POST",
						   success: function (data) {

						   		if(data.status == 'success'){

						   			Swal.fire('Éxito', data.text , 'success');
						   			
						   			setTimeout(function(){
						   				location.reload();
						   			}, 1000);


						   		} else if(data.status == 'failed') {

						   			Swal.fire('Atención', data.text , 'warning');

						   			// setTimeout(function(){
						   			// 	location.reload();
						   			// }, 3000);
						   			
						   		}
						   },
						   error: function(data){

						   }
						});

					  }
					})
			       
					// $.ajax({
					//    url: "{{route('teacher.schedule.ajax')}}",
					//    data: {
					//        id: arg.event.id,
					//        type: 'delete'
					//    },
					//    type: "POST",
					//    success: function (data) {
					//    	arg.event.remove();
					//    	displayMessage("Se ha borrado correctamente.");
					//    }
					// });
						
			    },

	        });

	        calendar.render();

	        function displayMessage(message) {
			    toastr.success(message, 'Apartado');          
			}
		});

	
    </script>
@endpush