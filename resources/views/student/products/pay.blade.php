@extends('student.layouts.admin')

@section('title')
	Comprar producto
@endsection

@push('css')
@endpush

@push('js')
@endpush

@section('content')
    
    <div class="row">

        <div class="col-xl-6">

            <div class="card">
                <div class="card-body">
                    <h1>{{$product->name}} <strong style="font-size:18px;">${{$product->price}} USD</strong></h1>
                    <p>{{$product->description}}</p>
                </div>
            </div>

        </div>
        <div class="col-xl-6">
            <div class="card">

                <div class="card-body">

                    <form id="card-form">
                        <div class="row">
                            <div class="col-xl-12">
                                <h2>Método de pago</h2>
                            </div>
                        </div>

                        <div class="form group">
                            <label>Nombre del titular</label>
                            <input id="card-holder-name" type="text" class="form-control" required>
                        </div>

                        <!-- Stripe Elements Placeholder -->
                        <div class="form-group">
                            <label>No. de tarjeta</label>
                            <div id="card-element" class="form-control"></div>
                            <span id="card-error"></span>
                        </div>

                        <button id="card-button" class="btn btn-primary btn-block">Realizar pago</button>
                    </form>

                </div>
            </div>

        </div>

    </div>
    
@endsection

@push('js')
    <script>
        $(document).ready(function(){
            var form = $('#card-form');
            stripe();
        });
    </script>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        function stripe(){
            const stripe = Stripe("{{env('STRIPE_KEY')}}");

            const elements = stripe.elements();
            const cardElement = elements.create('card');

            cardElement.mount('#card-element');

            //Método de pago
            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const cardForm = document.getElementById('card-form');

            cardForm.addEventListener('submit', async (e) => {
                e.preventDefault();

                const { paymentMethod, error } = await stripe.createPaymentMethod(
                    'card', cardElement, {
                        billing_details: { name: cardHolderName.value }
                    }
                );

                if (error) {
                    document.getElementById('card-error').textContent = error.message;
                } else {
                    // alert(paymentMethod.id);

                    $.ajax({
                        url: '{{route('payment.stripe')}}',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id": paymentMethod.id,
                            'product_id': {{$product->id}}
                        },
                        method: "POST",
                        success: function(res){
                            alert(res.success);
                            window.location.href = "{{ route('products.index')}}";
                        },
                        fail: function(e){
                            alert(e.error);
                        }
                    });
                }
            });
        }
        
    </script>

@endpush