@php
    $current = Route::currentRouteName();
@endphp

@if($global_system->type_app == 'live')
    <li class="nav-small-cap">
        <span class="hide-menu">Mis clases</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('lessons.index')}}" aria-expanded="false">
            <i data-feather="calendar" class="feather-icon"></i>
            <span class="hide-menu">Agendar clase</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('schedulestudent.index')}}" aria-expanded="false">
            <i data-feather="book-open" class="feather-icon"></i>
            <span class="hide-menu">Mis clases</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('admin.student.package')}}" aria-expanded="false">
            <i data-feather="package" class="feather-icon"></i>
            <span class="hide-menu">Paquetes</span>
        </a>
    </li>
    <li class="nav-small-cap">
        <span class="hide-menu">Historial</span>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('invoice.lesson')}}" aria-expanded="false">
            <i data-feather="list" class="feather-icon"></i>
            <span class="hide-menu">Clases</span>
        </a>
    </li>
    <li class="sidebar-item">
        <a class="sidebar-link" href="{{route('invoice.product')}}" aria-expanded="false">
            <i data-feather="list" class="feather-icon"></i>
            <span class="hide-menu">Paquetes</span>
        </a>
    </li>
    <li class="list-divider"></li>
@endif

<li class="nav-small-cap">
    <span class="hide-menu">{{__('admin.account.account')}}</span>
</li>
<li class="sidebar-item">
    <a class="sidebar-link sidebar-link" href="{{route('student.edit',auth()->user()->id_unique)}}" aria-expanded="false">
        <i data-feather="user-check" class="feather-icon"></i>
        <span class="hide-menu">Mi cuenta</span>
    </a>
</li>
<li class="sidebar-item">
    <a class="sidebar-link sidebar-link" href="{{route('logout.click')}}" aria-expanded="false">
        <i data-feather="log-out" class="feather-icon"></i>
        <span class="hide-menu">{{__('admin.account.logout')}}</span>
    </a>
</li>
