@extends('student.layouts.admin')

@section('title')
@endsection

@section('subtitle')
@endsection

@push('css')
@endpush

@section('content')

<section id="index-student">

    <section id="header">
        <div class="container-fluid">
            <div class="row">

                @if($data['schedule']->count() != 0)

                    <div class="col-xl-4">

                        <div class="teacher-next">
                            <h2>Next class</h2>
                            <a href="#" class="img" style="background-image: url('{{route('get.image',$data['schedule']->teacher->img)}}');">
                            </a>
                            <h3>{{$data['schedule']->teacher->name}}</h3>
                            <div id="count"></div>
                            <div id="next"></div>
                            <p>{{$data['schedule']->teacher->teacher->experience}}</p>
                        </div>

                    </div>
                    <div class="col-xl-8 text-center">
                        <h3 class="greeting">Hola, <strong>{{ auth()->user()->name }}</strong>.</h3>
                    </div>

                @else

                    <div class="col-xl-12 text-center">
                        <h3>Hola, <strong>{{ auth()->user()->name }}</strong>, Aún no has reservado ninguna clase...</h3>
                    </div>

                @endif

            </div>
        </div>
    </section>

    <div class="container-fluid">

        <div class="card-group">
            <div class="card border-right">
                <div class="card-body">
                    <div class="col-xl-12 text-center">
                        <h1>{{@Auth::user()->credit->credit}}</h1>
                        <h6>Créditos restantes</h6>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="col-xl-12 text-center">
                        <h1>{{$data['pasts']->count()}}</h1>
                        <h6>Clases cursadas</h6>
                    </div>
                </div>
            </div>
            <div class="card border-right">
                <div class="card-body">
                    <div class="col-xl-12 text-center">
                        <h1>{{$data['schedules']->count()}}</h1>
                        <h6>Próximas clases</h6>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>

@endsection

@push('js')
    <script>
        var countDownDate = new Date("{{$data['schedule']->schedule->start}}").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("count").innerHTML = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";

            // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("count").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>
@endpush
