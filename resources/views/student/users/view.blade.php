@extends('student.layouts.admin')

@section('title')
@endsection

@push('css')
@endpush

@section('content')

<section class="section-admin">

	<section class="header" style="background-image: url('{{asset('assets/img/section/banner.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h2 style="padding:120px 0;margin:0;">Mi cuenta</h2>
				</div>
			</div>
		</div>
	</section>

	<div class="container-fluid">
		<div class="row">
            <div class="col-xl-12">

                <div class="card">
                    <div class="card-body">
                        <h2>{{$data['user']->student->nickname}}#{{$data['user']->uid}}</h2>
                        <div class="col-xl-4">

                        </div>
                        <div class="col-xl-8">

                        </div>
                    </div>
                </div>

            </div>
		</div>
	</div>

</section>



@endsection

@push('js')
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
@endpush
