@extends('student.layouts.admin')

@section('title')
	{{__('user.edit.title')}}
@endsection

@push('css')
@endpush

@section('content')

<section class="section-admin">

	<section class="header" style="background-image: url('{{asset('assets/img/section/banner.png')}}')">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h2 style="margin-bottom:0;">Edit account</h2>
				</div>
			</div>
		</div>
	</section>

	<div class="container-fluid">
		<div class="row">

			<div class="col-xl-12">
				@if (Session::has('success'))
					<div class="alert alert-success">
						{!! Session::get('success') !!}
					</div>
				@endif
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<div class="card">
					<div class="card-body">

						<form action="{{route('student.update',$data['user']->id_unique)}}" method="post" enctype="multipart/form-data">
							@csrf
							{{ method_field('PATCH') }}
							<div class="row">
                                <div class="col-xl-12">
                                    <h3><strong>Profile picture</strong></h3><hr>
                                    <div class="img" style="background-image: url('{{route('get.image',$data['user']->img)}}');height: 150px;
                                        width: 150px;
                                        border-radius: 50%;
                                        background-size: cover;
                                        background-repeat: no-repeat;
                                        background-position: center;
                                        border: 1px solid #ececec;
                                        margin-bottom: 15px;"></div>
                                    <div class="form-group">
                                        <input type="file" name="image" class="form-control">
                                    </div>
                                </div>
								<div class="col-12">
									<h3><strong>{{__('user.edit.subtitle')}}</strong></h3><hr>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.name')}}</label>
										<input type="text" name="name" class="form-control" value="{{$data['user']->name}}">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.last_name')}}</label>
										<input type="text" name="last_name" class="form-control" value="{{$data['user']->last_name}}">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.email')}}</label>
										<input type="email" name="email" class="form-control" value="{{$data['user']->email}}" readonly>
									</div>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.gender')}}</label>
										<select name="gender" class="form-control">
											<option value="male" {{($data['user']->gender == 'male')? 'selected':''}}>{{__('general.gender.male')}}</option>
											<option value="female" {{($data['user']->gender == 'female')? 'selected':''}}>{{__('general.gender.female')}}</option>
											<option value="undefined" {{($data['user']->gender == 'undefined')? 'selected':''}}>{{__('general.gender.undefined')}}</option>
										</select>
									</div>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.phone')}}</label>
										<input type="tel" name="phone" class="form-control" value="{{$data['user']->phone}}">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.city')}}</label>
										<input type="text" name="city" class="form-control" value="{{$data['user']->city}}">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.country')}}</label>
										<input type="text" name="country" class="form-control" value="{{$data['user']->country}}">
									</div>
								</div>
								@role('admin')
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.role')}}</label>
										<select name="rol"  class="form-control">
											<option value="admin" {{($data['user']->hasRole('admin')? 'selected':'')}}>{{__('general.role.admin')}}</option>
											<option value="manager" {{($data['user']->hasRole('manager')? 'selected':'')}}>{{__('general.role.manager')}}</option>
											<option value="student" {{($data['user']->hasRole('student')? 'selected':'')}}>{{__('general.role.student')}}</option>
											<option value="teacher" {{($data['user']->hasRole('teacher')? 'selected':'')}}>{{__('general.role.teacher')}}</option>
										</select>
									</div>
								</div>
								@endrole
								<div class="col-xl-4">
									<div class="form-group">
										<label>TimeZone</label>
										<select name="timezone_id" class="form-control">
											@foreach ($data['timezones'] as $timezone)
												<option value="{{$timezone->id}}" {{($data['user']->timezone_id == $timezone->id)? 'selected':''}}>{{$timezone->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-12">
									<h4>{{__('user.edit.change_password')}}</h4><hr>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.password')}}</label>
										<input type="password" name="password" class="form-control {{($errors->first('password'))? 'invalid':''}}">
									</div>
								</div>
								<div class="col-xl-4">
									<div class="form-group">
										<label>{{__('user.edit.repeat_password')}}</label>
										<input type="password" name="password_confirmation" class="form-control {{($errors->first('password_confirmation'))? 'invalid':''}}">
									</div>
								</div>
								<div class="col-xl-12">
									<span class="badge badge-warning" style="font-size: 14px;">La contraseña debe cumplir con mínimo 6 caracteres, 1 mayúscula, 1 minúscula y un signo.</span>
								</div><br><br>
							</div>
							<div class="row">
								<div class="col-xl-12">
									<a href="{{route('admin.index')}}" class="btn btn-danger">{{__('user.edit.exit')}}</a>
									<button type="submit" class="btn btn-success">{{__('user.edit.save')}}</button>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
			<div class="col-xl-12">
				@if ($data['user']->hasRole('student'))
					<div class="row">
						<div class="col-xl-12">
							<div class="card">
								<div class="card-body">
									<form action="{{route('infostudents.update',$data['user']->id_unique,$data['user']->student->id)}}" method="post">
										@csrf
										{{ method_field('PATCH') }}
										<input type="hidden" name="user_id" value="{{$data['user']->id}}">
										<div class="row">
											<div class="col-12">
												<h3><strong>Información estudiante</strong></h3><hr>
											</div>
											<div class="col-xl-4">
												<div class="form-group">
													<label>Apodo</label>
													<input type="text" name="nickname" class="form-control" value="{{@$data['user']->student->nickname}}">
												</div>
											</div>
											<div class="col-xl-4">
												<div class="form-group">
													<label>Fecha de nacimiento</label>
													<input type="date" name="birthday" class="form-control" value="{{@$data['user']->student->birthday}}">
												</div>
											</div>
											<div class="col-xl-12">
												<div class="form-group">
													<label>¿Por que necesita que aprenda español?</label>
													<textarea name="comments_learn" class="form-control" rows="4">{{@$data['user']->student->comments_learn}}</textarea>
												</div>
											</div>
											<div class="col-xl-12">
												<div class="form-group">
													<label>¿Comentarios sobre el niñ@?</label>
													<textarea name="comments_children" class="form-control" rows="4">{{@$data['user']->student->comments_children}}</textarea>
												</div>
											</div>
										</div>
										<hr>
										<div class="row">
											<div class="col-xl-12">
												<button type="submit" class="btn btn-success">{{__('user.edit.save')}}</button>
											</div>
										</div>
									</form>

								</div>
							</div>
						</div>
					</div>
				@endif
				@if ($data['user']->hasRole('teacher'))
					<div class="row">
						<div class="col-xl-12">
							<div class="card">
								<div class="card-body">
									<form action="{{route('users.update',$data['user']->id)}}" method="post">
										@csrf
										{{ method_field('PATCH') }}
										<div class="row">
											<div class="col-12">
												<h3><strong>Información maestro</strong></h3><hr>
											</div>
											@if (auth()->user()->hasRole('admin'))
												<div class="col-xl-2">
													<div class="form-group">
														<label>Seguimiento</label>
														<select name="step" class="form-control">
															<option value="first" {{ ($data['user']->teacher->step == 'first')?'selected':'' }}>Prospecto</option>
															<option value="second" {{ ($data['user']->teacher->step == 'second')?'selected':'' }}>Revisión</option>
															<option value="third" {{ ($data['user']->teacher->step == 'third')?'selected':'' }}>Contratado</option>
														</select>
													</div>
												</div>
												<div class="col-xl-2">
													<div class="form-group">
														<label>Nivel educativo</label>
														<select name="level_education" class="form-control">
															<option value="tecnico_superior" {{($data['user']->teacher->level_education == 'tecnico_superior')? 'selected':''}}>Técnico superior</option>
															<option value="licenciatura" {{($data['user']->teacher->level_education == 'licenciatura')? 'selected':''}}>Licenciatura</option>
															<option value="posgrado" {{($data['user']->teacher->level_education == 'posgrado')? 'selected':''}}>Posgrado</option>
														</select>
													</div>
												</div>
											@endif
											@if ($data['user']->teacher->cv)
												<div class="col-xl-2">
													<div class="form-group">
														<label>Curriculum Vitae</label>
														<a href="{{route('get.image',$data['user']->teacher->cv)}}" target="_blank" class="btn btn-success btn-block" download>Ver CV</a>
													</div>
												</div>
											@endif
											<div class="col-xl-12">
												<div class="form-group">
													<label>Experiencia</label>
													<textarea name="experience" class="form-control" rows="4">{{$data['user']->teacher->experience}}</textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xl-12">
												<button type="submit" class="btn btn-success">Guardar cambios</button>
											</div>
										</div>
									</form>

								</div>
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>

</section>



@endsection

@push('js')
	<script src="{{asset('admin/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/datatable/datatable-basic.init.js')}}"></script>
@endpush
