@extends('teacher.layouts.admin')

@section('title','Calendario')

@section('subtitle')
    Mis próximas clases
@endsection

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
@endpush

@section('content')
	
   <div class="row">
       <div class="col-xl-12">

       </div>
   </div>

@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>
@endpush
