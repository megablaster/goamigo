<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/jpg" href="{{asset('assets/img/favicon.png')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link href="{{asset('admin/dist/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/admin.css')}}" rel="stylesheet">
    <!-- Hotjar Tracking Code for GoAmigogo | BackEnd Student -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2817841,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    @stack('css')
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header" data-logobg="skin6">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i
                            class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand">
                        <a href="{{route('admin.student.index')}}">
                            <b class="logo-icon"><img src="{{route('get.image',$global_system->logo)}}" alt="homepage" class="dark-logo" /></b>
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav nav-top float-left mr-auto ml-3 pl-1">
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link">
                                <div class="customize-input">
                                    @if (Auth::user()->credit->credit >= 2)
                                        <span class="info-credit"><strong>{{Auth::user()->credit->credit}}</strong> Créditos</span>
                                    @else
                                        <span class="info-credit"><strong>{{Auth::user()->credit->credit}}</strong> Crédito</span>
                                    @endif
                                </div>
                            </a>
                        </li>
                        <li class="nav-item d-none d-md-block">
                            <a class="nav-link">
                                <div class="customize-input">{!!Auth::user()->daysExpiration()!!}</div>
                            </a>
                        </li>
                        <!-- End Notification -->
                        <!-- ============================================================== -->
                    </ul>

                    <ul class="navbar-nav float-right">

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{route('get.image',Auth::user()->img)}}" alt="user" class="rounded-circle" width="40">
                                <span class="ml-2 d-none d-lg-inline-block"><span>{{__('general.hello')}},</span> <span
                                    class="text-dark">{{Auth::user()->name}} {{Auth::user()->last_name}}</span>
                                    <i data-feather="chevron-down" class="svg-icon"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <a class="dropdown-item" href="{{route('student.edit',Auth::user()->id_unique)}}">
                                    <i data-feather="user" class="svg-icon mr-2 ml-1"></i>{{__('general.submenu.profile')}}
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('logout.click')}}">
                                    <i data-feather="power" class="svg-icon mr-2 ml-1"></i> {{__('general.submenu.logout')}}
                                </a>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="left-sidebar" data-sidebarbg="skin6">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar" data-sidebarbg="skin6">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item">
                            <a class="sidebar-link sidebar-link" href="{{route('admin.index')}}" aria-expanded="false">
                                <i data-feather="home" class="feather-icon"></i>
                                @role('student')
                                    <span class="hide-menu">Bienvenido</span>
                                @endrole
                                @role('admin|teacher|manager')
                                    <span class="hide-menu">{{__('admin.statistics')}}</span>
                                @endrole
                            </a>
                        </li>
                        @include('student.parts.navbar')
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>

        <div class="page-wrapper">
            @yield('content')
        </div>

        <a href="#credit-message" id="credit-free" data-fancybox class="btn btn-warning">Lanzar Fancybox</a>

        <div id="credit-message" style="display: none;max-width: 750px;">
            <h3>Obtén tu primer crédito.</h3><hr>
            <p>Complete la siguiente información: @if(auth()->user()->name == null) <strong>Nombre,</strong>@endif @if(auth()->user()->last_name == null) <strong>Apellido,</strong>@endif @if(auth()->user()->student->nickname == null) <strong>Apodo,</strong>@endif @if(auth()->user()->student->birthday == null) <strong>Fecha de nacimiento</strong>@endif para obtener tu primer crédito gratis.</p>
            <p>Solo usaremos esta información para recordarle sus clases, ayudar con cualquier problema técnico, contactar a los padres cuando sea necesario, etc.</p>
            <p>Puede completar esta información en cualquier momento.</p>
            <a href="{{route('student.edit',auth()->user()->id_unique)}}" class="text-center btn btn-primary">Obtener crédito gratis</a>
        </div>

        @include('student.parts.footer')
    </div>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="{{asset('admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/app-style-switcher.js')}}"></script>
    <script src="{{asset('admin/dist/js/feather.min.js')}}"></script>
    <script src="{{asset('admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/sidebarmenu.js')}}"></script>
    <script src="{{asset('admin/dist/js/custom.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script src="{{asset('js/app.js')}}"></script>
    @stack('js')

    @if(auth()->user()->name == null || auth()->user()->last_name == null || auth()->user()->student->nickname == null || auth()->user()->student->birthday == null)
        <script>
            $(document).ready(function(){
                $('#credit-free').trigger('click');
            });
        </script>
    @endif

</body>

</html>
