@extends('student.layouts.admin')

@section('title')
	Zoom preview
@endsection

@section('subtitle')
@endsection

@push('css')
@endpush

@section('content')

	<div class="row">
		<div class="col-xl-12">

			<div class="iframe-container" style="overflow: hidden; padding-top: 56.25%; position: relative;">
			    <iframe allow="microphone; camera" style="border: 0; height: 100%; left: 0; position: absolute; top: 0; width: 100%;" src="https://success.zoom.us/wc/join/{{$id}}" frameborder="0"></iframe>
			</div>

	    </div>
    </div>

@endsection

@push('js')
@endpush