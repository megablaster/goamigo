@extends('student.layouts.admin')

@section('title')
Maestros
@endsection

@section('subtitle')
@endsection

@push('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
@endpush

@section('content')

<section id="package" class="section-admin">

    <section class="header" style="background-image: url('{{asset('assets/img/section/banner.png')}}')">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <h2 style="margin-bottom:0;">Schedule class</h2>
                </div>
            </div>
        </div>
    </section>

	<div class="container-fluid">

		<div class="row">
			<div class="col-xl-12">

				@if (Session::has('success'))
					<div class="alert alert-success">
						{!! Session::get('success') !!}
					</div>
				@endif

				@if (Session::has('failed'))
					<div class="alert alert-danger">
						{!! Session::get('failed') !!}
					</div>
				@endif

				<div class="card">
					<div class="card-body">

						<h4 class="card-title">Filtrar clases</h4>

						<div class="row">
							<div class="col-xl-4">

								<div class="form-teachers">
									<form action="{{route('lesson.search')}}" method="get">
										<div class="form-group">
											<label>Hora:</label>
											<select name="init" class="form-control">
												<option value="*" {{(@$data['init'] == '*')?'selected':'' }}>Cualquier hora</option>
												<option value="01:00:00" {{(@$data['init'] == '01:00:00')?'selected':'' }}>01:00 AM</option>
												<option value="01:30:00" {{(@$data['init'] == '01:30:00')?'selected':'' }}>01:30 AM</option>
												<option value="02:00:00" {{(@$data['init'] == '02:00:00')?'selected':'' }}>02:00 AM</option>
												<option value="02:30:00" {{(@$data['init'] == '02:30:00')?'selected':'' }}>02:30 AM</option>
												<option value="03:00:00" {{(@$data['init'] == '03:00:00')?'selected':'' }}>03:00 AM</option>
												<option value="03:30:00" {{(@$data['init'] == '03:30:00')?'selected':'' }}>03:30 AM</option>
												<option value="04:00:00" {{(@$data['init'] == '04:00:00')?'selected':'' }}>04:00 AM</option>
												<option value="04:30:00" {{(@$data['init'] == '04:30:00')?'selected':'' }}>04:30 AM</option>
												<option value="05:00:00" {{(@$data['init'] == '05:00:00')?'selected':'' }}>05:00 AM</option>
												<option value="05:30:00" {{(@$data['init'] == '05:30:00')?'selected':'' }}>05:30 AM</option>
												<option value="06:00:00" {{(@$data['init'] == '06:00:00')?'selected':'' }}>06:00 AM</option>
												<option value="06:30:00" {{(@$data['init'] == '06:30:00')?'selected':'' }}>06:30 AM</option>
												<option value="07:00:00" {{(@$data['init'] == '07:00:00')?'selected':'' }}>07:00 AM</option>
												<option value="07:30:00" {{(@$data['init'] == '07:30:00')?'selected':'' }}>07:30 AM</option>
												<option value="08:00:00" {{(@$data['init'] == '08:00:00')?'selected':''}}>08:00 AM</option>
												<option value="08:30:00" {{(@$data['init'] == '08:30:00')?'selected':''}}>08:30 AM</option>
												<option value="09:00:00" {{(@$data['init'] == '09:00:00')?'selected':''}}>09:00 AM</option>
												<option value="09:30:00" {{(@$data['init'] == '09:30:00')?'selected':''}}>09:30 AM</option>
												<option value="10:00:00" {{(@$data['init'] == '10:00:00')?'selected':''}}>10:00 AM</option>
												<option value="10:30:00" {{(@$data['init'] == '10:30:00')?'selected':''}}>10:30 AM</option>
												<option value="11:00:00" {{(@$data['init'] == '11:00:00')?'selected':''}}>11:00 AM</option>
												<option value="11:30:00" {{(@$data['init'] == '11:30:00')?'selected':''}}>11:30 AM</option>
												<option value="12:00:00" {{(@$data['init'] == '12:00:00')?'selected':''}}>12:00 AM</option>
												<option value="12:30:00" {{(@$data['init'] == '12:30:00')?'selected':''}}>12:30 AM</option>
												<option value="13:00:00" {{(@$data['init'] == '13:00:00')?'selected':''}}>01:00 PM</option>
												<option value="13:30:00" {{(@$data['init'] == '13:30:00')?'selected':''}}>01:30 PM</option>
												<option value="14:00:00" {{(@$data['init'] == '14:00:00')?'selected':''}}>02:00 PM</option>
												<option value="14:30:00" {{(@$data['init'] == '14:30:00')?'selected':''}}>02:30 PM</option>
												<option value="15:00:00" {{(@$data['init'] == '15:00:00')?'selected':''}}>03:00 PM</option>
												<option value="15:30:00" {{(@$data['init'] == '15:30:00')?'selected':''}}>03:30 PM</option>
												<option value="16:00:00" {{(@$data['init'] == '16:00:00')?'selected':''}}>04:00 PM</option>
												<option value="16:30:00" {{(@$data['init'] == '16:30:00')?'selected':''}}>04:30 PM</option>
												<option value="17:00:00" {{(@$data['init'] == '17:00:00')?'selected':''}}>05:00 PM</option>
												<option value="17:30:00" {{(@$data['init'] == '17:30:00')?'selected':''}}>05:30 PM</option>
												<option value="18:00:00" {{(@$data['init'] == '18:00:00')?'selected':''}}>06:00 PM</option>
												<option value="18:30:00" {{(@$data['init'] == '18:30:00')?'selected':''}}>06:30 PM</option>
												<option value="19:00:00" {{(@$data['init'] == '19:00:00')?'selected':''}}>07:00 PM</option>
												<option value="19:30:00" {{(@$data['init'] == '19:30:00')?'selected':''}}>07:30 PM</option>
												<option value="20:00:00" {{(@$data['init'] == '20:00:00')?'selected':''}}>08:00 PM</option>
												<option value="20:30:00" {{(@$data['init'] == '20:30:00')?'selected':''}}>08:30 PM</option>
												<option value="21:00:00" {{(@$data['init'] == '21:00:00')?'selected':''}}>09:00 PM</option>
												<option value="21:30:00" {{(@$data['init'] == '21:30:00')?'selected':''}}>09:30 PM</option>
												<option value="22:00:00" {{(@$data['init'] == '22:00:00')?'selected':''}}>10:00 PM</option>
												<option value="22:30:00" {{(@$data['init'] == '22:30:00')?'selected':''}}>10:30 PM</option>
												<option value="23:00:00" {{(@$data['init'] == '23:00:00')?'selected':''}}>11:00 PM</option>
												<option value="23:30:00" {{(@$data['init'] == '23:30:00')?'selected':''}}>11:30 PM</option>
												<option value="24:00:00" {{(@$data['init'] == '24:00:00')?'selected':''}}>12:00 PM</option>
												<option value="24:30:00" {{(@$data['init'] == '24:30:00')?'selected':''}}>12:30 PM</option>
											</select>
										</div>

										<div class="form-group">
											<label for="date">Fecha inicial</label>
											<input type="date" name="date" class="form-control" value="{{@$data['date']}}">
										</div>

										<div class="form-group">
											<label for="date">Fecha final</label>
											<input type="date" name="date_final" class="form-control" value="{{@$data['date_final']}}">
										</div>

										<button type="submit" class="btn btn-block btn-primary">Filtrar resultados</button>

									</form>

								</div>
							</div>
							<div class="col-xl-8">

								<div class="row">

									<div class="col-xl-12">
										<p>Búsqueda el día de <strong>{{$data['date']}}</strong> a <strong>@if($data['init'] == '*') cualquier hora @else las {{$data['init']}}@endif</strong>.</p>
									</div>

									@if($data['dates']->count() > 0)

										@foreach ($data['dates'] as $date)
											<div class="col-xl-4">

												<div class="item-teacher">
													<div class="date">
														{{Carbon\Carbon::parse($date->start)->isoFormat('MMMM Do YYYY, h:mm:ss a')}}
													</div>
													<div class="row">
														<div class="col-xl-4">
															<div class="img" style="background-image: url('{{route('get.image',$date->user->img)}}');"></div>
														</div>
														<div class="col-xl-8">
															<div class="info">
																<h3>{{$date->user->name}}</h3>
																<h5>{{$date->user->country}}</h5>
																<div class="stars">
																	<i class="fas fa-star"></i> 5.0
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xl-12">
															<p>{{$date->user->teacher->letter_presentation}}</p>
														</div>
													</div>
													<form action="{{route('teacher.schedule')}}" class="add-schedule" method="post">
														@csrf
														<input type="hidden" name="name" value="{{$date->user->name}}">
														<input type="hidden" name="user_id" value="{{auth()->user()->id}}">
														<input type="hidden" name="teacher_id" value="{{$date->user->id}}">
														<input type="hidden" name="schedule_id" value="{{$date->id}}">
														<button type="submit" class="btn btn-success btn-xs">Agendar</button>
													</form>
												</div>

											</div>
										@endforeach

									@else
										<div class="col-xl-12 text-center">
											<h3>No hay clases con tu selección.</h3>
										</div>
									@endif

								</div>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

</section>

@endsection

@push('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
@endpush
