@extends('student.layouts.admin')

@section('title')
	Maestros
@endsection

@section('subtitle')
@endsection

@push('css')
	<link href='https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.13.1/css/all.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link rel="stylesheet" href="{{asset('admin/assets/libs/owl/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/libs/owl/assets/owl.theme.default.min.css')}}">
@endpush

@section('content')


<section id="package" class="section-admin">

    <section class="header">
        <h2>Agendar clase</h2>
    </section>

	<div class="container-fluid">

		<div class="row">
			<div class="col-xl-12">

				@if (Session::has('success'))
				    <div class="alert alert-success">
				        {!! Session::get('success') !!}
				    </div>
				@endif

				<div class="card">
			        <div class="card-body">

			            <div class="row">
			            	<div class="col-xl-12">

								@if($data['teachers']->count() > 0)

									<div class="owl-carousel owl-theme owl-teachers">
										@foreach ($data['teachers'] as $teach)
											<div class="item">
												<a href="{{route('lesson.search',$teach->id)}}">
													<div class="teacher-item" style="background-image: url('{{route('get.image',$teach->img)}}');">
														<h3>{{$teach->name}} {{$teach->last_name}}</h3>
													</div>
												</a>
											</div>
										@endforeach
									</div>

								@endif

			            	</div>
			            	<div class="col-xl-12">

			            		<div id='calendar'></div>

			            	</div>
			            </div>

			        </div>
			    </div>
		    </div>
	    </div>
    </div>

</section>

@endsection

@push('js')
	<script src="https://momentjs.com/downloads/moment.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.all.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script src="{{asset('admin/assets/libs/owl/owl.carousel.min.js')}}"></script>
    <script>
		$(document).ready(function () {
			//Owl Carousel
			$('.owl-teachers').owlCarousel({
			    loop:false,
			    margin:20,
			    nav:false,
			    responsive:{
			        0:{
			            items:2
			        },
			        600:{
			            items:4
			        },
			        1000:{
			            items:6
			        }
			    }
			});

			var today = moment();
			today = today.format('YYYY-MM-DD');

		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });

		    var calendarEl = document.getElementById('calendar');
		    var calendar = new FullCalendar.Calendar(calendarEl, {
		    	selectAllow: function(select) {
				      return moment().diff(select.start) <= 0
				},
				eventOverlap: false,
                timeZone: '{{Auth()->user()->timezone->offset}}',
		    	locale: 'es',
		    	selectable: true,
				editable: false,
				droppable: false,
				initialView: 'timeGridWeek',
				initialDate: today,
				expandRows: false,
				allDaySlot: false,
				selectOverlap: false,
				dayHeaders: true,
				scrollTime: '00:00:00',
				slotDuration: '00:30:00',
				themeSystem: 'bootstrap',
				nowIndicator: true,
				displayEventTime: false,
				buttonText: {
			      dayGridMonth: 'Mes',
			      timeGridWeek: 'Semana',
			      timeGridDay: 'Día',
			      listWeek: 'Lista'
			    },
				slotLabelFormat: {
					hour: 'numeric',
					minute: '2-digit',
					omitZeroMinute: false,
					hour12: false
				},
				eventTimeFormat: {
					hour: 'numeric',
					minute: '2-digit',
					omitZeroMinute: false,
					hour12: false
				},
		        events: "{{route('student.schedule.get', $data['teacher']->id )}}",
		       	eventColor: '#378006',
		       	eventDisplay: 'block',
		        displayEventTime: true,
		        headerToolbar: {
		        	start: 'timeGridWeek',
		        	center:'title',
		        	end: 'prev,next',
		        },
		        select: function(arg) {
			    },
			    eventClick: function(arg) {

			    	Swal.fire({
					  title: '¿Deseas agendar esta clase?',
					  showDenyButton: false,
					  showCancelButton: true,
					  confirmButtonText: 'Agendar',
					  cancelButtonText: 'Cancelar',
					}).then((result) => {

					  if (result.isConfirmed) {

					  	//Add Schedule
					    $.ajax({
						   url: "{{route('teacher.schedule')}}",
						   data: {
						       schedule_id: arg.event.id,
						       user_id: {{auth()->user()->id}},
						       name: '{{ $data['teacher']->name }}',
						       teacher_id: {{$data['teacher']->id}}
						   },
						   type: "POST",
						   success: function (data) {

						   		if(data.status == 'success'){

						   			Swal.fire('Éxito', data.text , 'success');

						   			setTimeout(function(){
						   				location.reload();
						   			}, 1000);


						   		} else if(data.status == 'failed') {

						   			Swal.fire('Atención', data.text , 'warning');

						   			// setTimeout(function(){
						   			// 	location.reload();
						   			// }, 3000);

						   		}
						   },
						   error: function(data){

						   }
						});

					  }
					})
			    },

		    });

		    calendar.render();

		    function displayMessage(message) {
			    toastr.success(message, 'Apartado');
			}
		});
    </script>
@endpush
