@extends('student.layouts.admin')

@section('title')
    Favoritos
@endsection

@section('subtitle', 'Profesores')

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.min.css">
@endpush

@section('content')

    <div class="row">

        @foreach ($data['favorites'] as $favorite)

            @foreach ($favorite->teacher as $teacher)

                <div class="col-xl-6">
                    <div class="card teacher-item">
                        <div class="img" style="background-image: url('{{route('get.image',$teacher->img)}}');"></div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-9 offset-lg-3">
                                    <h2>{{$teacher->name}} {{$teacher->last_name}}</h2>
                                    <small class="subtitle">{{$teacher->teacher->career}}</small>
                                    <p>{{$teacher->teacher->letter_presentation}}</p>
                                    <a href="{{route('lessons.calendar_teacher',$teacher->id)}}" class="btn btn-success">Calendario</a>
                                    <a href="#reviews" data-fancybox="" class="btn btn-info">Ver opiniones</a>
                                     <form action="{{route('favorites.store')}}" method="post" class="favorite" style="display: inline-block;">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$favorite->id}}">
                                        <input type="hidden" name="type" value="remove_professor">
                                        <input type="hidden" name="teacher_id" value="{{$teacher->id}}">
                                        <button type="submit" class="btn btn-danger"><i class="fas fa-heart"></i> Quitar favorito</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

        @endforeach

    </div>
		
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.16.6/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
@endpush
