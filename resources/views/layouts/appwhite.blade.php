<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="shortcut icon" type="image/jpg" href="{{asset('assets/img/favicon.png')}}"/>
    @stack('css')
    <style>
        @font-face {
            font-family: 'ProgramOT';
            src: url('{{asset('fonts/ProgramOT-Bold.woff2')}}') format('woff2'),
                url('{{asset('fonts/ProgramOT-Bold.woff')}}') format('woff'),
                url('{{asset('fonts/ProgramOT-Bold.ttf')}}') format('woff');
            font-weight: bold;
            font-style: normal;
            font-display:swap;
        }
    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {!! SEO::generate() !!}
</head>
<body>

    @yield('content')

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="{{asset('script/webslidemenu/webslidemenu.js')}}"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/front.js') }}" defer></script>
    @stack('js')

</body>
</html>
