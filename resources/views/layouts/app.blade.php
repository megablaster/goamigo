<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="keywords" content="{{@$data['post']->keywords}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="shortcut icon" type="image/jpg" href="{{asset('assets/img/favicon.png')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset("vendor/cookie-consent/css/cookie-consent.css")}}">
    <style>
        @font-face {
            font-family: 'ProgramOT';
            src: url('{{asset('fonts/ProgramOT-Bold.woff2')}}') format('woff2'),
                url('{{asset('fonts/ProgramOT-Bold.woff')}}') format('woff'),
                url('{{asset('fonts/ProgramOT-Bold.ttf')}}') format('woff');
            font-weight: bold;
            font-style: normal;
            font-display:swap;
        }
    </style>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('css')
    <!-- Hotjar Tracking Code for GoAmigoGo | FrontEnd -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:2817843,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1336804060050691');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1336804060050691&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta name="google-site-verification" content="DuuOgMJVNV0aCFLJ8uTSh2upEyfdx1PVWYlyJnoLtso" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-MLJ8F0660R"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'G-MLJ8F0660R');
    </script>
    {!! htmlScriptTagJsApi() !!}
    {!! SEO::generate() !!}
</head>
<body>
    @yield('content')
    @include('front.parts.footer')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('js/front.js') }}" defer></script>
    @stack('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script src="{{asset('script/password/jquery.passwordRequirements.min.js')}}"></script>
    <script>
        $(".pr-password").passwordRequirements({
            numCharacters: 6,
            useLowercase:true,
            useUppercase:true,
            useNumbers:true,
            useSpecial:true
        });
    </script>
</body>
</html>
