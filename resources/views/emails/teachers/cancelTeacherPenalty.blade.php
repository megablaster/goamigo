@component('mail::message')

<p>Hola, <strong>{{$name}}</strong>, haz cancelado la clase del día: <strong>{{$schedule->schedule->start}}</strong>, del alumno <strong>{{$student->name}} {{$student->last_name}}</strong>, se le ha reintegrado un crédito por dicha clase.</strong></p>

<p>La solicitud genera un strike, recuerda que solo puedes tener 4 strike cada 6 meses.</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
