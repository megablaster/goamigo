@component('mail::message')

<p>Hola, <strong>{{$student->name}} {{$student->last_name}}</strong>, el profesor <strong>{{$teacher->name}} {{$teacher->last_name}}</strong>, ha cancelado la clase del día: <strong>{{$schedule->schedule->start}}</strong>, se te ha reintegrado un crédito por dicha clase.</strong></p>

<p>Lamentamos los inconvenientes que haya causado y esperamos verte de nuevo agendando una clase pronto.</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
