@component('mail::message')

<p>Hola, <strong>{{$user->name}} {{$user->last_name}}</strong> te damos la bienvenida al proceso de selección de <strong>Go Amigo</strong> te invitamos a validar tu cuenta, revisa tu bandeja de entrada y/o tu carpeta de SPAM:</p>

<p>A continuación te mostramos los datos de acceso:</p>

<p><strong>Nombre:</strong> {{$user->email}}<br><strong>Contraseña:</strong> {{$user->password}}</p>

@component('mail::button', ['url' => env('APP_URL').'/go-admin'])
Iniciar sesión
@endcomponent

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
