@component('mail::message')

<h1>¡Gracias por formar parte de GoAmigo!</h1>
<p>Hola, <strong>{{$user->name}} {{$user->last_name}}</strong> te damos la bienvenida a la plataforma <strong>Go Amigo</strong>. A partir de hoy podras recibir solicitudes de alumnos para impartir clases.</p>
<p>En el panel encontraras todas las herramientas para comenzar a interacturar con los alumnos, ver tus clases próximas, ingresos estimados y tu calendario de disponiblidad de horas.</p>

@component('mail::button', ['url' => env('APP_URL').'/go-admin'])
Iniciar sesión
@endcomponent

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
