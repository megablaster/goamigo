@component('mail::message')

<p>Hola, <strong>{{$name}}</strong>, haz solicitado una cancelación para la clase del día: <strong>{{$schedule->schedule->start}}</strong>, del alumno: {{$student->name}} {{$student->last_name}}</p>

<p>La solicitud genera un strike, recuerda que solo puedes tener 4 strike cada 6 meses.</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
