@component('mail::message')

<p>Hola, <strong>Administrador</strong> se ha registrado un nuevo estudiante te compartimos los datos del mismo:</p>

<p>Datos del usuario:</p>
<p>
<strong>Nombre:</strong> {{$user->name}} {{$user->last_name}}<br>
<strong>Correo:</strong> {{$user->email}}<br>
<strong>Género:</strong> {{$user->gender}}<br>
<strong>Teléfono:</strong> {{$user->phone}}<br>
</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
