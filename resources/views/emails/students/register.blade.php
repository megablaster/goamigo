@component('mail::message')

<p>Hola, <strong>{{$user->name}} {{$user->last_name}}</strong> te damos la bienvenida a la plataforma <strong>Go Amigo</strong> te invitamos a utilizar tu clase muestra para que conozcas nuestro servicio:</p>

@component('mail::button', ['url' => env('APP_URL').'/go-admin'])
Iniciar sesión
@endcomponent

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
