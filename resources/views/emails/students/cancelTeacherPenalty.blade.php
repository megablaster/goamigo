@component('mail::message')

<p>Hola, <strong>{{$name}}</strong>, se ha recibido una cancelación para la clase del día: <strong>{{$schedule->schedule->start}}</strong>, del alumno: {{$student->name}} {{$student->last_name}} el alumno ya no contaba con cancelaciones gratuitas y se te abonara la mitad del costo de la clase.</p>

<p>Esperemos te encuentres muy bien y gracisa por elegirnos.</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
