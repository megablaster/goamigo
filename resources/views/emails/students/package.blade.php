@component('mail::message')

<p>Hola, <strong>{{$user->name}} {{$user->last_name}}</strong>, se ha recibido una compra de créditos: <strong>{{$product->name}}</strong> x ${{number_format($product->price,2)}} USD.</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
