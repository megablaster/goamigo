@component('mail::message')

<p>Hola, <strong>{{$student->name}} {{$student->last_name}}</strong>, se ha recibido una cancelación para la clase del día: <strong>{{$schedule->schedule->start}}</strong>, con el profesor: <strong>{{$teacher->name}} {{$teacher->last_name}}, lamentamos informarte que se tomara como clase tomada ya que no cuentas con cancelaciones gratuitas.</strong></p>

<p>Lamentamos los inconvenientes que haya causado y esperamos verte de nuevo agendando una clase pronto.</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
