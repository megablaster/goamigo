@component('mail::message')

<p>Hola, <strong>{{$user->name}} {{$user->last_name}}</strong> te damos la bienvenida a la plataforma <strong>Go Amigo</strong> recibiras noticias de nosotros pronto, se te ha añadido un cupón que podras utilizar cuando se habilite la plataforma.</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
