@component('mail::message')

<p>Hola, <strong>{{$name}}</strong>, se ha recibido una cancelación para la clase del día: <strong>{{$schedule->schedule->start}}</strong>, del alumno: {{$student->name}} {{$student->last_name}}</p>

<p>Nota.- La solicitud no genera penalización ya que se realiza antes de 24 horas.</p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
