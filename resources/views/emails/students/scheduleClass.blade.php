@component('mail::message')

<p>Hola, <strong>{{$user->name}} {{$user->last_name}}</strong> se ha agendado una clase para el día <strong></strong>.</p>

@component('mail::button', ['url' => env('APP_URL').'/go-admin/students'])
Iniciar sesión
@endcomponent

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
