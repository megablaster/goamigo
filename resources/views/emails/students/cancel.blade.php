@component('mail::message')

<p>Hola, <strong>{{$student->name}} {{$student->last_name}}</strong>, se ha recibido una cancelación para la clase del día: <strong>{{$schedule->schedule->start}}</strong>, con el profesor: <strong>{{$teacher->name}} {{$teacher->last_name}}</strong></p>

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
