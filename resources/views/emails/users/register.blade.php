@component('mail::message')

<p>Hola, <strong>{{$user->name}} {{$user->last_name}}</strong> se ha registrado una cuenta, a continuación estan los datos de acceso:</p>

<p><strong>Name:</strong> {{$user->email}}</p>
<p><strong>Password:</strong> {{$user->password}}</p>

@component('mail::button', ['url' => env('APP_URL').'/go-admin'])
Iniciar sesión
@endcomponent

Atentamente el equipo de, {{ config('app.name') }}
@endcomponent
