<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'all' => 'Todos los niveles',
    'title' => 'Niveles',
    'add' => 'Agregar nivel',
    'table' => [
        'name' => 'Nombre',
        'description' => 'Descripción',
        'lesson' => 'Lecciones',
        'action' => 'Acciones',
    ],
    'create' => [
        'title' => 'Agregar nivel',
        'subtitle' => 'Información',
        'name' => 'Nombre',
        'description' => 'Descripción',
        'lesson' => 'No. de lecciones',
        'cancel' => 'Cancelar',
        'add' => 'Agregar nivel'
    ],

];
