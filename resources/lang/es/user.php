<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'all' => 'Todos los usuarios',
    'title' => 'Usuarios',
    'add' => 'Agregar usuario',
    'table' => [
        'name' => 'Nombre',
        'email' => 'Correo',
        'phone' => 'Teléfono',
        'country' => 'País',
        'role' => 'Rol',
        'date' => 'Fecha',
        'action' => 'Acciones',
    ],
    'edit' => [
        'title' => 'Editar usuario',
        'subtitle' => 'Información personal',
        'name' => 'Nombre',
        'last_name' => 'Apellido',
        'email' => 'Correo',
        'gender' => 'Genero',
        'phone' => 'Teléfono',
        'city' => 'Ciudad',
        'country' => 'País',
        'role' => 'Rol',
        'language' => 'Idioma',
        'image' => 'Imagen',
        'change_password' => 'Cambiar contraseña',
        'password' => 'Contraseña',
        'repeat_password' => 'Repetir contraseña',
        'exit' => 'Cancelar y regresar',
        'save' => 'Guardar cambios'
    ],
    'create' => [
        'title' => 'Agregar usuario',
        'subtitle' => 'Información personal',
        'name' => 'Nombre',
        'last_name' => 'Apellido',
        'email' => 'Correo',
        'gender' => 'Genero',
        'phone' => 'Teléfono',
        'city' => 'Ciudad',
        'country' => 'País',
        'role' => 'Rol',
        'password_title' => 'Agregar contraseña',
        'password' => 'Contraseña',
        'password_repeat' => 'Repetir contraseña',
        'add' => 'Agregar usuario'
    ]
];
