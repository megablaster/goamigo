<?php

return [

    'statistics' => 'Estadísticas',
    'administration' => 'Administración',
    'users' => 'Usuarios',
    'testimonials' => 'Testimonios',
    'sliders' => 'Rotadores',
    'accounting' => 'Contabilidad',
    'educational' => 'Plan de estudios',
    'levels' => 'Plan de estudios',
    'material' => 'Material de estudio',
    'classroom' => ' Salón de clase',
    'teacher' => [
    	'teachers' => 'Maestros'
    ],
    'account' => [
    	'account'=> 'Mi cuenta',
    	'logout' => 'Cerrar sesión'
    ],
    'payment' => [
        'payment' => 'Productos',
        'title' => 'Paquetes'
    ]

];
