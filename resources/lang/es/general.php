<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'view' => 'Ver',
    'edit' => 'Editar',
    'delete' => 'Eliminar',
    'cancel' => 'Cancelar',
    'hello' => 'Hola',
    'gender' => [
        'select' => 'Selecciona tu género',
        'female' => 'Femenino',
        'male' => 'Masculino',
        'undefined' => 'Otro',
    ],
    'role' => [
        'admin' => 'Administrador',
        'manager' => 'Gerente',
        'teacher' => 'Profesor',
        'student' => 'Estudiante',
    ],
    'submenu' => [
        'profile' => 'Mi cuenta',
        'income' => 'Mis ingresos',
        'setting' => 'Configuración',
        'logout' => 'Cerrar sesión'
    ],
    'roles' => [
        'admin' => '<span class="badge badge-success">Administrador</span>',
        'manager' => '<span class="badge badge-info">Gerente</span>',
        'teacher' => '<span class="badge badge-warning">Profesor</span>',
        'student' => '<span class="badge badge-primary">Estudiante</span>'
    ]
];
