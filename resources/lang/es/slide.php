<?php

return [

    'title' => 'Rotadores',
    'subtitle' => 'Todos los rotadores',
    'table' => [
    	'name' => 'Título',
        'description' => 'Descripción',
        'date' => 'Fecha',
        'action' => 'Acciones'
    ],
    'add' => 'Agregar rotador',
    'create' => [
        'title' => 'Agregar rotador',
        'subtitle' => 'Información',
        'name' => 'Nombre',
        'description' => 'Descripción',
        'button' => 'Texto de botón',
        'image' => 'Imagen',
        'cancel' => 'Cancelar',
        'add' => 'Agregar rotador'
    ]
    
];