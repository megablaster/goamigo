<?php

return [

    'title' => 'Testimonios',
    'subtitle' => 'Todos los testimonios',
    'table' => [
    	'name' => 'Nombre',
        'profession' => 'Profesión',
        'date' => 'Fecha',
        'action' => 'Acciones'
    ],
    'add' => 'Agregar testimonios',
    'create' => [
        'title' => 'Agregar testimonio',
        'subtitle' => 'Información',
        'name' => 'Nombre',
        'description' => 'Descripción',
        'profession' => 'Profesión',
        'photography' => 'Fotografía',
        'cancel' => 'Cancelar',
        'add' => 'Agregar testimonio'
    ]
    
];