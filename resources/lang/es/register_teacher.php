<?php

return [
	'title' => 'Llena la solicitud para formar parte de <strong>Go Amigo</strong>.',
	'subtitle' => 'Rellena la siguiente información',
	'name' => 'Nombre',
	'last_name' => 'Apellido',
	'phone' => 'Teléfono',
	'gender' => 'Genero',
	'email' => 'Correo electrónico',
	'city' => 'Ciudad',
	'country' => 'País',
	'title_password' => 'Contraseña',
	'password' => ' Contraseña',
	'repeat_password' => ' Repetir contraseña',
	'button' => 'Registrar cuenta',
	'submit' => 'Enviar',
	'experience' => '¿Cuentanos porqué te gustaría trabajar con nosotros?',
	'privacy' => 'Es necesario que aceptes el',
	'text_privacy' => 'Aviso de privacidad.'
];