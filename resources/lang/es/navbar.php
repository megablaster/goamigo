<?php

return [

    'menu' => [
        'home' => 'Inicio',
        'about' => 'Nosotros',
        'philosophy' => 'Filosofía',
        'teachers' => 'Maestros',
        'pricing' => 'Precios',
        'hiring' => 'Contratación',
        'privacy' => 'Aviso de privacidad',
        'faqs' => 'Preguntas frecuentes',
        'blog' => 'Blog',
        'search' => 'Buscar...',
        'button' => 'Registrarse',
        'account' => 'Mi cuenta',
        'login' => 'Iniciar sesión'
    ],

];
