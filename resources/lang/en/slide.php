<?php

return [

    'title' => 'Sliders',
    'subtitle' => 'All sliders',
    'table' => [
    	'name' => 'Title',
        'description' => 'Description',
        'date' => 'Date',
        'action' => 'Actions'
    ],
    'add' => 'Add slider',
    'create' => [
        'title' => 'Add slider',
        'subtitle' => 'Information',
        'name' => 'Name',
        'description' => 'Descriptión',
        'button' => 'Text button',
        'image' => 'Image',
        'cancel' => 'Cancel',
        'add' => 'Add slider'
    ]
    
];