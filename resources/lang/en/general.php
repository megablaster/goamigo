<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'view' => 'View',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'cancel' => 'Cancel',
    'hello' => 'Hello',
    'gender' => [
        'select' => 'Select your gender',
        'female' => 'Female',
        'male' => 'Male',
        'undefined' => 'Other',
    ],
    'role' => [
        'admin' => 'Administrator',
        'manager' => 'Manager',
        'teacher' => 'Teacher',
        'student' => 'Student',
    ],
    'submenu' => [
        'profile' => 'My Account',
        'income' => 'My income',
        'setting' => 'Settings',
        'logout' => 'Log out'
    ],
    'roles' => [
        'admin' => '<span class="badge badge-success">Administrator</span>',
        'manager' => '<span class="badge badge-info">Manager</span>',
        'teacher' => '<span class="badge badge-warning">Teacher</span>',
        'student' => '<span class="badge badge-primary">Student</span>'
    ]

];
