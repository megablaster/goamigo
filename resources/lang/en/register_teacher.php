<?php

return [
	'title' => 'Fill out the application to be part of <strong>Go Amigo</strong>.',
	'subtitle' => 'Fill in the following information',
	'name' => 'Name',
	'last_name' => 'Last name',
	'phone' => 'Phone',
	'gender' => 'Gender',
	'email' => 'Email',
	'city' => 'City',
	'country' => 'Country',
	'title_password' => 'Password',
	'password' => ' Password',
	'repeat_password' => ' Repeat password',
	'button' => 'Register account',
	'submit' => 'Submit',
	'experience' => 'Tell us why you would like to work with us?',
	'privacy' => 'You need to accept the',
	'text_privacy' => 'Privacy Notice'
];