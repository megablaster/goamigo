<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'all' => 'All Users',
    'title' => 'Users',
    'add' => 'Add User',
    'table' => [
        'name' => 'Name',
        'email' => 'Email',
        'phone' => 'Phone',
        'country' => 'Country',
        'role' => 'Role',
        'date' => 'Date',
        'action' => 'Actions',
    ],
    'edit' => [
        'title' => 'Edit user',
        'subtitle' => 'Personal information',
        'name' => 'Name',
        'last_name' => 'Last name',
        'email' => 'Email',
        'gender' => 'Gender',
        'phone' => 'Phone',
        'city' => 'City',
        'country' => 'Country',
        'role' => 'Role',
        'language' => 'Language',
        'image' => 'Image',
        'change_password' => 'Change password',
        'password' => 'Password',
        'repeat_password' => 'Repeat password',
        'exit' => 'Cancel',
        'save' => 'Save changes'
    ],
    'create' => [
        'title' => 'Add user',
        'subtitle' => 'Personal information',
        'name' => 'Name',
        'last_name' => 'Last name',
        'email' => 'Email',
        'gender' => 'Gender',
        'phone' => 'Phone',
        'city' => 'City',
        'country' => 'Country',
        'role' => 'Role',
        'password_title' => 'Assign password',
        'password' => 'Password',
        'password_repeat' => 'Repeat password',
        'add' => 'Add user'
    ]

];
