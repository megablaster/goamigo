<?php

return [
    'history' => 'Sales History',
    'statistics' => 'Statistics',
    'administration' => 'Administration',
    'users' => 'Users',
    'testimonials' => 'Testimonials',
    'sliders' => 'Sliders',
    'educational' => 'Educational plan',
    'levels' => 'Levels',
    'teacher' => [
    	'teachers' => 'Teachers'
    ],
    'account' => [
    	'account'=> 'Account',
    	'logout' => 'Log out'
    ],
    'payment' => [
        'payment' => 'Payment',
        'title' => 'Package'
    ],
    'credits' => [
        'title' => 'Credits',
    ],

];
