<?php

return [

    'title' => 'Not just another language, <span>but a gateway to another culture</span>',
    'step1' => 'Native Latin teachers',
    'step2' => 'Cultural topics',
    'step3' => 'Easy to use',
    'step4' => 'Observable results',

];
