<?php

return [

    'title' => 'Testimonials',
    'subtitle' => 'All testimonials',
    'table' => [
    	'name' => 'Name',
        'profession' => 'Profession',
        'date' => 'Date',
        'action' => 'Actions'
    ],
    'add' => 'Add testimonial',
    'create' => [
        'title' => 'Add testimonial',
        'subtitle' => 'Information',
        'name' => 'Name',
        'description' => 'Description',
        'profession' => 'Profession',
        'photography' => 'Photography',
        'cancel' => 'Cancel',
        'add' => 'Add testimonial'
    ]
    
];
