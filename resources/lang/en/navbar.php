<?php

return [

    'menu' => [
        'home' => 'Home',
        'about' => 'About',
        'philosophy' => 'Philosophy',
        'teachers' => 'Teachers',
        'pricing' => 'Pricing',
        'hiring' => 'Hiring',
        'privacy' => 'Notice of Privacy',
        'terms' => 'Terms and Conditions',
        'faqs' => 'Faqs',
        'blog' => 'Blog',
        'search' => 'Search...',
        'button' => 'Sign In',
        'account' => 'My account',
        'login' => 'Log in'
    ],

];
