<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'type',
        'description',
        'percentage',
        'user_id',
        'type_app',
        'type_coupon',
        'amount_coupon',
        'amount',
        'date_coupon',
        'percentage_coupon'
    ];
}
