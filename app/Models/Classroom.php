<?php

namespace App\Models;
use App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ScheduleTeacher;

class Classroom extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'teacher_id',
        'schedule_id',
        'url',
        'id_vedamo',
    ];

    //Relationship
    public function teacher()
    {
    	return $this->hasOne(User::class,'id','teacher_id');
    }

    public function schedule()
    {
        return $this->hasOne(ScheduleTeacher::class,'id','schedule_id');
    }

    public function student()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function review()
    {
        return $this->hasOne(Review::class,'classroom_id','id');
    }
}
