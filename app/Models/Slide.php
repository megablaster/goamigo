<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Carbon\Carbon;

class Slide extends Model
{
    use HasFactory, HasTranslations;

    protected $guarded = [];

    //Translatable
    public $translatable = ['title','description','button'];

    public function getDate()
    {
    	$date = Carbon::parse($this->created_at);
    	return $date->format('d-m-Y');
    }
}
