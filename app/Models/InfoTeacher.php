<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Penalty;

class InfoTeacher extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getCareerAttribute()
    {
    	$work = $this->level_education;

    	switch ($work) {
    		case 'tecnico_superior':
    		    return 'Técnico superior';
    			break;
    		case 'licenciatura':
    		    return 'Licenciatura';
    			break;
    		default:
    			return 'Posgrado';
    			break;
    	}
    }

    public function getStrikesAttribute()
    {
       $dates = [];
       $dateContract = Carbon::parse($this->contract_date)->year(now()->format('Y'))->format('Y-m-d');

       $first_init = Carbon::parse($dateContract);
       $first_final = Carbon::parse($dateContract)->addDay(180);
       $second_init = Carbon::parse($first_final)->addDay(1);
       $second_final = Carbon::parse($second_init)->addDay(180);

       //Get strikes last 6 month
       $strikes = Penalty::where('user_id', auth()->user()->id)->get();

       $dates = [
            'date_contract' => $dateContract,
            'first_init' => $first_init,
            'first_final' => $first_final,
            'second_init' => $second_init,
            'second_final' => $second_final,
            'strikes_month' => 5,
       ];

       return $dates;
    }
}
