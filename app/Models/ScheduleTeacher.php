<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Penalty;
use App\Models\User;

class ScheduleTeacher extends Model
{
    use HasFactory;

    protected $fillable = [
    	'title','user_id','start','end','hour','days'
    ];

    public function cancelteacher()
    {
        $init = Carbon::parse(Carbon::now());
        $finish = Carbon::parse($this->start);

        $days = $finish->diffInHours($init);

        //Penalty
        $penalty = Penalty::where('user_id', auth()->user()->id)->first();
        $type_user = $penalty->type;
        $actually = $penalty->actually;
        $restant = $penalty->maximum - $actually;

        if ($actually < 5 && $days >= 24) {

            return '<a href="#" class="btn btn-danger btn-sm cancelMore24"
                    data-text="Esta a punto de solicitar una cancelación, cuentas con '.$restant.' cancelaciones cada 6 meses."
                    data-id="'.$this->id.'"
                    >Cancelar</a>';

        } else if($days < 24 || $actually >= 5) {

            if ($actually >= 5) {

                return '<a href="#" class="btn btn-danger btn-sm cancelMin24" data-text="Esta a punto de solicitar una cancelación, ya no cuentas con cancelaciones." data-id="'.$this->id.'">Cancelar</a>';

            } else {

                return '<a href="#" class="btn btn-danger btn-sm cancelMin24" data-text="Esta a punto de solicitar una cancelación, cuentas con '.$restant.' cancelaciones cada 6 meses. ¿Estas seguro de cancelar?" data-id="'.$this->id.'">Cancelar</a>';
            }
        }
    }

    public function getTimeAttribute (){
    	return Carbon::parse($this->start)->toTimeString();
	}

	public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function points(){
        
    }
}
