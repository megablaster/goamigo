<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class System extends Model
{
    use HasFactory, HasTranslations;

	protected $guarded = [];

	 //Translatable
	 public $translatable = ['privacy'];

    protected $fillable = [
		'name',
		'logo',
		'facebook',
		'youtube',
		'instagram',
		'emails',
		'copyright',
		'paymentfull',
		'paymenthalf',
        'type_app',
        'type_coupon',
        'percentage_coupon',
        'amount_coupon',
        'date_coupon',
		'privacy',
		'terms',
		'faqs'
    ];
}
