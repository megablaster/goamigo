<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\Post_gallery;
use App\Models\User;

class Post extends Model
{
    use HasFactory;

    public function getDate()
    {
        $date = Carbon::parse($this->created_at);
        return $date->format('d-m-Y');
    }

    //Accesors
    public function getExtractAttribute()
    {
        $extract = strip_tags($this->description);
        return substr($extract,0,300).'...';
    }

    //Relationship
    public function category()
    {
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function author()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function gallery()
    {
        return $this->hasMany(Post_gallery::class,'post_id','id');
    }
}
