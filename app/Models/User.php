<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;
use App\Models\InfoStudent;
use App\Models\InfoTeacher;
use App\Models\Invoice;
use App\Models\Credit;
use App\Models\Penalty;
use App\Models\Timezone;
use App\Models\Point;
use Carbon\Carbon;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory;
    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    use Billable;

    const ADMIN = 'admin';
    const MANAGER = 'manager';
    const TEACHER = 'teacher';
    const STUDENT = 'student';

    const ENGLISH = 'en';
    const SPANISH = 'es';

    protected $fillable = [
        'name',
        'last_name',
        'gender',
        'phone',
        'email',
        'city',
        'country',
        'email',
        'password',
        'timezone_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Relaciones
    public function penalty()
    {
        return $this->hasOne(Penalty::class,'user_id','id');
    }

    public function student()
    {
        return $this->hasOne(InfoStudent::class,'user_id','id');
    }

    public function teacher()
    {
        return $this->hasOne(InfoTeacher::class,'user_id','id');
    }

    public function credit()
    {
        return $this->hasOne(Credit::class,'user_id','id');
    }

    public function schedule()
    {
        return $this->hasMany(ScheduleTeacher::class,'user_id','id');
    }

    public function timezone()
    {
        return $this->hasOne(Timezone::class,'id', 'timezone_id');
    }

    public function scheduleWeek()
    {
        $now = Carbon::now();

        return $this->hasMany(ScheduleTeacher::class,'user_id','id')
                    ->where('start',$now->addDay())
                    ->where('end', $now->endOfWeek());
    }


    public function invoices()
    {
        return $this->hasMany(Invoice::class,'student_id','id');
    }

    //Scope
    public function daysExpiration()
    {
        $dateInit = Carbon::parse(Carbon::now()->format('d-m-Y'));
        $dateFin = Carbon::parse($this->credit->expiration)->format('d-m-Y');

        //Resultado
        $result = $dateInit->diffInDays($dateFin,false);

        if ($result < 1) {
            return '<span class="info-days">Sin vigencia</span>';
        } else {
            if ($result == 1) {
                return '<span class="info-days">Resta '.$result.' día</span>';
            } else {
                return '<span class="info-days">Restan '.$result.' días</span>';
            }
        }
    }

    //Obtenemos parametros
    public function getRole()
    {
        if ($this->hasRole('admin')) {
            return __("general.roles.admin");
        } else if($this->hasRole('manager')) {
            return __("general.roles.manager");
        } else if($this->hasRole('teacher')) {
            return __("general.roles.teacher");
        } else if($this->hasRole('student')) {
            return __("general.roles.student");
        }
    }

    public function getStep()
    {
        if ($this->suspend == true) {
            return '<span class="badge badge-dark">Suspendido</span>';
        }

        if ($this->hasRole('teacher')) {

            if ($this->teacher->step == 'first') {
                return '<span class="badge badge-warning">Prospecto</span>';
            } elseif($this->teacher->step == 'second'){
                return '<span class="badge badge-info">Prueba</span>';
            } else {
                return '<span class="badge badge-success">Contratado</span>';
            }

        } elseif($this->hasRole('student')) {

            if ($this->credit->trial == true) {
                return '<span class="badge badge-warning">Sin activar</span>';
            } else {
                return '<span class="badge badge-success">Activo</span>';
            }
            
        } else {
            return '<span class="badge badge-success">Administrador</span>';

        }

    }

    public function getDate()
    {
        $date = Carbon::parse($this->created_at);
        return $date->format('d-m-Y');
    }
}
