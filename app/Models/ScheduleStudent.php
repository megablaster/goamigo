<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ScheduleTeacher;
use App\Models\User;
use App\Models\Classroom;
use Carbon\Carbon;

class ScheduleStudent extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'teacher_id',
        'schedule_id',
        'name',
        'zoom_id',
        'comments'
    ];

    //Mutators
    public function getEstatusAttribute()
    {
        switch ($this->status) {
            case 'active':
                $status = '<span class="badge badge-success">Activo</span>';
                break;
            case 'cancel':
                $status = '<span class="badge badge-danger">Cancelado</span>';
                break;
            default:
                $status = '<span class="badge badge-warning">Reagendado</span>';
                break;
        }

        return $status;
    }

    public function cancel()
    {
        $init = Carbon::parse(Carbon::now());
        $finish = Carbon::parse($this->schedule->start);

        $days = $finish->diffInHours($init);

        //Penalty
        /*$penalty = Penalty::where('user_id', auth()->user()->id)->first();
        $type_user = $penalty->type;
        $actually = $penalty->actually;
        $restant = $penalty->maximum - $actually;

        if ($actually < 2 && $days >= 24) {

            return '<a href="#" class="btn btn-danger btn-sm cancelMore24"
                    data-text="Esta a punto de solicitar una cancelación, cuentas con '.$restant.' cancelaciones gratuitas."
                    data-id="'.$this->id.'"
                    >Cancelar</a>';

        } else if($days < 24 || $actually >= 2) {

            return '<a href="#" class="btn btn-danger btn-sm cancelMin24" data-text="No puedes cancelar gratuitamente, se te cobrará la clase. ¿Estas seguro de cancelar?" data-id="'.$this->id.'">Cancelar</a>';

        }*/

    }

    public function getDays()
    {
        $fechaEmision = Carbon::parse(Carbon::now());
        $fechaExpiracion = Carbon::parse($this->schedule->start);
        $diasDiferencia = $fechaExpiracion->diffInDays($fechaEmision);

        return $diasDiferencia;
    }

    //Relationship
    public function schedule()
    {
    	return $this->hasOne(ScheduleTeacher::class, 'id','schedule_id');
    }

    public function teacher()
    {
    	return $this->hasOne(User::class, 'id','teacher_id');
    }

    public function classroom()
    {
        return $this->hasOne(Classroom::class,'schedule_id','schedule_id');
    }

    public function student()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
}
