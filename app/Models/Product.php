<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use App\Models\System;

class Product extends Model
{
    use HasFactory, HasTranslations;

    protected $guarded = [];

    //Translatable
    public $translatable = ['name','description'];


    public function getGainAttribute()
    {
    	$system = System::find(1);

    	$paymentProfesor = $this->credits * $system->paymentfull;
    	$total = $this->price - $paymentProfesor;
    	
    	return '<span class="badge badge-success">&uarr;'.$total.' USD</span>';

    }
}
