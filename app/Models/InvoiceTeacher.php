<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ScheduleTeacher;
use App\Models\System;

class InvoiceTeacher extends Model
{
    use HasFactory;

    public function student()
    {
        return $this->hasOne(User::class,'id','student_id');
    }

    public function getEstatusAttribute()
    {
        if ($this->status == 'purchase') {
            return '<span class="badge badge-success">Pagado</span>';
        } elseif ($this->status == 'cancel') {
            return '<span class="badge badge-danger">Cancelado</span>';
        } elseif($this->status == 'refund') {
            return '<span class="badge badge-info">Reembolso</span>';
        } elseif($this->status == 'payhalf'){
            return '<span class="badge badge-primary">Pago media clase</span>';
        } else {
            return '<span class="badge badge-info">Cancelado</span>';
        }
    }

    public function getIncomeAttribute()
    {
        $system = System::find(1);

        if ($this->status == 'paid') {

            return '<span class="badge badge-success">'.$system->paymentfull.' USD</span>';

        } else if($this->status == 'payhalf') {

            return '<span class="badge badge-warning">'.$system->paymenthalf.' USD</span>';

        } else if($this->status == 'trial') {

            return '<span class="badge badge-warning">'.$system->paymentfull.' USD</span>';            

        }
    }

    public function getIncomeAdminAttribute()
    {
        $system = System::find(1);

        if ($this->status == 'paid') {

            return '<span class="badge badge-success">'.$system->paymentfull.' USD</span>';

        } else if($this->status == 'payhalf') {

            return '<span class="badge badge-warning">'.$system->paymenthalft.' USD</span>';

        } else if($this->status == 'trial') {

            return '<span class="badge badge-warning">'.$system->paymentfull.' USD</span>';            

        }
    }

    public function schedule()
    {
        return $this->hasOne(ScheduleTeacher::class,'id','schedule_id');
    }

    public function teacher()
    {
        return $this->hasOne(User::class,'id','teacher_id');
    }
}
