<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Testimonial extends Model
{
    use HasFactory,HasTranslations;

    protected $guarded = [];

    public function getDate()
    {
    	$date = Carbon::parse($this->created_at);
    	return $date->format('d-m-Y');
    }
}
