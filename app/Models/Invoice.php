<?php

namespace App\Models;

use App\Models\User;
use App\Models\ScheduleTeacher;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
		'student_id',
		'teacher_id',
		'schedule_id',
		'status',
		'credit',
		'description'
    ];

    public function getEstatusAttribute()
    {
        if ($this->status == 'purchase') {
            return '<span class="badge badge-success">Pagado</span>';
        } elseif ($this->status == 'cancel') {
            return '<span class="badge badge-danger">Cancelado</span>';
        } elseif($this->status == 'refund') {
            return '<span class="badge badge-info">Reembolso</span>';
        } else {
            return '<span class="badge badge-primary">Prueba</span>';
        }
    }

    public function teacher()
    {
        return $this->hasOne(User::class,'id','teacher_id');
    }

    public function schedule()
    {
        return $this->hasOne(ScheduleTeacher::class,'id','schedule_id');
    }
}
