<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Favorite extends Model
{
    use HasFactory;

    protected $fillable = [
		'user_id',
		'teacher_id'
    ];

    //Relationship
    public function teacher()
    {
    	return $this->hasMany(User::class,'id','teacher_id');
    }
}
