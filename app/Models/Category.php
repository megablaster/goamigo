<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
use Carbon\Carbon;


class Category extends Model
{
    use HasFactory;

    public function getDate()
    {
        $date = Carbon::parse($this->created_at);
        return $date->format('d-m-Y');
    }

    //Relationship
    public function posts()
    {
        return $this->hasMany(Post::class,'category_id','id');
    }
}
