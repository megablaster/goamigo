<?php

namespace App\Mail\Teacher;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class StudentCancelPenalty extends Mailable
{
    use Queueable, SerializesModels;

    public $student;
    public $teacher;
    public $schedule;
    public $name;

    public function __construct($student,$teacher,$schedule)
    {
        $this->student = $student;
        $this->teacher = $teacher;
        $this->schedule = $schedule;
        $this->name = $teacher->name.' '.$teacher->last_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('El profesor ha cancelado una clase.')
                    ->markdown('emails.teachers.cancelPenalty');
    }
}
