<?php

namespace App\Mail\Teacher;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class TeacherCancelPenalty extends Mailable
{
    use Queueable, SerializesModels;

    public $student;
    public $teacher;
    public $schedule;
    public $name;

    public function __construct($student,$teacher,$schedule)
    {
        $this->student = $student;
        $this->schedule = $schedule;
        $this->name = $teacher->name.' '.$teacher->last_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Hola ,'.$this->name.', haz solicitado una cancelación')
                    ->markdown('emails.teachers.cancelTeacherPenalty');
    }
}
