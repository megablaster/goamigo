<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TeacherFirst extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $name;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->name = $user->name.' '.$user->last_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Hola, '.$this->name.' '.'estas a solo dos pasos de completar tu registro.')
                    ->markdown('emails.teachers.register');
    }
}
