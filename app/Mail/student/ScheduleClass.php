<?php

namespace App\Mail\Student;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class ScheduleClass extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $schedule;
    public $name;

    public function __construct(User $user,Schedule $schedule)
    {
        $this->user = $user;
        $this->name = $this->user->nickname;
        $this->schedule = $schedule;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Hola, '.$this->name.' '.'hemos recibido clase agend.')
                    ->markdown('emails.teachers.contract');
    }
}
