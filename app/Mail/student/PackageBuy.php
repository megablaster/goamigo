<?php

namespace App\Mail\Student;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Product;

class PackageBuy extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $product;

    public function __construct(User $user,Product $product)
    {
        $this->user = $user;
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Se ha adquirido un paquete de créditos - '.$this->product->name)
                    ->markdown('emails.students.package');
    }
}
