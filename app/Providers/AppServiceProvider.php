<?php

namespace App\Providers;

use App;
use Session;
use View;
use App\Models\System;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('systems')){
            $system_setting = System::find(1);
            View::share('global_system', $system_setting);
        }
    }
}
