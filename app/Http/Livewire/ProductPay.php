<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Product;
use App\Models\Credit;
use Exception;
use Auth;

class ProductPay extends Component
{
    public $product;
    public $credit;
    protected $listeners = ['paymentMethodCreate'];

    public function mount(Product $product,Credit $credit)
    {
        $this->product = $product;
        $this->credit = $credit;
    }
    
    public function render()
    {
        return view('livewire.product-pay');
    }

    public function paymentMethodCreate($paymentMethod){

        try {

            auth()->user()->charge($this->product->price * 100, $paymentMethod);

            //We update the days and credits
            $credit = $this->Credit::where('user_id',Auth::user()->id)->first();
            $credit->credit = $credit->credit + $product->credits;
            $credit->save();

            $this->emit('resetStripe');
            
        } catch (Exception $e){

            $this->emit('errorPayment');

        }

    }
}
