<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserDisable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $suspend = auth()->user()->suspend;

        if ($suspend == 1) {
            return redirect()->route('admin.suspend')->with(
                'error','Su cuenta ha sido deshabilitada, pongase en contacto con el administrador.'
            );
        }

        return $next($request);
    }
}
