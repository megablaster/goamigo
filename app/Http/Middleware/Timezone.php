<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Timezone as Time;
use Auth;

class Timezone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {

            // $timezone = Time::find(auth()->user()->timezone_id)->first();
            date_default_timezone_set('America/Mexico_City');

        } else {

            date_default_timezone_set('America/Mexico_City');
            
        }
        
        return $next($request);
    }
}
