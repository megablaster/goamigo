<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png|required|max:2000',
        ];
    }

    public function messages()
    {
        return [
              'title.required' => 'El :attribute es obligatorio.',
              'description.required' => 'El :attribute es obligatorio.',
              'image.required' => 'La :attribute es obligatorio.',
              'image.mimes' => 'El formato de la :attribute no es válido (jpeg, jpg o png).',
        ];
    }
    
    public function attributes()
    {
        return [
              'title' => 'título',
              'description' => 'descripción',
              'image' => 'imagen',
        ];
    }
}
