<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'email' => ['required', Rule::unique('users')->ignore($this->user)],
            // 'password' => 'sometimes|confirmed|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ];
    }

    public function messages()
    {
        return [
              'name.required' => 'El :attribute es obligatorio.',
              'name.min' => 'El :attribute debe tener al menos :min caracteres.',
              'email.required' => 'El :attribute es obligatorio.',
              'password.required' => 'La :attribute es obligatoria.',
              'password_confirmation.confirmed' => 'La :attribute no concuerda.'
        ];
    }
    
    public function attributes()
    {
        return [
              'name' => 'nombre',
              'email' => 'correo',
              'password' => 'contraseña',
              'password_confirmation' => 'contraseña'
        ];
    }
}
