<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLevelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'lessons' => 'required'
        ];
    }

    public function messages()
    {
        return [
              'name.required' => 'El :attribute es obligatorio.',
              'lessons.required' => 'Las no. de :attribute es obligatorio.',
        ];
    }
    
    public function attributes()
    {
        return [
              'name' => 'nombre',
              'lessons' => 'clases'
        ];
    }
}
