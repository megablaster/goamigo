<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    public function __construct(){
        $this->middleware('guest')->except('logout');
    }

    protected $redirectTo = '/go-admin';

    protected function redirectTo()
    {
        if (auth()->user()->hasRole('admin')) {
            return '/go-admin';
        } elseif(auth()->user()->hasRole('teacher')) {
            return '/go-admin/teachers';
        } else {
          return '/go-admin/students';
        }
        
    }

    public function logout() {
      Auth::logout();
      return redirect('/login');
    }
}
