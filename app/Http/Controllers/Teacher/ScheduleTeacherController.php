<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\ScheduleTeacher;
use App\Mail\Teacher\StudentCancel;
use App\Mail\Teacher\TeacherCancel;
use App\Mail\Teacher\StudentCancelPenalty;
use App\Mail\Teacher\TeacherCancelPenalty;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Penalty;
use App\Models\Credit;
use App\Models\User;
use App\Models\Invoice;
use App\Models\InvoiceTeacher;
use App\Models\ScheduleStudent;
use Mail;

class ScheduleTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {

            $data = ScheduleTeacher::select('id')->get();
            return response()->json($data);
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    public function cancelTeacher(Request $request)
    {
        //Obtenemos student
        $user = User::find(auth()->user()->id);

        if ($request->type == 'min') {

            $schedule = ScheduleStudent::find($request->id);
            $schedule->status = 'cancel';
            $schedule->save();

            $teacher = ScheduleTeacher::find($schedule->schedule_id);

            //Cambiamos status de factura
            $invoice = Invoice::where('schedule_id',$schedule->student->id)->first();
            $invoice->status = 'cancel';
            $invoice->save();

            //Reasignamos el crédito
            $credit = Credit::where('user_id',$schedule->student->id)->first();
            $credit->credit = $credit->credit + 1;
            $credit->save();

            //Agregamos penalización
            $penalty = Penalty::where('user_id',auth()->user()->id)->first();
            $penalty->actually = $penalty->actually + 1;
            $penalty->save();


            $invoice = InvoiceTeacher::where('schedule_id',auth()->user()->id)->first();
            $invoice->status = 'cancel';
            $invoice->save();

            //Enviamos correo a teacher
            Mail::to(auth()->user()->email)->send(new TeacherCancelPenalty($schedule->student,$schedule->teacher,$schedule));

            //Enviamos correo a student
            Mail::to($schedule->student->email)->send(new StudentCancelPenalty($schedule->student,$schedule->teacher,$schedule));

            return redirect()->back()->with('success','Se ha cancelado la clase correctamente, lamentamos informarte que se te ha asignado un strike.');

        } else if($request->type == 'more') {

            $schedule = ScheduleStudent::find($request->id);
            $schedule->status = 'cancel';
            $schedule->save();

            $teacher = ScheduleTeacher::find($schedule->schedule_id);

            //Cambiamos status de factura
            $invoice = Invoice::where('schedule_id',$teacher->id)->first();
            $invoice->status = 'cancel';
            $invoice->save();

            //Cambiamos status de factura
            $invoice = InvoiceTeacher::where('schedule_id',$request->id)->first();
            $invoice->status = 'cancel';
            $invoice->save();

            //Reasignamos el crédito
            $credit = Credit::where('user_id',$schedule->student->id)->first();
            $credit->credit = $credit->credit + 1;
            $credit->save();

            //Agregamos penalización
            $penalty = Penalty::where('user_id',auth()->user()->id)->first();
            $penalty->actually = $penalty->actually + 1;
            $penalty->save();

            //Enviamos correo a teacher
            Mail::to(auth()->user()->email)->send(new TeacherCancel($schedule->student,$schedule->teacher,$schedule));

            //Enviamos correo a student
            Mail::to($schedule->student->email)->send(new StudentCancel($schedule->student,$schedule->teacher,$schedule));

            return redirect()->back()->with('success','Se ha cancelado la clase correctamente, se te ha asignado un strike.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ScheduleTeacher  $scheduleTeacher
     * @return \Illuminate\Http\Response
     */
    public function show(ScheduleTeacher $scheduleTeacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ScheduleTeacher  $scheduleTeacher
     * @return \Illuminate\Http\Response
     */
    public function edit(ScheduleTeacher $scheduleTeacher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ScheduleTeacher  $scheduleTeacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ScheduleTeacher $scheduleTeacher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ScheduleTeacher  $scheduleTeacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(ScheduleTeacher $scheduleTeacher)
    {
        //
    }

    public function cancel(Request $request)
    {
        //Obtenemos student
        $user = User::find(auth()->user()->id);

        if ($request->type == 'min') {

            $schedule = ScheduleStudent::find($request->id);
            $schedule->status = 'cancel';
            $schedule->save();

            //Enviamos correo a Teacher
            Mail::to()->send(new TeacherCancel($user,$schedule->teacher,$schedule));

            
        } else if($request->type == 'more') {

            $schedule = ScheduleStudent::find($request->id);
            $schedule->status = 'cancel';
            $schedule->save();

            //Habilitamos la clase agendada del maestro
            $teacher = ScheduleTeacher::find($schedule->schedule_id);
            $teacher->assigned = 0;
            $teacher->save();

            //Cambiamos status de factura
            $invoice = Invoice::where('schedule_id',$teacher->id)->first();
            $invoice->status = 'cancel';
            $invoice->save();

            //Reasignamos el crédito
            $credit = Credit::where('user_id',auth()->user()->id)->first();
            $credit->credit = $credit->credit + 1;
            $credit->save();

            //Agregamos penalización
            $penalty = Penalty::where('user_id',auth()->user()->id)->first();
            $penalty->actually = $penalty->actually + 1;
            $penalty->save();

            //Enviamos correo a teacher
            Mail::to($schedule->teacher->email)->send(new TeacherCancel($user,$schedule->teacher,$schedule));

            //Enviamos correo a student
            Mail::to(auth()->user()->email)->send(new StudentCancel($user,$schedule->teacher,$schedule));


            return redirect()->back()->with('success','Se ha cancelado la clase correctamente, se te ha abonado 1 crédito.');

        }
    }

    public function ajax(Request $request)
    {
         $date = Carbon::parse($request->start);

         switch ($request->type) {
           case 'create':
            
              $event = ScheduleTeacher::create([
                  'user_id' => $request->user_id,
                  'title' => $request->title,
                  'start' => $request->start,
                  'end' => $request->end,
                  'days' => $request->days,
                  'hour' => $date->toTimeString()
              ]);
 
              return response()->json($event);
             break;
  
           case 'update':

              $event = ScheduleTeacher::find($request->id)->update([
                  'user_id' => $request->user_id,
                  'title' => $request->title,
                  'start' => $request->start,
                  'end' => $request->end,
                  'days' => $request->days,
                  'hour' => $date->toTimeString()
              ]);
 
              return response()->json($event);
             break;
  
           case 'delete':

              $event = ScheduleTeacher::find($request->id)->delete();
              return response()->json($request->event);

             break;
             
           default:
             # code...
             break;
        }

    }

    public function getDates(Request $request,$id)
    {

        $data = ScheduleTeacher::where('user_id',$id)->get();
        return response()->json($data);
        
    }

}
