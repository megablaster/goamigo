<?php

namespace App\Http\Controllers\Teacher;

use App\Models\InvoiceTeacher;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvoiceTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'invoices' => InvoiceTeacher::where('teacher_id',auth()->user()->id)->latest()->get()
        ];

        return view('teacher.invoices.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvoiceTeacher  $invoiceTeacher
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceTeacher $invoiceTeacher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InvoiceTeacher  $invoiceTeacher
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceTeacher $invoiceTeacher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InvoiceTeacher  $invoiceTeacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceTeacher $invoiceTeacher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvoiceTeacher  $invoiceTeacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceTeacher $invoiceTeacher)
    {
        //
    }
}
