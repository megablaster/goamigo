<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ScheduleStudent;
use Illuminate\Support\Facades\Http;
use App\Models\ScheduleTeacher;
use App\Models\Classroom;
use Carbon\Carbon;

class ClassroomTeacherController extends Controller
{
    public function index(){

    	$id = auth()->user()->id;

        $data = [
            'schedules' => ScheduleStudent::where('teacher_id',$id)->where('status','active')->get()
        ];

        return view('teacher.classroom.index',compact('data'));
    }

    public function rate($id){

        $schedule = ScheduleStudent::find($id);

        $data = [
            'schedule' => $schedule
        ];

        return view('teacher.classroom.view',compact('data'));
    }

    public function save_rate(Request $request){

        $schedule = ScheduleStudent::find($request->id);
        $schedule->comments = $request->comments;
        $schedule->save();

        return redirect()->back()->with('success','Se han enviado sus comentarios, gracias.');

    }

    public function create_room($id){

        //Get classroom
        //$classroom = Classroom::where('schedule_id',$id)->first();

        //Endpoints
        $endUser = "https://api.bluejeans.com/oauth2/token#User";

        //Info General
        $appkey = "37429843263245";
        $appsecret  = "d1c64b37879242b6a3eae423c74f2589";

        //Get Token Access
        $res = $this->getAccessToken($endUser);
        $token = $res['access_token'];
        $user_id = $res['scope']['user'];

        try {

            //Create Meeting
            $meeting = $this->createMeeting($user_id,$token,$id);
            dd($meeting);

            //Save code meeting
            $decode = json_decode($response);
            $classroom->url = $decode->response->url;
            $classroom->id_vedamo = $decode->response->id;
            $classroom->save();
            return redirect()->away($classroom->url);

        } catch (\Exception $e) {

            return redirect()->back()->with('warning',$e->getMessage());
        }
    }

    public function getAccessToken($endUser)
    {
        $data = array(
            'grant_type' => 'password',
            'username' => 'blue6.brauliomiramontes',
            'password' => 'Megablaster007',
        );

        $client = new \GuzzleHttp\Client();

        $response = $client->post($endUser, [
            'body' => json_encode($data)
        ]);

        return $res = json_decode($response->getBody(), true);
    }

    public function createMeeting($user_id,$token,$schedule)
    {
        //Get classroom
        $classroom = Classroom::where('schedule_id',$schedule)->first();

         //Convert Timestamp
        $start = Carbon::parse($classroom->schedule->start);
        $end = Carbon::parse($classroom->schedule->end);

        //Point for create meeting
        $endMeeting = "https://api.bluejeans.com/v1/user/".$user_id."/scheduled_meeting?personal_meeting=false&email=true&access_token=".$token."";

        //Create meeting
        $data = array(
          "title" => $classroom->teacher->name.' '.$classroom->teacher->last .' - '. $classroom->student->name.' '.$classroom->student->last,
          "description" => "Scheduled meeting",
          "start" => $this->milliseconds($start),
          "end" => $this->milliseconds($end),
          "timezone" => "America/New_York",
          "endPointType" => "string",
          "endPointVersion" => "string",
          "attendees" => array(
            array(
                "email" => "codingear@gmail.com"
            )
          ),
          "isLargeMeeting" => true
        );

        //return $data;

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];

        $client = new \GuzzleHttp\Client();

        $response = $client->post($endMeeting, [
            'headers' => $headers,
            'body' => json_encode($data)
        ]);

        return $res = json_decode($response->getBody(), true);
    }


    public function milliseconds($val)
    {
        $stamp = strtotime($val);
        $time_in_ms = $stamp*1000;
        return $time_in_ms;
    }
}
