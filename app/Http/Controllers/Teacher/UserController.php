<?php

namespace App\Http\Controllers\Teacher;

use Mail;
use Storage;
use Countries;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Penalty;
use App\Models\Timezone;
use App\Models\InfoStudent;
use App\Models\InfoTeacher;
use App\Models\Credit;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Spatie\Permission\Models\Role;
use App\Mail\UserRegister;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'users' => User::select('id','name','email','phone','country','suspend')->latest()->get()
        ];

        return view('admin.users.index',compact('data'));
    }

     public function type($type)
    {
        $data = [
            'users' => User::role($type)->select('id','name','email','phone','country','suspend')->latest()->get()
        ];

        return view('admin.users.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('admin.users.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);

        if ($validator->fails()) {
            return redirect('post/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::create($request->except('_token','rol','password_confirmation'));
        $user->email_verified_at = NOW();
        $user->password = bcrypt($request->password);
        $user->save();

        //Asignamos el rol
        $user->syncRoles($request->rol);

        //Create table student
        if ($user->hasRole('student')) {

            $credit = Credit::where('user_id',$user->id)->first();

            if (is_null($credit)) {

                $credit = Credit::create([
                    'user_id' => $user->id,
                    'expiration' => Carbon::now(),
                    'credit' => 0,
                    'trial' => false,
                ]);
            }

            Penalty::create([
                'actually' => 0,
                'maximum' => 2,
                'type' => 'student',
                'user_id' => $user->id
            ]);

        } else if($user->hasRole('teacher')){

            Penalty::create([
                'actually' => 0,
                'maximum' => 5,
                'type' => 'teacher',
                'user_id' => $user->id
            ]);

        }

        //Se crea el registro de estudiante
        InfoStudent::create([
            'user_id' => $user->id
        ]);

        InfoTeacher::create([
            'user_id' => $user->id
        ]);
        

        //Insert password for email
        $user['password'] = $request->password;

        //Send email user register
        Mail::to($user->email)->send(new UserRegister($user));

        //Add user to Stripe
        $user->createAsStripeCustomer();

        return redirect()->route('users.edit',$user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            if($id == auth()->user()->id){

                $data = [
                    'user' => User::find($id),
                    'timezones' => Timezone::Orderby('offset')->get()
                ];
                
                return view('teacher.users.edit',compact('data'));

            }

        } catch (Exception $e){

            return back()->with('success',$exception->getMessage());

        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        // dd($request->all());

        if ($request->type == 'teacher') {

            $user->teacher->experience = $request->experience;
            $user->teacher->letter_presentation = $request->letter_presentation;
            $user->teacher->approve_info = 0;
            $user->teacher->save();


            return redirect()->route('users_teacher.edit',$user->id)->with('success','Se ha actualizado la información personal.');

        } else {

            if ($request->password) {

                $validator = Validator::make($request->all(), [
                    'name' => 'required|min:5',
                    'email' => 'required|unique:users,email,'.$user->id,
                    'password' => 'required|confirmed|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                ]);

            } else {

                $validator = Validator::make($request->all(), [
                    'name' => 'required|min:5',
                    'email' => 'required|unique:users,email,'.$user->id,
                ]);

            }

            if ($validator->fails()) {
                return redirect()
                            ->route('users.edit',$user->id)
                            ->withErrors($validator)
                            ->withInput();
            }

            //Update Role
            if ($request->rol) {
                $user->syncRoles($request->rol);   
            }

            $user->update($request->except('_token','_method','rol','password','password_confirmation'));

            if ($user->hasRole('student')) {

                $credit = Credit::where('user_id',$user->id)->first();

                if (is_null($credit)) {

                    $credit = Credit::create([
                        'user_id' => $user->id,
                        'expiration' => Carbon::now()->subDays(10),
                        'credit' => 1,
                        'trial' => true,
                    ]);
                }

            } else {

                $credit = Credit::where('user_id',$user->id)->first();
                if (!is_null($credit)) {
                    $credit->delete();
                }
                
            }

            if ($request->password) {
                $user->password = bcrypt($request->password);
            }

            if ($request->image) {

                //Delete image
                Storage::delete($user->img);
                $path = $request->file('image')->store('user');
                $user->img = $path;

            }

            //Save image
            $user->save();

        }

        return redirect()->route('users_teacher.edit',$user->id)->with('success','Se ha actualizado la información personal.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Delete user
        $user = User::findOrFail($id);
        $user->delete();
        
        return redirect()->route('users.index')->with('success','Se ha borrado el usuario.');
    }

    public function suspend($id)
    {
        //Obtener usuario
        $user = User::findOrFail($id);

        if ($user->suspend == 1) {
            
            $user->suspend = 0;
            $user->save();
            return redirect()->route('users.index')->with('success','Se ha quitado la suspensión del usuario.');   

        } else {

            $user->suspend = 1;
            $user->save();
            return redirect()->route('users.index')->with('success','Se ha suspendido el usuario.');   
        }
    }
}
