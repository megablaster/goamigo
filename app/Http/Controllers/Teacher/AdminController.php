<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\System;
use App\Models\ScheduleStudent;
use App\Models\ScheduleTeacher;
use App\Models\InvoiceTeacher;
use Carbon\Carbon;

class AdminController extends Controller
{
	public function __construct(){
	    $this->middleware(['role:teacher']);
	}

    public function index()
    {
        $incomesPaid = InvoiceTeacher::whereBetween('created_at',[Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])->where('status','paid')->get();
        $incomesHalf = InvoiceTeacher::whereBetween('created_at',[Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])->where('status','payhalf')->get();
        $incomeTrial = InvoiceTeacher::whereBetween('created_at',[Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])->where('status','trial')->get();

        $system = System::find(1);

        $paid = $incomesPaid->count() * $system->paymentfull;
        $half = $incomesHalf->count() * $system->paymenthalf;
        $trial = $incomeTrial->count() * $system->paymentfull;

    	$data = [
    		'students' => ScheduleStudent::where('teacher_id', auth()->user()->id)->select('id')->get(),
            'incomes' => $paid + $half + $trial,
            'classes' => ScheduleStudent::where('teacher_id',auth()->user()->id)->get()
    	];

    	if (auth()->user()->teacher->step == 'third') {
    		return view('teacher.index',compact('data'));
    	} else {
    		return redirect()->route('users_teacher.edit',auth()->user()->id);
    	}

    }
}
