<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\Teacher;
use App\Models\Timezone;
use App\Models\ScheduleTeacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function calendar()
    {
        return 'Calendario';
    }

    public function availability(Request $request)
    {
        $timezone = Timezone::find(auth()->user()->id);
        return view('teacher.availability',compact('timezone'));
    }
}
