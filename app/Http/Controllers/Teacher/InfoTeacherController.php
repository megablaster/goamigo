<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InfoTeacher;

class InfoTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Update days
        if ($request->type == 'availability') {

            $info = InfoTeacher::where('user_id',$id)->first();

            $monday = $this->save_hour($request->monday_init,$request->monday_fin);
            $info->monday = $monday;
            $info->save();

            if ($request->monday_enable == 'on') {
                $info->monday_enable = 1;
                $info->save();
            } else {
                $info->monday_enable = 0;
                $info->save();
            }

            $tuesday = $this->save_hour($request->tuesday_init,$request->tuesday_fin);
            $info->tuesday = $tuesday;
            $info->save();

            if ($request->tuesday_enable == 'on') {
                $info->tuesday_enable = 1;
                $info->save();
            } else {
                $info->tuesday_enable = 0;
                $info->save();
            }

            $wednesday = $this->save_hour($request->wednesday_init,$request->wednesday_fin);
            $info->wednesday = $wednesday;
            $info->save();

            if ($request->wednesday_enable == 'on') {
                $info->wednesday_enable = 1;
                $info->save();
            } else {
                $info->wednesday_enable = 0;
                $info->save();
            }

            $thursday = $this->save_hour($request->thursday_init,$request->thursday_fin);
            $info->thursday = $thursday;
            $info->save();

            if ($request->thursday_enable == 'on') {
                $info->thursday_enable = 1;
                $info->save();
            } else {
                $info->thursday_enable = 0;
                $info->save();
            }

            $friday = $this->save_hour($request->friday_init,$request->friday_fin);
            $info->friday = $friday;
            $info->save();

            if ($request->friday_enable == 'on') {
                $info->friday_enable = 1;
                $info->save();
            } else {
                $info->friday_enable = 0;
                $info->save();
            }

            $saturday = $this->save_hour($request->saturday_init,$request->saturday_fin);
            $info->saturday = $saturday;
            $info->save();

            if ($request->saturday_enable == 'on') {
                $info->saturday_enable = 1;
                $info->save();
            } else {
                $info->saturday_enable = 0;
                $info->save();
            }

            $sunday = $this->save_hour($request->sunday_init,$request->sunday_fin);
            $info->sunday = $sunday;
            $info->save();

            if ($request->sunday_enable == 'on') {
                $info->sunday_enable = 1;
                $info->save();
            } else {
                $info->sunday_enable = 0;
                $info->save();
            }

            // $info->update($request->except('_token','_method','type','monday_enable','tuesday_enable','wednesday_enable','thursday_enable','friday_enable','saturday_enable','sunday_enable'));
            return redirect()->back()->with('success','Se actualizan las horas de disponibilidad');

        } else {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function date($date)
    {
        return date("G:i", strtotime($date));
    }

    public function save_hour($ini,$final)
    {
        $init = explode(':', $ini);
        $fin = explode(':', $final);

        $array = [];

        for ($i = $init[0]; $i < $fin[0]+1; $i++) { 

            if ($i < 10) {
                if ($i == $init[0]) {
                    $array[] = $i.':00';    
                } else {
                    $array[] = '0'.$i.':00';    
                }
                
            } else {
                $array[] = $i.':00';
            }
        }

        return json_encode($array);
    }
}
