<?php

namespace App\Http\Controllers;

use App\Models\Review;
use App\Models\Point;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function update(Request $request, Review $review)
    {
        $review->comment_dad = $request->comments;
        $review->stars = $request->stars;
        $review->save();

        //Add stars
        $point = Point::where('schedule_id',$request->schedule_id)->first();

        if ($point->points == 0) {
            $point->user_id = $request->teacher_id;
            $point->points = $request->stars;
            $point->save();
        }

        return redirect()->route('schedulestudent.show',$request->review_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }
}
