<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Str;


class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware(['role:admin|manager']);
    }

    public function index()
    {
        $data = [
            'categories' => Category::latest()->get()
        ];

        return view('admin.categories.index',compact('data'));
    }

    public function create()
    {
        $categories = Category::get();
        return view('admin.categories.create',compact('categories'));
    }

    
    public function store(Request $request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->url = Str::slug($category->name);

        $category->save();

        return redirect()->route('categories.index');
    }

    public function edit($id)
    {
        $data = [
            'category' => Category::find($id),
        ];

        return view('admin.categories.edit',compact('data'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->description = $request->description;

        //Create url
        if ($request->url){
            $category->url = $request->url;
        }

        $category->save();

        return redirect()->back()->with('success','Se ha actualizado la categoría.');

    }

    public function destroy($id)
    {
        $category = Category::find($id);

        if ($category->posts->count() > 0) {
            return redirect()->route('categories.index')->with('failed', 'No se ha eliminado la categoría, ya que cuenta con entradas.');   
        } else {
            $category->delete();
            return redirect()->route('categories.index')->with('success', 'Se ha borrado la categoría.');   
        }
    }
}
