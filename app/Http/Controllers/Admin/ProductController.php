<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Auth;

class ProductController extends Controller
{
    public function index()
    {
        $data = [
            'products' => Product::all()
        ];

        return view('admin.products.index',compact('data'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(Request $request)
    {
        $product = new Product;
        $product->create($request->except('_token'));

        return redirect()->route('products.index')->with('success','Se ha agregado el nuevo producto.');
    }

    public function edit(Product $product)
    {
        $data = [
            'product' => Product::find($product->id)
        ];

        return view('admin.products.edit',compact('data'));
    }

    public function update(Request $request, Product $product)
    {
        $product->update($request->except('_token'));

        return redirect()->route('products.index')->with('success', 'Se ha actualizado el producto.');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index')->with('success', 'Se ha borrado el producto.');
    }

    public function pay(Product $product)
    {
        // $intent = Auth::user()->createSetupIntent();
        return view('student.products.pay',compact('product'));
    }
}
