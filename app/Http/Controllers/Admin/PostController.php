<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Timezone;
use App\Models\User;
use App\Models\Post;
use App\Models\Category;
use App\Models\Post_gallery;
use Illuminate\Http\Request;
use Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware(['role:admin|manager']);
    }

    public function index()
    {
        $data = [
            'posts' => Post::latest()->get()
        ];

        return view('admin.posts.index',compact('data'));
    }

    public function create()
    {
        $categories = Category::get();
        return view('admin.posts.create',compact('categories'));
    }

    
    public function store(Request $request)
    {
        $validated = $request->validate([
           'image' => 'required|image|mimes:jpg,png,jpeg|max:512|dimensions:max_width=900,max_height=543',
           'description'=> 'required',
           'name' => 'required',
           'category_id' => 'required'
        ]);

        $post = new Post;
        $post->name = $request->name;
        $post->description = $request->description;
        $post->keywords = $request->keywords;
        $post->meta_description = $request->meta_description;
        $post->url = Str::slug($post->name);
        $post->category_id = $request->category_id;
        $post->user_id = auth()->user()->id;

        if ($request->image) {
            //Delete image
            $path = $request->file('image')->store('post');
            $post->img = $path;
        }

        $post->save();

        return redirect()->route('posts.index');
    }

    public function edit($id)
    {
        $data = [
            'post' => Post::find($id),
            'categories' => Category::get()
        ];

        return view('admin.posts.edit',compact('data'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'description' => 'required',
        ];

        $customMessages = [
            'description.required' => 'La descripción es necesaria.',
        ];

        $this->validate($request, $rules, $customMessages);

        $post = Post::find($id);
        $post->name = $request->name;
        $post->description = $request->description;
        $post->category_id = $request->category_id;
        $post->keywords = $request->keywords;
        $post->meta_description = $request->meta_description;

        //Create url
        if ($request->url){
            $post->url = $request->url;
        }

        //Author
        $post->user_id = Auth()->user()->id;

        if ($request->image) {

            $rules = [
                'image' => 'required|image|mimes:jpg,png,jpeg|max:512|dimensions:min_width=900,min_height=543',
            ];

            $customMessages = [
                'mimes' => 'La portada no cumple con el formato jpg, png o jpeg.',
                'dimensions' => 'La portada no cumple con las dimensiones mínimas de 900x350px.',
                'max' => 'La portada supera el tamaño de 512kb.',
            ];

            $this->validate($request, $rules, $customMessages);

            //Delete image
            Storage::delete($post->img);
            $path = $request->file('image')->store('post');
            $post->img = $path;
        }

        $post->save();

        return redirect()->back()->with('success','Se ha actualizado el post.');

    }

    public function gallery(Request $request)
    {
       

        //Get post
        $post = Post::find($request->post_id);

        $gallery = new Post_gallery;
        $gallery->post_id = $request->post_id;

        if ($request->image) {

            $rules = [
                'image' => 'required|image|mimes:jpg,png,jpeg|max:512|dimensions:min_width=550,min_height=550',
            ];

            $customMessages = [
                'mimes' => 'La galería no cumple con el formato jpg, png o jpeg.',
                'dimensions' => 'La galería no cumple con las dimensiones mínimas de 550x550px.',
                'max' => 'La galería supera el tamaño de 512kb.',
            ];

            $this->validate($request, $rules, $customMessages);

            //Delete image
            Storage::delete($post->img);
            $path = $request->file('image')->store('post');
            $gallery->img = $path;

        }

        $gallery->save();

        return redirect()->back()->with('success','Se ha subido la foto de galería.');
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect()->route('posts.index')->with('success', 'Se ha borrado la entrada.');
    }

    public function destroyGallery(Request $request)
    {
        $gallery = Post_gallery::where('id',$request->id)->first();
        $gallery->delete();
        return redirect()->route('posts.edit',[$request->post_id])->with('success', 'Se ha borrado la foto de galería.');
    }
}
