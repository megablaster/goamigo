<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\InvoiceTeacher;
use App\Models\InvoiceProduct;
use App\Models\Invoice;
use Carbon\Carbon;

class AccountingController extends Controller
{
    public function income()
    {
    	$data = [
    		// 'invoices' => InvoiceProduct::whereBetween('created_at',[Carbon::now()->startOfMonth(),Carbon::now()->endOfMonth()])->latest()->get(),
            'invoices' => InvoiceProduct::latest()->get(),
    	];

    	return view('admin.accounting.income', compact('data'));
    }

    public function lesson()
    {
         $data = [
            'invoices' => Invoice::latest()->get()
        ];

        return view('admin.accounting.lesson',compact('data'));
    }
}
