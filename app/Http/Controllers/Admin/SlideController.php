<?php

namespace App\Http\Controllers\Admin;

use App\Models\Slide;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreSlideRequest;
use Storage;

class SlideController extends Controller
{
    public function __construct(){
        $this->middleware(['role:admin']);
    }
    
    public function index()
    {
        $data = [
            'slides' => Slide::select('id','title','description','url')->latest()->get()
        ];

        return view('admin.slides.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSlideRequest $request)
    {
        $slide = Slide::create($request->except('_token','image'));

        //Save image
        $path = $request->file('image')->store('slide');
        $slide->img = $path;
        $slide->save();

        return redirect()->route('slides.edit',$slide->id)->with('success','Se agrega correctamente el rotador.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'slide' => Slide::find($id)
        ];

        return view('admin.slides.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slide = Slide::find($id);
        $slide->update($request->except('_token','image'));

        if ($request->image) {

            //Delete image
            Storage::delete($slide->img);
            $path = $request->file('image')->store('slide');
            $slide->img = $path;

            //Save image
            $slide->save();

        }

        return redirect()->route('slides.edit',$slide->id)->with('success','Se ha actualizado la información.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        $slide->delete();

        return redirect()->route('slides.index')->with('success','Se ha borrado el rotador.');
    }
}
