<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\Post;
use Auth;
use Session;
use Config;
use App;
use Carbon\Carbon;

class AdminController extends Controller
{

    public function __construct(){
        $this->middleware(['role:admin|teacher|student|manager']);
    }

    public function index()
    {
        //Redirect admin
        if (auth()->user()->hasRole('student')) {
            return redirect()->route('admin.student.index');
        } elseif(auth()->user()->hasRole('teacher')) {
            return redirect()->route('admin.teacher.index');
        }

    	$data = [
    		'students' => User::role('student')->select('id')->get(),
            'teachers' => User::role('teacher')->whereHas('teacher', function($query) {
                $query->where('step', 'third');
            })->get(),
            'requests' => User::role('teacher')->whereHas('teacher', function($query) {
                $query->where('step', 'first');
            })->get(),
			'products' => Product::all(),
            'posts' => Post::latest()->get(),
            'categories' => Category::select('id')->get(),
            'moreView' => Post::orderBy('views', 'DESC')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->first(),
            'lessView' => Post::orderBy('views', 'ASC')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->first(),
    	];

    	return view('admin.index',compact('data'));
    }

    public function suspend()
    {
        return 'Su cuenta ha sido suspendida, comunicate con un administrador.';
    }

    public function lang(Request $request)
    {
        $lang = $request->lang;
        Session::put('locale',$lang);
        return redirect()->back();

    }
}
