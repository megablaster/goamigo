<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Level;
use App\Http\Requests\StoreLevelRequest;
use Auth;

class LevelController extends Controller
{
    public function __construct(){
        $this->middleware(['role:admin']);
    }
    
    public function index()
    {
        $data = [
            'levels' => Level::select('id','name','description','lessons')->latest()->get()
        ];

        return view('admin.levels.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.levels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLevelRequest $request)
    {
        $level = Level::create($request->except('_token'));
        $level->save();

        return redirect()->route('levels.index')->with('success','Se agrego el nivel.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'level' => level::find($id)
        ];

        return view('admin.levels.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Get level
        $level = Level::find($id);

        //Get Lang
        $level->setTranslation('name',Auth::user()->language,$request->name)->save();
        $level->setTranslation('description',Auth::user()->language,$request->description)->save();
        $level->lessons = $request->lessons;
        $level->save();

        return redirect()->route('levels.index')->with('success','Se ha actualizado la información del nivel.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level = Level::find($id);
        $level->delete();

        return redirect()->route('levels.index')->with('success','Se ha borrado el nivel.');
    }
}
