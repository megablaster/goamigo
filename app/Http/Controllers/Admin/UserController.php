<?php

namespace App\Http\Controllers\Admin;

use Mail;
use Storage;
use Countries;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Penalty;
use App\Models\Timezone;
use App\Models\InfoStudent;
use App\Models\InfoTeacher;
use App\Models\Credit;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Spatie\Permission\Models\Role;
use App\Mail\UserRegister;
use App\Mail\TeacherContract;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware(['role:admin|manager']);
    }

    public function index()
    {
        $data = [
            'users' => User::select('id','name','email','phone','country','suspend')->latest()->get()
        ];

        return view('admin.users.index',compact('data'));
    }

     public function type($type)
    {
        $data = [
            'users' => User::role($type)->select('id','name','email','phone','country','suspend')->latest()->get()
        ];

        return view('admin.users.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('admin.users.create',compact('data'));
    }

    public function featured(Request $request)
    {
        $user = User::find($request->user_id);
        $user->teacher->featured = $request->featured;
        $user->teacher->save();

        if ($request->featured == 1) {
            return redirect()->back()->with('success','Se ha destacado el profesor.');
        } else {
            return redirect()->back()->with('success','Se ha quitado del home el profesor.');
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:5',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[@$!%*#?&]).{6,}$/',
        ]);

        if ($validator->fails()) {
            return redirect('post/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::create($request->except('_token','rol','password_confirmation'));
        $user->email_verified_at = NOW();
        $user->password = bcrypt($request->password);
        $user->save();

        //Asignamos el rol
        $user->syncRoles($request->rol);

        //Create table student
        if ($user->hasRole('student')) {

            $credit = Credit::where('user_id',$user->id)->first();

            if (is_null($credit)) {

                $credit = Credit::create([
                    'user_id' => $user->id,
                    'expiration' => Carbon::now()->subDays(11),
                    'credit' => 0,
                    'trial' => false,
                ]);
            }

        }

        //Se crea el registro de estudiante
        InfoStudent::create([
            'user_id' => $user->id
        ]);

        InfoTeacher::create([
            'user_id' => $user->id
        ]);

        //Insert password for email
        $user['password'] = $request->password;

        //Send email user register
        Mail::to($user->email)->send(new UserRegister($user));

        //Add user to Stripe
        $user->createAsStripeCustomer();

        return redirect()->route('users.edit',$user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'user' => User::find($id),
            'last_user' => User::select('id')->latest()->first(),
            'timezones' => Timezone::Orderby('offset')->get()
        ];

        return view('admin.users.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$user->id,
            'password' => 'required|confirmed|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#$%&\()*+,₹./:;<=>?@[\]^_`{|}~-]).{6,}$/',
            'phone' => 'required|regex:/(01)[0-9]{9}/'
        ]);

       if ($request->type == 'teacher') {

            $user = User::find($id);

            $user->teacher->step = $request->step;
            $user->teacher->level_education = $request->level_education;
            $user->teacher->experience = $request->experience;
            $user->teacher->letter_presentation = $request->letter_presentation;
            $user->teacher->approve_info = $request->approve_info;

            if ($user->teacher->featured == 1) {
                $user->teacher->featured == 0;
            }

            $user->teacher->save();

            if ($request->step == 'third') {

                //Send email welcome y contract date
                $user->teacher->contract_date = Carbon::now();
                $user->teacher->save();

                Mail::to($user->email)->send(new TeacherContract($user));

            }


        } elseif($request->type == 'user') {

            if ($request->password) {

                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|unique:users,email,'.$user->id,
                    'password' => 'required|confirmed|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#$%&\()*+,₹./:;<=>?@[\]^_`{|}~-]).{6,}$/',
                    'phone' => 'required|regex:/(01)[0-9]{9}/'
                ]);

            } else {

                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|unique:users,email,'.$user->id,
                    'phone' => 'required|min:5|max:20'
                ]);

            }

            if ($validator->fails()) {
                return redirect()
                            ->route('users.edit',$user->id)
                            ->withErrors($validator)
                            ->withInput();
            }

            //Update Role
            if ($request->rol) {
                $user->syncRoles($request->rol);
            }

            $user->update($request->except('_token','_method','rol','password','password_confirmation'));

            if ($user->hasRole('student')) {

                $credit = Credit::where('user_id',$user->id)->first();

                if (is_null($credit)) {

                    $credit = Credit::create([
                        'user_id' => $user->id,
                        'trial' => false,
                    ]);
                }

            } else {

                $credit = Credit::where('user_id',$user->id)->first();

                if (!is_null($credit)) {
                    $credit->delete();
                }

            }

            if ($request->password) {
                $user->password = bcrypt($request->password);
            }

            if ($request->image) {

                //Delete image
                Storage::delete($user->img);
                $path = $request->file('image')->store('user');
                $user->img = $path;

            }

            //Save image
            $user->save();
        }

        return redirect()->route('users.edit',$user->id)->with('success','Se ha actualizado la información personal.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Delete user
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')->with('success','Se ha borrado el usuario.');
    }

    public function suspend($id)
    {
        //Obtener usuario
        $user = User::findOrFail($id);

        if ($user->suspend == 1) {

            $user->suspend = 0;
            $user->save();
            return redirect()->route('users.index')->with('success','Se ha quitado la suspensión del usuario.');

        } else {

            $user->suspend = 1;
            $user->save();
            return redirect()->route('users.index')->with('success','Se ha suspendido el usuario.');
        }
    }
}
