<?php

namespace App\Http\Controllers\Admin;

use App\Models\Testimonial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTestimonialRequest;
use Storage;
use Str;

class TestimonialController extends Controller
{
    public function __construct(){
        $this->middleware(['role:admin']);
    }
    
    public function index()
    {
        $data = [
            'testimonials' => Testimonial::select('id','name','profession')->latest()->get()
        ];

        return view('admin.testimonials.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $testimonial = Testimonial::create($request->except('_token','image'));

        //Save image
        $path = $request->file('image')->store('testimonial');
        $testimonial->img = $path;
        $testimonial->save();

        return redirect()->route('testimonials.edit',$testimonial->id)->with('success','Se agrega correctamente el testimonio.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'testimonial' => Testimonial::find($id)
        ];

        return view('admin.testimonials.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->update($request->except('_token','image'));

        if ($request->image) {

            //Delete image
            Storage::delete($testimonial->img);
            $path = $request->file('image')->store('testimonial');
            $testimonial->img = $path;

            //Save image
            $testimonial->save();

        }

        return redirect()->route('testimonials.edit',$testimonial->id)->with('success','Se ha actualizado la información.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->delete();

        return redirect()->route('testimonials.index')->with('success','Se ha borrado el testimonio.');
    }
}
