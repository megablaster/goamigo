<?php

namespace App\Http\Controllers\Admin;

use App\Models\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class SystemController extends Controller
{
    public function __construct(){
        $this->middleware(['role:admin']);
    }

    public function index()
    {
        $system = System::find(1);
        return view('admin.systems.index', compact('system'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function show(System $system)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function edit(System $system)
    {
        $system = $system;
        return view('admin.systems.edit', compact('system'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, System $system)
    {
        $system->update($request->except('_token','img'));

        //Update logo
        if ($request->logo) {

            //Delete image
            Storage::delete($system->logo);
            $path = $request->file('logo')->store('system');
            $system->logo = $path;
            $system->save();

        }

        return redirect()->route('systems.edit',$system->id)->with('success','Se actualiza correctamente la configuración.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\System  $system
     * @return \Illuminate\Http\Response
     */
    public function destroy(System $system)
    {
        //
    }
}
