<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VedamoController extends Controller
{
	public function create_room($schedule,$teacher,$student,$classroom_id)
    {
        //Get classroom
        $classroom = Classroom::find($classroom_id);

        $endpoint_url = env('VEDAMO_ENDPOINT');
        $consumer_key = env('VEDAMO_CUSTOMER_KEY');
        $secret  = env('VEDAMO_SHARE_KEY');

        $nonce  =  uniqid('', true);
        $signature_method  = "HMAC-SHA256";
        $version = "1.0";
        $access_token = "";
        $timestamp = time();

        // Create Room
        $data = array(
            'command' => array(
                'resource' => 'room',
                'action'   => 'create',
                    'params'   => array(
                        'creator_id' => $teacher->id,
                        'name' => "Aula virtual de".' '.$teacher->name,
                        'description' => "Aula virtual del profesor".' '.$teacher->name.' y el alumno:'.' '.$student->name,
                        'participants' => array(
                            array(
                                'user_id' => $teacher->id,
                                'name' => $teacher->name.' '.$teacher->last_name,
                                'username' => $teacher->name,
                                'email' => $teacher->email,
                                'role' => 'admin',
                                'notify' => array('email'),
                                'language' => 'es'
                            ),
                            array(
                                'user_id' => $student->id,
                                'name' => $student->name.' '.$student->last_name,
                                'username' => $student->name,
                                'email' => $student->email,
                                'role' => 'user',
                                'notify' => array('email'),
                                'language' => 'en'
                        ),
                    )
                )
            )
        );

        // Urlencode $data array with RFC3986 -> important for empty spaces, produces %20 instead of %2b (+ sign)
        $data_encoded = http_build_query($data, '', '&', PHP_QUERY_RFC3986);

        // A helper function to rawurldecode the $data_encoded.Takes care of extra "+" signs in the keys and values if any.
        function escape_decode($value) {
            return rawurldecode(str_replace('+', ' ', $value));
        };

        // Transforms the encoded data to one dimensional array in format:
        // ["command[resource]"]=> "room" ["command[action]"]=> "get" ["command[params][include_participants]"]=> "1"
        foreach (explode('&', $data_encoded) as $kvp) {
            $parts = explode('=', $kvp, 2);
            $key   = escape_decode($parts[0]);
            $value = isset($parts[1]) ? escape_decode($parts[1]) : null;

            if (!isset($result[$key])) {
                $data_sorted[$key] = $value;
            } else {
                if (!is_array($result[$key])) {
                $data_sorted[$key] = [$result[$key]];
                }

                $data_sorted[$key][] = $value;
            }
        }


        // Sort the data_sorted keys by string compare.
        uksort($data_sorted, 'strcmp');

        // Encode the sorted data with RFC3986
        $data_encoded_final = http_build_query($data_sorted, '', '&', PHP_QUERY_RFC3986);


        // Generate the base string needed for creation of the signature
        $base = "POST" . "&" . rawurlencode($endpoint_url) . "&"
            . rawurlencode($data_encoded_final)
                . rawurlencode("&oauth_consumer_key=" . rawurlencode($consumer_key)
                . "&oauth_nonce=" . rawurlencode($nonce)
                . "&oauth_signature_method=" . rawurlencode($signature_method)
                . "&oauth_timestamp=" . rawurlencode($timestamp)
                . "&oauth_token=" . rawurlencode($access_token)
                . "&oauth_version=" . rawurlencode($version)
        );


        // Encode the secret and access_token
        $key = rawurlencode($secret) . '&' . rawurlencode($access_token);

        // Generate the oauth_signature. Keep in mind that hash_hmac produce raw output instead of lowercase hexits.
        $signature = rawurlencode(base64_encode(hash_hmac('sha256', $base, $key, true)));


        // Generate the POST CURL request
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $endpoint_url);
        curl_setopt($ch,CURLOPT_POST, count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $data_encoded_final);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Uncomment the following 2 lines in case of curl SSL problems -> do not do this on production
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $headers = [
            'Authorization: OAuth oauth_consumer_key="' .$consumer_key .'",oauth_signature_method="' .$signature_method .'",oauth_timestamp="' .$timestamp .'",oauth_nonce="' .$nonce .'",oauth_version="' .$version .'",oauth_signature="' .$signature .'",oauth_token="' .$access_token .'"',
            'Content-Type: application/x-www-form-urlencoded'
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (!$response){
               echo 'Curl error: ' . curl_error($ch);
        } else {
            $decode = json_decode($response);

            $classroom->url = $decode->response->url;
            $classroom->id_vedamo = $decode->response->id;
            $classroom->save();

            $this->close_room($classroom->id_vedamo);

        }

        curl_close($ch);
    }

    public function close_room($id)
    {

        $endpoint_url = "https://api.vedamo.com/";
        $consumer_key = "463891";
        $secret  = "252932337ec60bb194fb5989f5390506";

        $nonce  =  uniqid('', true);
        $signature_method  = "HMAC-SHA256";
        $version = "1.0";
        $access_token = "";
        $timestamp = time();

        // Create Room
        $data = array(
            'command' => array(
                'resource' => 'room',
                'action'   => 'close',
                    'params'   => array(
                        'room_id' => $id,
                )
            )
        );

        // Urlencode $data array with RFC3986 -> important for empty spaces, produces %20 instead of %2b (+ sign)
        $data_encoded = http_build_query($data, '', '&', PHP_QUERY_RFC3986);

        // Transforms the encoded data to one dimensional array in format:
        // ["command[resource]"]=> "room" ["command[action]"]=> "get" ["command[params][include_participants]"]=> "1"
        foreach (explode('&', $data_encoded) as $kvp) {
            $parts = explode('=', $kvp, 2);
            $key   = escape_decode($parts[0]);
            $value = isset($parts[1]) ? escape_decode($parts[1]) : null;

            if (!isset($result[$key])) {
                $data_sorted[$key] = $value;
            } else {
                if (!is_array($result[$key])) {
                $data_sorted[$key] = [$result[$key]];
                }

                $data_sorted[$key][] = $value;
            }
        }


        // Sort the data_sorted keys by string compare.
        uksort($data_sorted, 'strcmp');

        // Encode the sorted data with RFC3986
        $data_encoded_final = http_build_query($data_sorted, '', '&', PHP_QUERY_RFC3986);


        // Generate the base string needed for creation of the signature
        $base = "POST" . "&" . rawurlencode($endpoint_url) . "&"
            . rawurlencode($data_encoded_final)
                . rawurlencode("&oauth_consumer_key=" . rawurlencode($consumer_key)
                . "&oauth_nonce=" . rawurlencode($nonce)
                . "&oauth_signature_method=" . rawurlencode($signature_method)
                . "&oauth_timestamp=" . rawurlencode($timestamp)
                . "&oauth_token=" . rawurlencode($access_token)
                . "&oauth_version=" . rawurlencode($version)
        );


        // Encode the secret and access_token
        $key = rawurlencode($secret) . '&' . rawurlencode($access_token);

        // Generate the oauth_signature. Keep in mind that hash_hmac produce raw output instead of lowercase hexits.
        $signature = rawurlencode(base64_encode(hash_hmac('sha256', $base, $key, true)));


        // Generate the POST CURL request
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $endpoint_url);
        curl_setopt($ch,CURLOPT_POST, count($data));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $data_encoded_final);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Uncomment the following 2 lines in case of curl SSL problems -> do not do this on production
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $headers = [
            'Authorization: OAuth oauth_consumer_key="' .$consumer_key .'",oauth_signature_method="' .$signature_method .'",oauth_timestamp="' .$timestamp .'",oauth_nonce="' .$nonce .'",oauth_version="' .$version .'",oauth_signature="' .$signature .'",oauth_token="' .$access_token .'"',
            'Content-Type: application/x-www-form-urlencoded'
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (!$response){
               echo 'Curl error: ' . curl_error($ch);
        } else {

            return json_decode($response);

            // $classroom->url = $resp['url'];
            // $classroom->id_vedamo = $resp['id'];
            // $classroom->save();

        }

        curl_close($ch);
    }
}
