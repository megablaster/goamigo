<?php

namespace App\Http\Controllers\Student;

use App\Models\Invoice;
use App\Models\InvoiceProduct;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lessons(){

        $data = [
            'invoices' => Invoice::where('student_id',auth()->user()->id)->latest()->get()
        ];

        return view('student.invoices.lesson',compact('data'));
    }

    public function product()
    {
        $data = [
            'invoices' => InvoiceProduct::where('user_id',auth()->user()->id)->latest()->get()
        ];

        return view('student.invoices.product',compact('data'));
    }


}
