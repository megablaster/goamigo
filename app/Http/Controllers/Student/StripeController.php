<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Stripe as StripeModel;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\Models\User;
use App\Models\Product;
use App\Models\Credit;
use App\Models\InvoiceProduct;
use App\Mail\Student\PackageBuy;
use Auth;
use Mail;
use Carbon\Carbon;
use Exception;

class StripeController extends Controller
{
    public function payment(Request $request)
    {
        try {

            //Get product
            $product = Product::find($request->product_id);

            $stripeCharge = Auth::user()->charge(
                $product->price * 100 , $request->id
            );

            //Create user
            // auth()->user()->createAsStripeCustomer();

            //Create Invoice
            // Auth()->user()->invoiceFor($product->name, 500);

            //We update the days and credits
            $credit = Credit::where('user_id',Auth::user()->id)->first();
            
            //Create invoice product
            $invoiceProduct = new InvoiceProduct;
            $invoiceProduct->user_id = auth()->user()->id;
            $invoiceProduct->product_id = $product->id;
            $invoiceProduct->save();

            //Set date today
            if ($credit->expiration < Carbon::today()) {

                $credit->expiration = Carbon::today()->addDays(1);
                $credit->save();

            }

            //Get expiration
            $expiration = Carbon::parse($credit->expiration);

            $credit->credit = $credit->credit + $product->credits;
            $credit->expiration = $expiration->addDays($product->days);
            $credit->save();

            //Send email buy package
            $user = Auth::user();
            Mail::to($user->email)->send(new PackageBuy($user,$product));

            return response()->json(['success' => 'Se ha hecho la compra correctamente.']);

        } catch (Exception $ex) {

            return response()->json(['error' => $ex->getMessage()]);

        }
    }

    public function refund($user_id,$payment_id,$product_id)
    {
        $user = User::find($user_id);
        $product = Product::find($product_id);

        $payment = $user->charge($product->price, $payment_id);
        $user->refund($payment->id);
    }

    public function invoices()
    {
        $invoices = auth()->user()->invoices();
        return view('student.invoices.index', compact('invoices'));

    }

    public function invoice($user_id, $invoice_id)
    {
        try {
            StripeModel::setApiKey(config('services.stripe.secret'));
            $user = User::find($user_id);

            return $user->downloadInvoice($invoice_id, [
                'vendor'  => 'Go Amigo',
                'product' => 'Clases',
            ]);

        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}
