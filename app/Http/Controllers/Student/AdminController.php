<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\ScheduleStudent;
use Carbon\Carbon;

class AdminController extends Controller
{
	public function __construct(){
	    $this->middleware(['role:student']);
	}

    public function index()
    {
        $id = auth()->user()->id;

    	$data = [
            'schedule' => ScheduleStudent::where('user_id',$id)->where('status','active')->take(1)->latest()->first(),
            'schedules' => ScheduleStudent::where('user_id',$id)->where('status','active')->get(),
            'pasts' => ScheduleStudent::where('user_id',$id)->where('status','expired')->get(),
            'user' => auth()->user(),
    	];

    	return view('student.index',compact('data'));
    }

    public function package()
    {
        $data = [
            'products' => Product::get()
        ];

        return view('student.package',compact('data'));
    }

    public function lang(Request $request)
    {
        if ($request->session()->has('locale')  ) {
            session()->put('locale', $request->lang);
        }

        return redirect()->back();

    }
}
