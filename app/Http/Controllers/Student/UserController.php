<?php

namespace App\Http\Controllers\Student;


use Mail;
use Storage;
use Countries;
use Carbon\Carbon;
use App\Mail\UserRegister;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Timezone;
use App\Models\InfoStudent;
use App\Models\InfoTeacher;
use App\Models\Penalty;
use App\Models\Credit;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $data = [
            'users' => User::select('id','name','email','phone','country','suspend')->latest()->get()
        ];

        return view('admin.users.index',compact('data'));
    }

     public function type($type)
    {
        $data = [
            'users' => User::role($type)->select('id','name','email','phone','country','suspend')->latest()->get()
        ];

        return view('admin.users.index',compact('data'));
    }

    public function edit($id)
    {
        $data = [
            'user' => User::where('id_unique',$id)->first(),
            'timezones' => Timezone::Orderby('offset')->get()
        ];

        return view('student.users.edit',compact('data'));
    }

    public function show($id)
    {
        $data = [
            'user' => User::where('id_unique',$id)->first(),
        ];

        return view('student.users.view',compact('data'));
    }

    public function update(Request $request, $id)
    {
            $user = User::find($id);

            if ($request->password) {

                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|unique:users,email,'.$user->id,
                    'password' => 'required|confirmed|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                ]);

            } else {

                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|unique:users,email,'.$user->id,
                ]);

            }

            if ($validator->fails()) {
                return redirect()
                        ->route('student.edit',$user->id)
                        ->withErrors($validator)
                        ->withInput();
            }

            $user->update($request->except('_token','_method','password','password_confirmation'));

            $credit = Credit::where('user_id',$user->id)->first();

            if (is_null($credit)) {

                $credit = Credit::create([
                    'user_id' => $user->id,
                    'expiration' => Carbon::now(),
                    'credit' => 0,
                    'trial' => true,
                ]);
            }

        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        if ($request->image) {

            //Delete image
            Storage::delete($user->img);
            $path = $request->file('image')->store('user');
            $user->img = $path;

        }

        //Add credit
        if ($user->credit->trial == true && $user->name != null && $user->last_name != null && $user->student->nickname != null && $user->student->birthday != null) {

            $credit = Credit::where('user_id',$user->id)->first();

            $credit->expiration = Carbon::now()->addDays(10);
            $credit->credit = 1;
            $credit->trial = false;
            $credit->save();

        }

        //Save image
        $user->save();

        if ($credit->credit == 1) {
            return redirect()->route('student.edit',$user->id_unique)->with('success','Se ha agregado un crédito a tu cuenta.');
        } else {
            return redirect()->route('student.edit',$user->id_unique)->with('success','Se ha actualizado la información personal.');

        }

    }

    public function destroy($id)
    {
        //Delete user
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')->with('success','Se ha borrado el usuario.');
    }

    public function suspend($id)
    {
        //Obtener usuario
        $user = User::findOrFail($id);

        if ($user->suspend == 1) {

            $user->suspend = 0;
            $user->save();
            return redirect()->route('users.index')->with('success','Se ha quitado la suspensión del usuario.');

        } else {

            $user->suspend = 1;
            $user->save();
            return redirect()->route('users.index')->with('success','Se ha suspendido el usuario.');
        }
    }

    public function teacher($id_unique)
    {
        $teacher = User::where('id_unique',$id_unique)->first();
        dd($teacher);
    }
}
