<?php

namespace App\Http\Controllers\Student;

use Zoom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Invoice;
use App\Models\InvoiceTeacher;
use App\Models\ScheduleStudent;
use App\Models\ScheduleTeacher;
use App\Models\Classroom;
use App\Models\Review;
use App\Models\Point;
use App\Models\Favorite;
use Carbon\Carbon;

class LessonController extends Controller
{
	public function index()
	{
        $teacher = User::role('teacher')->whereHas('teacher', function($query) {
            $query->where('step', 'third');
        })->latest()->first();
        $teachers = User::role('teacher')->whereHas('teacher', function($query) {
            $query->where('step', 'third');
        })->whereHas('schedule', function($query){
            $query->whereBetween('start', [Carbon::now()->startOfWeek()->toDateTimeString(), Carbon::now()->endOfWeek
            ()->toDateTimeString()]);
        })->get();

		$arrays = array();
		foreach($teachers as $teacher){
		    array_push($arrays,$teacher->id);
        }

        $data = [
            'teacher' => $teacher,
            'teachers' => $teachers,
            'ids' => $arrays,
            'dates' => ScheduleTeacher::whereDate('start','=',Carbon::tomorrow())->where('assigned',0)->get(),
            'init' => '*',
            'date' => Carbon::tomorrow()->format('Y-m-d'),
            'date_final' => Carbon::tomorrow()->addDays(6)->format('Y-m-d')
        ];

		return view('student.lessons.indexold',compact('data'));
	}

	public function getSchedule(Request $request,$id)
    {
        $data = ScheduleTeacher::where('user_id',$id)->where('start','>=',Carbon::now()->addDay(1)
            ->toDateTimeString())->where('assigned',0)->get();
        return response()->json($data);
    }

    public function schedule(Request $request)
    {
        // event_id, user_id, teacher_id
        //Obtenemos el usuario
        $user = User::find($request->user_id);
        $credit = $user->credit->credit;

        //Obtenemos el maestro
        $teacherUser = User::find($request->teacher_id);

        //Obtenemos vigencia
        $dateInit = Carbon::parse(Carbon::now()->format('d-m-Y'));
        $dateFin = Carbon::parse($user->credit->expiration)->format('d-m-Y');

        //Resultado
        $validity = $dateInit->diffInDays($dateFin,false);

        //Revisamos si todavia esta disponible el horario
        $teacher = ScheduleTeacher::where('id',$request->schedule_id)->where('assigned',0)->first();

        //Revisamos si la clase esta disponible, si yienes créditos o si tienes vigencia.
        if ($teacher) {

            if ($credit >= 1) {

                if ($validity >= 1) {

                    //Asignamos la clase al alumno.
                    $studentSchedule = new ScheduleStudent;
                    $studentSchedule->user_id = $request->user_id;
                    $studentSchedule->teacher_id = $request->teacher_id;
                    $studentSchedule->schedule_id = $request->schedule_id;
                    $studentSchedule->name = $request->name;
                    $studentSchedule->save();

                    //Agregamos facturas
                    /*$chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    $code = "";
                    for ($i = 0; $i < 6; $i++) {
                        $code .= $chars[mt_rand(0, strlen($chars)-1)];
                    }*/

                    /*$invoice = new Invoice;
                    $invoice->student_id = auth()->user()->id;
                    $invoice->teacher_id = $request->teacher_id;
                    $invoice->schedule_id = $request->schedule_id;
                    $invoice->code = $code;
                    $invoice->status = 'purchase';
                    $invoice->save();*/

                    /*$invoice = new InvoiceTeacher;
                    $invoice->student_id = auth()->user()->id;
                    $invoice->teacher_id = $request->teacher_id;
                    $invoice->schedule_id = $request->schedule_id;
                    $invoice->code = $code;
                    $invoice->status = 'purchase';
                    $invoice->save();*/

                    //Generamos una clase
                    $classroom = new Classroom;
                    $classroom->user_id = $user->id;
                    $classroom->teacher_id = $teacherUser->id;
                    $classroom->schedule_id = $teacher->id;
                    $classroom->url = '-';
                    $classroom->id_vedamo = '-';
                    $classroom->save();

                    //Cambiamos el estatus del bloque del teacher
                    $teacher->assigned = 1;
                    $teacher->save();

                    //Restamos puntos del alumno.
                    $user->credit->credit = $user->credit->credit - 1;
                    $user->credit->save();

                    //Revisamos si ya no tiene créditos
                    /*if ($user->credit->credit == 0) {
                        $user->credit->expiration = Carbon::now();
                        $user->credit->trial = 0;
                        $user->credit->save();
                    }*/

                    //Generamos el registro para dejar el review
                    $review = new Review;
                    $review->user_id = $user->id;
                    $review->classroom_id = $classroom->id;
                    $review->teacher_id = $request->teacher_id;
                    $review->save();

                    //Points
                    $point = new Point;
                    $point->points = 0;
                    $point->user_id = $teacherUser->id;
                    $point->schedule_id = $request->schedule_id;
                    $point->save();

                    //Crear la clase en BigBlueBottom

                    return redirect()->back()->with('success','Se ha agendado correctamente la clase, puedes verla en tus clases.');

                } else {

                    return redirect()->back()->with('failed','No cuentas con vigencia de tus créditos, te recomendamos comprar un paquete.');

                }

            } else {

                return redirect()->back()->with('failed','No cuentas con créditos suficientes, te recomendamos adquirir más.');
            }
        } else {

            return redirect()->back()->with('failed','La clase ya no esta disponible, intenta con otra búsqueda.');
        }

    }

    public function calendar_teacher($id)
    {
    	$data = [
    		'teacher' => User::find($id),
            'favorite' => Favorite::where('teacher_id', $id)->first()
    	];

    	return view('student.availability',compact('data'));
    }

	public function search(Request $request)
	{
        if($request->init == '*'){

            $init = ScheduleTeacher::where('assigned',0)
                    ->whereDate('start','=',Carbon::parse($request->date)->toDateString())
                    ->whereDate('start','=',Carbon::parse($request->date_final)->toDateString())
                    ->get();

        } else {

            $init = ScheduleTeacher::where('hour',$request->init)->where('assigned',0)
                    ->whereDate('start','=',Carbon::parse($request->date)->toDateString())
                    ->whereDate('start','=',Carbon::parse($request->date_final)->toDateString())
                    ->get();
        }

        $data = [
            'dates' => $init,
            'init' => $request->init,
            'date' => $request->date,
            'date_final' => $request->date_final
        ];

		return view('student.lessons.indexold',compact('data'));
	}
}
