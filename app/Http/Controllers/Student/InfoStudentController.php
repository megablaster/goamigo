<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\InfoStudent;
use App\Models\Credit;
use Carbon\Carbon;

class InfoStudentController extends Controller
{
    public function update(Request $request, $id)
    {
        $user = User::where('id_unique',$id)->first();
        $info = InfoStudent::find($user->id);
        $info->update($request->except('_token','_method'));
        $info->save();

        //Users current
        $user = Auth()->user();

         //Add credit
        if ($user->credit->trial == true && $user->name != null && $user->last_name != null && $user->student->nickname != null && $user->student->birthday != null) {

            $credit = Credit::where('user_id',$user->id)->first();

            if ($credit->trial == true) {
                $credit->expiration = Carbon::now()->addDays(10);
                $credit->credit = 1;
                $credit->trial = false;
                $credit->save();
            }
        }

        //Save image
        $user->save();

        if ($user->credit->credit == 1) {
            return redirect()->route('student.edit',$user->id_unique)->with('success','Se ha agregado un crédito a tu cuenta.');
        } else {
            return redirect()->route('student.edit',$user->id_unique)->with('success','Se ha actualizado la información personal.');

        }

        return redirect()->route('student.edit',$request->user_id)->with('success','Se ha actualizado la información.');

    }

}
