<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Favorite;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'favorites' => Favorite::where('user_id', auth()->user()->id)->get()
        ];

        return view('student.favorites.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $favorite = Favorite::find($request->id);

        if ($request->type == 'remove') {

            $favorite->delete();
            return redirect()->route('lessons.calendar_teacher',$request->teacher_id)->with('success','Se ha removido el profesor de favoritos.');

        } elseif($request->type == 'remove_professor') {

            $favorite->delete();
            return redirect()->route('favorites.index')->with('success','Se ha removido el profesor de favoritos.');

        } else {

            $favorite = new Favorite;
            $favorite->create($request->except('_token'));

            return redirect()->route('lessons.calendar_teacher',$request->teacher_id)->with('success','Se ha agregado el profesor a favoritos.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function show(Favorite $favorite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function edit(Favorite $favorite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favorite $favorite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Favorite $favorite)
    {
        //
    }
}
