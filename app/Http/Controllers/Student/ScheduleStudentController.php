<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\ScheduleStudent;
use App\Models\ScheduleTeacher;
use App\Models\Penalty;
use App\Models\Credit;
use App\Models\Invoice;
use App\Models\InvoiceTeacher;
use App\Models\User;
use App\Models\Point;
use App\Mail\Student\StudentCancel;
use App\Mail\Student\TeacherCancel;
use App\Mail\Student\StudentCancelPenalty;
use App\Mail\Student\TeacherCancelPenalty;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;

class ScheduleStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = auth()->user()->id;

        $data = [
            'schedules' => ScheduleStudent::where('user_id',$id)->whereDate('created_at', '>=', Carbon::now())->latest()->get()
        ];

        return view('student.classroom.index',compact('data'));
    }
   
    public function show($id)
    {
        $schedule = ScheduleStudent::find($id);

        $data = [
            'schedule' => $schedule,
            'lastcomments' => ScheduleStudent::where('teacher_id',$schedule->teacher_id)->where('id','<>',$id)->get(),
        ];

        return view('student.classroom.view', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $schedule = ScheduleStudent::find($id);
        $schedule->comments = $request->comments;
        $schedule->save();

        return redirect()->route('schedulestudent.show',$schedule->id)->with('success','Se ha actualizado correctamente los comentarios.');
    }

    public function cancel(Request $request)
    {
        //Obtenemos student
        $user = User::find(auth()->user()->id);

        if ($request->type == 'min') {

            $schedule = ScheduleStudent::find($request->id);
            $schedule->status = 'cancel';
            $schedule->save();

            $teacher = ScheduleTeacher::find($schedule->schedule_id);

            //Cambiamos status de factura
            $invoice = Invoice::where('schedule_id',$teacher->id)->first();
            $invoice->status = 'cancel';
            $invoice->save();

            $invoice = InvoiceTeacher::where('schedule_id',$teacher->id)->first();
            $invoice->status = 'payhalf';
            $invoice->save();

            //Enviamos correo a teacher
            Mail::to($schedule->teacher->email)->send(new TeacherCancelPenalty($user,$schedule->teacher,$schedule));

            //Enviamos correo a student
            Mail::to(auth()->user()->email)->send(new StudentCancelPenalty($user,$schedule->teacher,$schedule));

            return redirect()->back()->with('success','Se ha cancelado la clase correctamente, lamentamos informarte que perdiste la clase, ya que no contabas con cancelaciones gratuitas.');


        } else if($request->type == 'more') {

            $schedule = ScheduleStudent::find($request->id);
            $schedule->status = 'cancel';
            $schedule->save();

            //Habilitamos la clase agendada del maestro
            $teacher = ScheduleTeacher::find($schedule->schedule_id);
            $teacher->assigned = 0;
            $teacher->save();

            //Cambiamos status de factura
            $invoice = Invoice::where('schedule_id',$teacher->id)->first();
            $invoice->status = 'cancel';
            $invoice->save();

            //Cambiamos status de factura
            $invoice = InvoiceTeacher::where('schedule_id',$teacher->id)->first();
            $invoice->status = 'cancel';
            $invoice->save();

            //Reasignamos el crédito
            $credit = Credit::where('user_id',auth()->user()->id)->first();
            $credit->credit = $credit->credit + 1;
            $credit->save();

            //Agregamos penalización
            $penalty = Penalty::where('user_id',auth()->user()->id)->first();
            $penalty->actually = $penalty->actually + 1;
            $penalty->save();

            //Enviamos correo a teacher
            Mail::to($schedule->teacher->email)->send(new TeacherCancel($user,$schedule->teacher,$schedule));

            //Enviamos correo a student
            Mail::to(auth()->user()->email)->send(new StudentCancel($user,$schedule->teacher,$schedule));

            return redirect()->back()->with('success','Se ha cancelado la clase correctamente, se te ha abonado 1 crédito.');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ScheduleStudent  $scheduleStudent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $schedule = ScheduleStudent::find($request->id);

    }
}
