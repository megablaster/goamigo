<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOTools;
use App\Models\User;
use App\Models\Coupon;
use App\Models\Category;
use App\Models\Post;
use App\Models\Slide;
use App\Models\Penalty;
use App\Mail\TeacherRegister;
use App\Mail\CampaingRegister;
use App\Mail\UserRegister;
use App\Mail\StudentRegister;
use App\Models\InfoStudent;
use App\Models\InfoTeacher;
use App\Models\System;
use App\Models\Credit;
use Mail;
use Countries;
use Auth;
use App;
use Carbon\Carbon;
use Session;
use Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        SEOTools::setTitle('Homepage');
        SEOTools::setDescription('');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'teachers' => User::role('teacher')->whereHas('teacher', function($q){
                $q->where('featured',1);
                $q->where('step','third');
                $q->where('approve_info',1);
            })->get(),
            'countries' => Countries::getList('es'),
            'slides' => Slide::latest()->get(),
        ];

        return view('front.index',compact('data'));
    }

    public function post()
    {
        SEOTools::setTitle('Post');
        SEOTools::setDescription('');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'categories' => Category::get(),
            'posts' => Post::latest()->paginate(12),
            'randoms' => Post::inRandomOrder()->latest()->limit(3)->get(),
            'countries' => Countries::getList('es'),
        ];

        return view('front.post',compact('data'));
    }

    public function category($category)
    {
        $category = Category::where('url',$category)->first();

        $data = [
            'categories' => Category::get(),
            'category' => $category,
            'posts' => Post::latest()->where('category_id',$category->id)->paginate(12),
            'randoms' => Post::inRandomOrder()->latest()->limit(3)->get(),
            'countries' => Countries::getList('es'),
        ];

        return view('front.post',compact('data'));
    }

    public function single($category,$url)
    {
        //Get post
        $post = Post::where('url',$url)->first();
        
        //Update views
        $post->views = $post->views + 1;
        $post->save();

        SEOTools::setTitle($post->name);
        SEOTools::setDescription($post->meta_description);
        SEOTools::opengraph()->setUrl( url()->current() );
        SEOTools::setCanonical(route('front.single',[$post->category->url,$post->url]));
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('GoAmigo');
        SEOTools::jsonLd()->addImage(route('get.image',$post->img));

        $data = [
            'post' => $post,
            'categories' => Category::get(),
            'posts' => Post::inRandomOrder()->latest()->limit(3)->get(),
            'countries' => Countries::getList('es'),
        ];

        return view('front.single',compact('data'));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');

        $posts = Post::query()
            ->where('name', 'LIKE', "%{$search}%")
            //->orWhere('description', 'LIKE', "%{$search}%")
            ->orWhere('description', 'LIKE', "%{$search}%")
            ->paginate(12);

        $data = [
            'categories' => Category::get(),
            'posts' => $posts,
            'randoms' => Post::inRandomOrder()->latest()->limit(3)->get(),
            'countries' => Countries::getList('es'),
            'search' => $search
        ];

        return view('front.post', compact('data'));
    }

    public function about()
    {
        SEOTools::setTitle('About');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.about',compact('data'));
    }

    public function privacy()
    {
        SEOTools::setTitle('Privacy');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.privacy',compact('data'));
    }

    public function terms()
    {
        SEOTools::setTitle('Terms and Conditions');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.terms',compact('data'));
    }

    public function faqs()
    {
        SEOTools::setTitle('Faqs');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.faqs',compact('data'));
    }

    public function teacher()
    {
        SEOTools::setTitle('Teachers');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
            'teachers' => User::role('teacher')->whereHas('teacher', function($q){
                $q->where('featured',1);
                $q->where('step','third');
                $q->where('approve_info',1);
            })->get(),
        ];

        return view('front.teacher',compact('data'));
    }

    public function hiring()
    {
        SEOTools::setTitle('Hiring');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.hiring',compact('data'));
    }

    public function philosophy()
    {
        SEOTools::setTitle('Philosophy');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.philosophy', compact('data'));
    }

    public function pricing()
    {
        SEOTools::setTitle('Pricing');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.pricing',compact('data'));
    }

    public function teacher_register()
    {
        SEOTools::setTitle('Home');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.teacher_register',compact('data'));
    }

    public function teacher_save(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!/+:;@$%^&_*-_"()=¡&]).{6,}$/',
            'file' => 'required|mimes:pdf|max:6000',
            'experience' => 'required',
            'privacy' => 'accepted',
            'g-recaptcha-response' => 'recaptcha',
        ];

        $messages = [
            'name.required' => 'Se requiere el nombre.',
            'email.required' =>'El correo es necesario.',
            'password.regex' => 'La contraseña debe contener 1 mayuscula, 1 minuscula, 1 número y un signo.',
            'file.mimes' => 'El CV debe tener el formato en pdf.',
        ];

        $this->validate($request, $rules, $messages);

        $user = User::create($request->except('_token','rol','password_confirmation'));
        $user->password = bcrypt($request->password);
        $user->save();

        //Asignamos el rol
        $user->syncRoles('teacher');

        //Se crea el registro de estudiante
        InfoStudent::create([
            'user_id' => $user->id
        ]);

        InfoTeacher::create([
            'user_id' => $user->id,
            'step' => 'first',
            'experience' => $request->experience
        ]);

        if ($request->file) {

            //Delete image
            Storage::delete($user->teacher->file);
            $path = $request->file('file')->store('user');
            $user->teacher->cv = $path;
            $user->teacher->save();

        }

        $global_system = System::find(1);
        if (env('APP_TYPE') == 'live') {

            //Send email verify
            $user->markEmailAsVerified();

            //Send email to teacher
            Mail::to($user->email)->send(new TeacherRegister($user));

        } else {

            //Create coupon
            Coupon::create([
                'code' => Str::random(15),
                'type' => $global_system->type_coupon,
                'percentage' => $global_system->percentage_coupon,
                'description' => 'campaing '. $global_system->date_coupon,
                'amount' => $global_system->amount_coupon,
                'user_id' => $user->id
            ]);

            //Send email to welcome campaing
            Mail::to($user->email)->send(new TeacherRegister($user));

        }

        //Insert password for email
        $user['password'] = $request->password;

        return redirect()->back()->with('success','Se ha recibido su registro, revisa tu correo y válida la cuenta.');
    }

    public function student_register()
    {
        SEOTools::setTitle('Home');
        SEOTools::setDescription('This is my page description');
        SEOTools::opengraph()->setUrl('#');
        SEOTools::setCanonical('#');
        SEOTools::opengraph()->addProperty('type', 'page');
        SEOTools::twitter()->setSite('#');
        SEOTools::jsonLd()->addImage('#');

        $data = [
            'countries' => Countries::getList('es'),
        ];

        return view('front.student_register',compact('data'));
    }

    public function student_save(Request $request)
    {
        $userCheck = User::where('email',$request->email)->first();

        if ($userCheck != null) {
            return redirect('/login')->with('userCheck','Ya tienes una cuenta, inicia sesión.');
        }

        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!/+:;@$%^&_*-_"()=¡&]).{6,}$/',
            'privacy' => 'accepted',
            'g-recaptcha-response' => 'recaptcha',
        ];

        $messages = [
            'email.required' =>'El correo es necesario.',
            'password.regex' => 'La contraseña debe contener 1 mayuscula, 1 minuscula, 1 número y un signo.',
            'g-recaptcha-response' => ''
        ];

        $this->validate($request, $rules, $messages);

        $user = User::create($request->except('_token', 'rol', 'password_confirmation'));
        $user->password = bcrypt($request->password);
        $user->save();

        //Asignamos el rol
        $user->syncRoles('student');

        //Create table student
        if ($user->hasRole('student')) {

            $credit = Credit::where('user_id',$user->id)->first();

            if (is_null($credit)) {
                $credit = Credit::create([
                    'user_id' => $user->id,
                    'expiration' => Carbon::now(),
                    'credit' => 0,
                    'trial' => true,
                ]);
            }

            Penalty::create([
                'actually' => 0,
                'maximum' => 2,
                'type' => 'student',
                'user_id' => $user->id
            ]);

        }

        //Se crea el registro de estudiante
        InfoStudent::create([
            'user_id' => $user->id
        ]);

        InfoTeacher::create([
            'user_id' => $user->id,
            'step' => 'first',
        ]);

        //Add user to Stripe
        $user->createAsStripeCustomer();

        //Update table InfoStudent
        // $user->student->update($request->only('nickname','birthday','comments_learn','comments_children'));
        // $user->student->save();

        $global_system = System::find(1);

        if ($global_system->type_app == 'live'){

            //Send email verify
            $user->markEmailAsVerified();

            //Send email to student
            Mail::to($user->email)->send(new StudentRegister($user));

        } else {

            //Create coupon
            Coupon::create([
                'code' => Str::random(15),
                'type' => $global_system->type_coupon,
                'percentage' => $global_system->percentage_coupon,
                'description' => 'campaing '. $global_system->date_coupon,
                'amount' => $global_system->amount_coupon,
                'user_id' => $user->id
            ]);

            //Send email verify
            $user->markEmailAsVerified();

            //Send email to student
            Mail::to($user->email)->send(new StudentRegister($user));
        }

        //Send email to admin
        $global_system = System::find(1);
        $emails = explode(',',$global_system->emails);

        foreach ($emails as $email) {
            Mail::to($email)->send(new UserRegister($user));
        }

        return redirect()->route('front.index')->with('success', 'Se ha enviado un correo a tu bandeja, te pedimos validez tu cuenta y sigas con el proceso');
    }

    public function change_lang(Request $request)
    {
        $lang = $request->lang;
        Session::put('locale',$lang);
        return redirect()->back();
    }
}
